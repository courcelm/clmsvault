FROM python:3.6

RUN  apt-get update && apt-get install -y \
    gawk \
    tcsh \
    wget \
  && rm -rf /var/lib/apt/lists/*



# Ensure that Python outputs everything that's printed inside
# the application rather than buffering it.
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY install_dep/percolator-v2-09-linux-amd64.deb /usr/src/app/

RUN dpkg -i /usr/src/app/percolator-v2-09-linux-amd64.deb

COPY requirements/base.txt /usr/src/app/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY services/gunicorn /usr/src/app/services/gunicorn

COPY init.sh /usr/src/app/
RUN chown www-data:www-data /usr/src/app \
    && chown www-data:www-data /usr/src/app/*.sh \
    && chmod 700 /usr/src/app/*.sh


COPY ./ /usr/src/app/src

ARG CI_COMMIT_SHA
ENV GIT_SHA $CI_COMMIT_SHA

USER www-data
CMD ["./init.sh"]
