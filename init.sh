#!/bin/sh

cd src
python manage.py collectstatic --noinput  --settings=CLMSVault.settings.base

python manage.py migrate
gunicorn -c ../services/gunicorn/gunicorn_conf.py CLMSVault.wsgi:application