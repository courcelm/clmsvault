# Gunicorn configuration file
# import multiprocessing

bind = '0.0.0.0:8000'
#workers = multiprocessing.cpu_count() * 2 + 1
workers = 2
reload = False
timeout = 518400
max_requests = 50

loglevel = 'warning'
accesslog = '-'
access_log_format = '{"remote_address":"%(h)s","timestamp":"%(t)s","request_method":"%(m)s","url":"%(U)s","query":"%(q)s","protocol":"%(H)s","status":"%(s)s","response_length":"%(b)s","referer":"%(f)s","user_agent":"%(a)s","process_id":"%(p)s"}'
