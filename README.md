
# CLMSVault

CLMSVault is an open source web based software for storing, processing and visualizing protein cross-linking data sets.


CLMSVault can be run as a standalone software or deployed on a server running Docker.


## Getting started

* [User manual] (https://gitlab.com/courcelm/clmsvault/raw/master/static/CLMSVault_User_manual.pdf)



## Publication

J Proteome Res. 2017 Jul 7;16(7):2645-2652. doi: 10.1021/acs.jproteome.7b00205. Epub 2017 Jun 5.

[CLMSVault: A Software Suite for Protein Cross-Linking Mass-Spectrometry Data Analysis and Visualization.] (https://doi.org/10.1021/acs.jproteome.7b00205)

Courcelles M, Coulombe-Huntington J, Cossette E, Gingras AC, Thibault P, Tyers M.