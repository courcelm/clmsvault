/*

The Important Stuff

Setting up...

addSVG()
new jbSvgDrawingInterface(...)
new jbDrawing(...)
new jbViewer(...)

Events...

svg.onmousemove = v.eventmanager.mousemover;
svg.onmousedown = v.eventmanager.mousedown;
svg.onmouseup = v.eventmanager.mouseup;

h2. Peptides

v.setPeptides(...)

h2. Tags

items = v.searchTags(...)

items.addMode(...)
items.setMode(...)
items.reApplyMode()

h2. Peaks

peak = v.addPeak(...);

peak.setDefaultMode(...)
peak.addMode(...)
peak.setMode(...)
peak.setFTextMode(...)
peak.setHTextMode(...)
peak.setAssocPeakHover(...)
peak.setAssocItemsHover(...)



h2. Interactions:

v.autoZoom() // Zoom!

v.modeScanAll(...) // set modes if they exist

*/


// SALMAN, L00K:
// EVERY TIME I WANT YOU ATTENTION YOU'LL SEE:  "// SALMAN, L00K:"




// SALMAN, L00K:
// GLOBAL VARIABLES; KEEP THESE

var svg;
var di;
var d;
var v;

var names = new Array(
   "checko", 
   "checkab", "checkay", "checkbb", "checkby",
   "checkabs", "checkays", "checkbbs", "checkbys"
);
var colours = new Array(
   "#66cc00",
   
   "#ff6600",
   "#ff0000",
   "#0066ff",
   "#0000ff",
   
   "#cc9966",
   "#cc6666",
   "#6699cc",
   "#6666cc"
);


function repaintfrommtab(){
	spectra[0] = aa.mtab;
	makespectrum(0);
}



















/* OBJECT: ISOTOPES 

The format of the elemental input table supplied to the constructor must be: 

Name	Average	iso0mass	iso0%ab	iso1mass	iso1%ab	...	isonmass	ison%ab

Hyphens are allowed for missing values... but zeros are fine too.

   e.g.:
C	12.0107	12	98.8922	13.0033548378	1.1078	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
12C	12	12	100	-	-	-	-	-	-	-	-	-	-
13C	13.0033548378	13.0033548378	100	-	-	-	-	-	-	-	-	-	-
N	14.0067	14.0030740052	99.6337	15.0001088984	0.3663	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
14N	14.0030740052	14.0030740052	100	-	-	-	-	-	-	-	-	-	-
15N	15.0001088984	15.0001088984	100	-	-	-	-	-	-	-	-	-	-

e.g. of usage:

	var els = parseTable(document.getElementById("e").value); // table of elements
  	var iso = new Isotopes(els); // isotope object
	var fa = new Array();          // an array of chemical formulae
	for(var i=0; i<10; i++){
		fa.push("CH3CH2OH");
	}
	var fc = iso.formulachainO(fa); // get the object
	
	properties of fc:
			
		dump = Object(
		   monomass = 460.4186481469999
		   iso = Object(
		      0 = 0.7742273888757276
		      1 = 0.18358219465909717
		      2 = 0.036317008712637835
		      3 = 0.005172240527553551
		      4 = 0.0006325376457197732
		      5 = 0.00006597230820935562
		      6 = 0.000006109624345049915
		      7 = 5.067155944942174e-7
		      8 = 3.8137885451022803e-8
		      9 = 2.6241246291193426e-9
		   )
		   i = 0
		   isomax = 0
		   neutron = 1.0033548378
		   m = 460.4186481469999
		   massmax = 460.4186481469999
		)
	
	

*/

function Isotopes(els){ // pass in your elements table (as parsed by parseTable)
	this.elements = new Array(); // we'll put our elemental data here
	this.neutron = 1.00866491600; // need this for approx. isotope mass calculations
	this.proton = 1.007276466812; // need this for approx. isotope mass calculations
	this.max = 10; // maximum number of isotopes to bother checking
	this.nulliso = new Array(); // a null isotope pattern (starting point for calculations)
	this.nulliso.push(1);
	for(var i=1; i<this.max; i++){
		this.nulliso.push(0);
	}
	// each row in this table is: Element, Average, iso0mass, iso0%, iso1mass, iso1%, etc.
	for(var i=0; i<els.length; i++){
		var en = els[i][0];
		var av = els[i][1];
		this.elements[en] = new Array();
		this.elements[en]['av'] = av;
		this.elements[en]['masses'] = new Array(); // store isotope masses here
		this.elements[en]['probs'] = new Array(); // store isotope probabilities here
		for(var j=2; j<els[i].length; j+=2){
			if(els[i][j] == "-"){ // null entry
				this.elements[en]['masses'].push(0);
				this.elements[en]['probs'].push(0);
			}
			else {
				this.elements[en]['masses'].push(els[i][j]);
				this.elements[en]['probs'].push(els[i][j+1]/100); // convert % to p
			}
		}
	}
	this.formulae = new Array(); // we'll remember formulae so we don't go calculating them over and over
	this.formulamasses = new Array(); // store formula monoisotopic masses in here
}

// methods for dealing with chains of chemical formulae...

// supply an array of formulae, e.g. residue formulae, and get back a useful object :-)
Isotopes.prototype.formulachainO = function(fa){ // formula array
	// called by somebody who wants the information!
	var iso = this.formulachain(fa); // calculates the isotope distribution for this chain
	var isomax = this.isomax(iso); // finds the maximal isotope
	var monomass = this.formulachainmass(fa); // calculates monomass for this chain
	var fcO = new Object(); // make an object...
	fcO.monomass = monomass;
	fcO.iso = iso;
	fcO.isomax = fcO.i = isomax;
	fcO.neutron = this.neutron; // for later reference if necessary
	fcO.massmax = fcO.m = monomass + isomax * this.neutron; // mass of highest peak
	return fcO;
}

Isotopes.prototype.formulachain = function(fa){ // formula array
	// called by formulachainO
	// combine different formulae together...
	var iso = this.nulliso;
	var monomass = 0;
	for(var i=0; i<fa.length; i++){ // foreach formula, lookup the iso and combine
		iso = this.combine(this.formula(fa[i]), iso);
	}
	return iso; // return combined iso
}

Isotopes.prototype.isomax = function(iso){ // very basic
	// used in formulachainO
	var maxi = 0;
	var maxp = 0;
	for(var i=0; i<iso.length; i++){
		if(iso[i] > maxp){
			maxp = iso[i];
			maxi = i;
		}
	}
	return maxi;
}

Isotopes.prototype.formulachainmass = function(fa){ // formula array
	// called by formulachainO
	// combine different formulae together...
	var monomass = 0;
	for(var i=0; i<fa.length; i++){ // foreach formula, lookup monomass and sum
		monomass += this.formulamass(fa[i]);
	}
	return monomass;
}

// methods for dealing with single formulae...

// here we combine the isos for elements in a formula, and also the mass
Isotopes.prototype.formula = function(f){
	// called by formulachain, formulamass
	var fn = this.standardizeformula(f); // check it's the right format!
	// we might've already done this formula
	if(this.formulae[fn])
		return this.formulae[fn]; // don't recalculate!
	
	var iso = this.nulliso;
	var ff = fn.split(/ /);
	var monomass = 0;
	for(var i=0; i<ff.length; i+=2){
		var e = ff[i]; // element name
		var n = ff[i+1]; // number of this type of element
		if(! this.elements[e]){
			alert("no definitions for element " + e);
			return;
		}
		monomass += this.elements[e]['masses'][0] * n;
		for(var j=0; j<n; j++){
			iso = this.combine(this.elements[e]['probs'], iso); // combine iso 
		}
	}
	this.formulae[fn] = iso;
	this.formulamasses[fn] = monomass;
	return iso;
}


// this is the key calculation!!
Isotopes.prototype.combine = function(isoA,isoB){
	// called by formulachain and formula
	// each iso is a list of probabilities
	// we want to combine probabilities from isoA, isoB and isoC, 
	// remembering that the corresponding index in isoC is the sum of indices being considered in the other two
	var isoC = new Array();
	for(var i = 0; i < this.max; i++){
		isoC.push(0);
		for(var j=0; j<=i; j++){
			var d = isoA[j] * isoB[i-j];
			if(d + "" != "NaN"){
				isoC[i] += d;
			}
		}
	}
	return isoC;
}

// calls formula() unless mass is already there
Isotopes.prototype.formulamass = function(f){
	// called by formulachainmass
	var fn = this.standardizeformula(f);
	if(! this.formulamasses[fn])
		this.formula(fn);
	return this.formulamasses[fn];
}


// relaxed-ish format to strict format conversion...

// CH3CH2OH will be converted to C 4 H 6 O 1
// hyphen (-) in front, signifies a negative mass, e.g. loss of this or that
Isotopes.prototype.standardizeformula = function(f){
	// called by formula,  formulamass	
	// here's our generally accepted format:
	var fs = f.split(/(-?\d*[A-Z][a-z]?\s*\d*\s*)/);
	// optional hyphen to signify subtraction of the following element
	// first number is optional isotope, e.g. 13C
	// one caps and one optional l/c letter, e.g. C, Mn, etc.
	// optional space
	// optional number (defaults to 1) to say how many of this element
	
	// store the counts of each in here
	var comp = new Array();
	// and the actual element names here
	var es = new Array();
	for(var i=0; i<fs.length; i++){
		if(! fs[i] || fs[i] == "")
			continue;
		var n = 1; //  default number of this element
		var fsi = fs[i].split(/(\s*\d+\s*$)/); // leave the spaces on the number
		var e = fsi[0]; // this is the element name + optional hyphen
		var sign = 1; // multiplier
		e = e.replace(/ /g,""); // get spaces out of name (not otherwise removed if there was no number!)
		if(e.match(/^-/)){ // is there a hyphen
			e = e.replace(/^-/,""); // removed it
			sign = -1; // invert the multiplier
		}
		if(fsi[1]) // if the number is defined, use it...
			n = parseInt(fsi[1]); // automatically parses out the spaces :-)
		if(comp[e]){ // if there's already a count, update it
			comp[e] += n*sign;
		}
		else { // otherwise, define count and add name to array
			es.push(e);
			comp[e] = n*sign;
		}
	}
	es.sort(); // sort the array of names so they're always in the same order
	var ff = new Array();
	for(var i=0; i<es.length; i++){ // for each name
		var e = es[i]; // name
		var n = comp[e]; // number of these
		ff.push(e); // add to array...
		ff.push(n);
	}
	var fn = ff.join(" "); // join array with single space... that's our strict format :-)
	return fn;
}











// some helper functions...

function dumpItem(n,t,s){ // recursively list the stuff in a data structure
	// n is the name ..  name of this object
	// t is the thing ... the object whose name is n, not the object containing n!
	// s is the spacing ...  fill an extra tab each time to make it pretty :-)
	var d = ""; // dump string
	if(typeof(t) == "object"){ // it's an object
		d += s+n+" = Object(\n"; // first line
		for(item in t){ // dump each thing inside it...
			d += dumpItem(item, t[item], s + "\t");
		}
		d += s+")\n"; // close it off
	}
	else if(typeof(t) == "string"){ // quote it
		d += s+n+" = \""+t+"\"\n";
	}
	else if(typeof(t) == "number"){ // raw
		d += s+n+" = "+t+"\n";
	}
	else { // unknown ? write the type...
		d += s+n+" = "+typeof(t)+" ("+t+")\n";
	}
	return d;
}
function dump(o){ // shortcut to get started with data dumping
	return dumpItem("dump",o,"");
}
function dumpalert(o){ // shortcut to alert a dump
	alert(dump(o));
}



// parses a tab delimited table from a string... called by run
function parseTable(s){
	var rows = s.split(/[\n\r]+/);
	for(var i=0; i<rows.length; i++){
		rows[i] = rows[i].split(/\t/);
	}
	return rows;
}


// try greekSymbol("a") to get alpha, for example!
function greekSymbol(str) {
  if(str.length == 0) {
    return "";
  } 

  return String.fromCharCode(str.charCodeAt(0) + (913 - 65)) + greekSymbol(str.substring(1)); 
}
// try greekSymbol("a") to get alpha, for example!
function subNumber(str) {
  if(str.length == 0) {
    return "";
  } 
  return String.fromCharCode(str.charCodeAt(0) + (0x2080 - 48)) + subNumber(str.substring(1)); 
}

function stringMultiply(str,n){
	// no operator overloading in javascript :-(
	var out = "";
	for(var i=0; i<n; i++){
		out += str;
	}
	return out;
}














//////////////////   AUXILIARY ANNOTATOR BEGIN   ////////////////// 


function AA(){
	// this is my auxiliary annotator class...
	AA.maxlosses = 2;
   this.mzDP = 2;
}
AA.prototype.matchspectrum = function (spec, stab, ppm, pep1, pep2, lp1, lp2){
	/*
		Each row:
			0:seq, 1:mass, 2:alphbeta, 3:ionname, 4:containslinker, 5:otherseq, 6:fullseq, 7:has losses, 8:z, 9:zdev, 10:mz, 11:is used
			12: isotope number 13: is non-matched isotope
	*/
	spec.sort(function(a,b){ return b[1]-a[1]; });
	var table = new Array();
	for(var i=0; i<spec.length; i++){
		var pair = spec[i];
		var emz = pair[0];
		var eic = pair[1];
		var delta;
		if(ppm < 0)
			delta = Math.abs(ppm);
		else
			delta = this.ppm2delta(emz,ppm);
		var lo = emz - delta;
		var hi = emz + delta;
		var newrow = new Array();
		
		
		for(var j=0; j<stab.length; j++){
			var row = stab[j];
				
			
			if(row[10] >= lo && row[10] <= hi && row[11] == 0){
				row[11] = 1; // now we've used it!
				
				newrow = new Array(
					// Run ScanNumber crosslinker FastaHeader1 Peptide1 FastaHeader2 Peptide2
					"DEMO",	1,"Xi","Peptide 1",	pep1,	"Peptide 2",	pep2,	
					// MatchedPeptide alphabeta LinkerPosition1 LinkerPostion2 FragmentName
					row[0], row[2],  lp1,  lp2,   row[3] + stringMultiply("+",row[8]) + " " + emz.toFixed(this.mzDP) ,
					// Fragment NeutralMass Charge CalcMZ ExpMZ MS2Error IsotopPeak IsotopPeakNo Description
					row[6] ? row[6] : row[0], row[1], row[8], row[10], emz, emz-row[10], row[13], row[12], row[4] ? "crosslinked" : "linear",
					// virtual BasePeak AbsoluteIntesity RelativeIntensity IsPrimaryMatch Dashes
					"no",     "",         eic,                  eic,                    1,                     "-" );
				
				j=stab.length; // break;
			}
		}
		if(newrow.length == 0){
//			newrow = new Array("DEMO",	1,"Xi","Peptide 1",	pep1,	"Peptide 2",	pep2,	"",	"",
//					   lp1,	   lp2,	   "",	   "",	   "",	   "",	   "",	   emz,	   "",
//					   0,	   "",	   "no",	   "",	   eic,	   eic,	   1,	   "-" );
		
			newrow = new Array(
				// Run ScanNumber crosslinker FastaHeader1 Peptide1 FastaHeader2 Peptide2
				"DEMO",	1,"Xi","Peptide 1",	pep1,	"Peptide 2",	pep2,	
				// MatchedPeptide alphabeta LinkerPosition1 LinkerPostion2 FragmentName
				"", "",  lp1,  lp2,   "" ,
				// Fragment NeutralMass Charge CalcMZ ExpMZ MS2Error IsotopPeak IsotopPeakNo Description
				"", "", "", "", emz, "", 0, 0, "",
				// virtual BasePeak AbsoluteIntesity RelativeIntensity IsPrimaryMatch Dashes
				"no",     "",         eic,                  eic,                    1,                     "-" );
			
		}
		table.push(newrow);
	}
	return table;
}
AA.prototype.parse_spec = function(spectext){
	var spectrum = spectext.split(/\n/);
	var spec = new Array();
	for(var i = 0; i< spectrum.length; i++){
		if(spectrum[i].match(/[\d\.]+\s+[\d\.]+/)){
			var mm = spectrum[i].replace(/^.*?([\d\.]+)\s+([\d\.]+).*?$/g, "$1,$2").split(/,/);
			mm[0] = parseFloat(mm[0]);
			mm[1] = parseFloat(mm[1]);
			spec.push(mm);
		}
	}
	return spec;
}
AA.prototype.parse_spec2 = function(spectext){
	var spectrum = spectext.split(/\n/);
	var spec = new Array();
	for(var i = 0; i< spectrum.length; i++){
		if(spectrum[i].match(/[\d\.]+\s+[\d\.]+/)){
			var mm = spectrum[i].replace(/^.*?([\d\.]+)\s+([\d\.]+).*?$/g, "$1,$2").split(/,/);
         var o = {};
			o.mz = parseFloat(mm[0]);
			o.int = parseFloat(mm[1]);
			spec.push(o);
		}
	}
	return spec;
}
AA.prototype.parse_seq = function(seq){
	seq = seq.replace( /[\(\)]/g, "" );
	var pep = seq.split( /(?=[A-Z<>\[\]])/ );
	for(var i=0; i<pep.length; i++){
		var rlm = pep[i].replace( /([A-Z<>\[\]])(\d*)(.*)/ , "$1,$2,$3").split(/,/);
		// seems OK so far...
		pep[i] = rlm;
	}
	return pep;
}
AA.prototype.parse_masses = function(masses){
	var list = masses.split(/\n/);
	var masslist = new Array();
	for(var i=0; i<list.length; i++){
		if(list[i].match(/[\w\[\]<>]+\s+\d+\.\d+/)){
			var mm = list[i].replace(/.*?([\w\[\]<>]+)\s+(\d+\.\d+).*/, "$1,$2").split(/,/);
			mm[1] = parseFloat(mm[1]);
			masslist.push( mm );
		}
	}
	return masslist;
}
AA.prototype.item_mass = function(masslist, item){
	if(item == undefined || item.length == 0){
		return 0;
	}
	for(var i=0; i<masslist.length; i++){
		if(item == masslist[i][0]){
			//alert(item + " " + masslist[i][1]);
			return masslist[i][1];
		}
	}
	return 0;
}
AA.prototype.item_formula = function(item){
	if(item == undefined || item.length == 0){
		return "X";
	}
	// for each modification!
	var thisform = "";
	var itemsplit = item.split(/\b/);
	for(var j=0; j<itemsplit.length; j++){
		var thisformadd = '';
		for(var i=0; i<this.formulae.length; i++){
			if(itemsplit[j] == this.formulae[i][0]){
				//alert(item + " " + masslist[i][1]);
				thisformadd = this.formulae[i][1];
			}
		}
		thisform += thisformadd;
	}
	if(thisform == ""){
		alert("Could not find formula for '"+item+"'");
		return 0;
	}
	return thisform;
}

AA.prototype.vfragmentlinearpeptide = function(pep_seq, iontable, lossestable){
	
	
	var rlms = this.parse_seq(pep_seq);
	
		
	var fa = [];
	for(var i=0; i<rlms.length; i++){
		fa.push(
			this.item_formula(rlms[i][0])
			+ this.item_formula(rlms[i][1])
			+ this.item_formula(rlms[i][2]) 
		);
	}
	var bfmass = 0;
	var bfseq = "";
	var b_frags = new Array();
	var b=1;
	for(var i=0; i<rlms.length-2; i++){
		var fc = this.iso.formulachainO(fa.slice(0,i+1));
      var mi = fc.isomax;
		bfmass = fc.massmax;
		bfseq += rlms[i][0] + rlms[i][1] + rlms[i][2];
      var dots = "";
      for(var j=0; j<mi; j++){
         dots += "'"
      }
		if(i>0){
			b_frags.push(new Array(bfseq,bfmass,"alpha","b"+b+greekSymbol("a")+dots,0,"","",0,0,0,0,0,fc.i,0));
			b++;
		}
	}
	// y-series:
	var yfmass = 0;
	var yfseq = "";
	var y_frags = new Array();
	var y=1;
	for(var i=rlms.length-1; i>1; i--){
		var fc = this.iso.formulachainO(fa.slice(i,fa.length));
      var mi = fc.isomax;
		yfmass = fc.massmax;
		yfseq = rlms[i][0] + rlms[i][1] + rlms[i][2] + yfseq;
      var dots = "";
      for(var j=0; j<mi; j++){
         dots += "'"
      }
		if(i<rlms.length-1){
			y_frags.push(new Array(yfseq,yfmass,"alpha","y"+y+greekSymbol("a")+dots,0,"","",0,0,0,0,fc.i,0));
			y++;
		}
	}
	if(y_frags.length > 0){
		var fc = this.iso.formulachainO(fa);
		yfmass = fc.massmax;
	}
	return new Array(b_frags, y_frags, yfmass);
}
AA.prototype.mz2ppm = function(mz,delta){
	return delta * 1000000 / mz;
}
AA.prototype.ppm2delta = function(mz,ppm){
	return mz * ppm / 1000000;
}
AA.prototype.mass2mz = function(mass,z){
	return 1.00727646681 + mass / z;
}
AA.prototype.mz2mass = function(mz,z){
	return (mz - 1.00727646681) * z 
}
AA.prototype.ztabsort = function(tab){
	tab.sort(sorttabbestfirst);
	return tab;
}
AA.prototype.vfragmentcharges = function(tab,precz,maxzdev){
	var pattern = /[RHK\[]/g;
	var charges = new Array();
	for(var i=0; i+1<tab.length; i++){
		var row = tab[i];
		var copy = copyarray(row);
		var joinseq = row[0] + row[5];
		joinseq = joinseq.split("_");
		joinseq = joinseq[0];
		if(joinseq.match(pattern)){
			for(var j=0; j < joinseq.match(pattern).length; j++){
				copy[8]++;
				copy[9] = 1 + j - joinseq.match(pattern).length;
				//if(copy[8] <= precz && Math.abs(copy[9]) <= maxzdev){
				if(copy[8] <= precz){
					copy[10] = this.mass2mz(copy[1], copy[8]);
					charges.push(copyarray(copy));
				}
				
			}
			for(var j=1; j <= maxzdev; j++){
				copy[8]++;
				copy[9]++;
				if(copy[8] <= precz){
					copy[10] = this.mass2mz(copy[1], copy[8]);
					charges.push(copyarray(copy));
				}
				
			}
		}
	}	
	return charges;
}
AA.prototype.vfrag2tab = function(vfrag) {
	var tab = new Array();
	for(var i=0; i+1<vfrag.length; i++){
		var vfragi = vfrag[i];
		for(var j=0; j< vfragi.length; j++){
			tab.push(vfragi[j]);
		}
	}
	return new Array(tab, vfrag[vfrag.length-1]);
}

AA.prototype.vfragmentlosses = function(tab, lossestable){
	var rows = lossestable.split(/\n/);
	for(i in rows){
		var row = rows[i].split(/\t/);
		var lossies = this.vapplylosses(tab, new RegExp(row[1],"g"), row[0], row[2]);
		tab = appendarray(tab,lossies);
	}
	/*
		
	var lossies = this.vapplylosses(tab, /Mox/g, 'CH3SOH', 63.9977368900906);
	tab = appendarray(tab,lossies);
	
	var lossies = this.vapplylosses(tab, /[RKNQ\[]/g, 'NH3', 17.02654493);
	tab = appendarray(tab,lossies);
	
	var lossies = this.vapplylosses(tab, /[STDE\]]/g, 'H2O', 18.01056027);
	tab = appendarray(tab,lossies);
	*/
	return tab;
}

AA.prototype.vapplylosses = function(tab, pattern, loss, mass){
	var lossies = new Array();
	for(var i=0; i+1<tab.length; i++){
		var row = tab[i];
		var copy = copyarray(row);
		var joinseq = row[0] + row[5];
		if(joinseq.match(pattern)){
			for(var j=0; j < joinseq.match(pattern).length; j++){
				if(copy[7] < AA.maxlosses){
					copy[1] -= mass;
					copy[5] += "_" + loss;
					copy[3] += "_" + loss;
					copy[7]++;
					lossies.push(copyarray(copy));
				}
			}
		}
	}
	return lossies;
}


/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///


AA.prototype.vfragmentxlinkpeptide2 = function(pep_a_seq, pep_b_seq, a_link_pos, b_link_pos, lossestable, iontable, params){
   // this takes into account the iontable and makes losses!
   /*
      strategy:
         for each peptide, we need to figure out all the fragments, we also need to calculate the entire peptide
         if the peptides are xlinked, we need to calculate the combinations
         for each fragment, we need to calculate the losses
         for each fragment, we need to calculate the different types of ion (a,b,c,x,y,z)
         Now we should have all possible molecules as formulae... we can calculate their isotopes
         next, we return the table that can be used by the charge function vfragmentcharges()
   */
   
   
   /// Maybe I can goes through this after and turn it into bitesize functions?
   
   
   // some useful items, could be configurable in the future:
   var ALPHA = String.fromCharCode(0x2090);
   var BETA = String.fromCharCode(0x1D66);
   var ALPHACAP = String.fromCharCode(0x0391);
   var BETACAP = String.fromCharCode(0x0392);
   var DOT = String.fromCharCode(0x02E3);
   var PRIME = String.fromCharCode(0x2032);
   var PRIME2 = String.fromCharCode(0x2033);
   var PRIME3 = String.fromCharCode(0x2034);
   var PRIME4 = String.fromCharCode(0x2057);
   var PLUS = String.fromCharCode(0x207A);
   // and to get subscript numbers: subNumber(n.toString())
   
   // params should contain: maxz, minz, fragrounds (can be 1 or 2), maxlosses (can be any number)
   
   //some settings:
   
   var isxlinked = false;
   var is2ndpep = false;
   if(a_link_pos > 0) isxlinked = true;
   if(pep_b_seq.length > 0) is2ndpep = true;
   
   // some variables
   var rlmsB; var rlmsA;
   var atable = [];
   var btable = [];
   
   // first, make the complete list of general fragments (L and R ions! left and right) for both peptides 
   // we'll use the special function vfragmentsequence for this
   // first, need to 
   
   // make sure peps have ends!
   if(! pep_a_seq.match(/\[/)) pep_a_seq = "["+pep_a_seq;
   if(! pep_b_seq.match(/\[/)) pep_b_seq = "["+pep_b_seq;
   if(! pep_a_seq.match(/\]/)) pep_a_seq += "]";
   if(! pep_b_seq.match(/\]/)) pep_b_seq += "]";
   
	   // A pep
   var apar = "par";
   var seqAs = pep_a_seq.split(/(\[?[A-Z]\d*[a-z]*\]?)/).join(":").split(/\:+/);
   if(isxlinked){
      seqAs.splice(a_link_pos+1,0,"z");
   }
      
   // B pep
   var seqBs = ["-"];
   var bpar = "-";
   if(is2ndpep){
      var bpar = "par";
      seqBs = pep_b_seq.split(/(\[?[A-Z]\d*[a-z]*\]?)/).join(":").split(/\:+/);;
      if(isxlinked){
         seqBs.splice(b_link_pos+1,0,"z");
      }
   }
   
   // construct sequence:
   var parent = seqAs.join("") + ":" + seqBs.join("") + "; (" + apar + ":" + bpar + ")";
   var iparents = [
      seqAs.join("") + ":-; (" + apar + ":-)",
      "-:" + seqBs.join("") + "; (-:" + bpar + ")"
   ];
   
   var frag1list = this.vfragmentsequence(parent);
   
   var allfrags = [parent];
   if(! isxlinked)
      allfrags = iparents;
   if(! is2ndpep)
      allfrags = [iparents[0]];

   for(var i=0; i< frag1list.length; i++){
      allfrags.push(frag1list[i]);
      if(params.fragrounds == 2){
         var frag2list = this.vfragmentsequence(frag1list[i]);
         for(var j=0; j< frag2list.length; j++){
            allfrags.push(frag2list[j]);
         }
      }
   }
   
   // if crosslinked, hook the relevant combinations together and add the linker (Z)
   // we need to convert all the fragments into normal sequences...
   for(var i=0; i< allfrags.length; i++){
      var frag = allfrags[i].split(/\s*;\s*/);
      // this is Sequence first, name last
      if(frag[0].match(/z/)){
         frag[0] = frag[0].replace(/\:/,"Z");
         frag[0] = frag[0].replace(/z/g,"");
      }
      else {
         frag[0] = frag[0].replace(/\:/,"");
      }
      var names = frag[1].replace(/[()\s]/g,"").split(/\:/);
      // alpha empty, parent or fragment
      if(names[0] == "-")
         names[0] = "";
      else if(names[0] == "par")
         names[0] = ALPHACAP;
      else 
         names[0] += ALPHA;
      
      // beta empty, parent or fragment
      if(names[1] == "-")
         names[1] = "";
      else if(names[1] == "par")
         names[1] = BETACAP;
      else 
         names[1] += BETA;
            
      if(names[0] == ALPHACAP && names[1] != "" && names[1] != BETACAP)
         names.reverse();
      /// this may be the subject of some debate!!!!!!!!
      if(frag[0].match(/Z/))
         frag[1] = names.join(DOT);
      else
         frag[1] = names.join("");
      
      var type = frag[1];
      // replace numbers by subscript numbers!
      var nos = frag[1].split(/(\d+)/);
      for(var j=0; j<nos.length; j++){
         if(nos[j].match(/\d/)){
            nos[j] = subNumber(nos[j]);
         }
      }
      frag[1] = nos.join("");
      frag[0] = frag[0].replace(/-/,"");
      // name them!
      allfrags[i] = {"type":type,"frag":frag[1],"name": frag[1], "seq": frag[0], "tok":[], "mask":[], "lost":[], "lostform":[]};
   }

   // calculate the loss combinations for each fragment... we might try to limit this to 2!
   /*
   losses table format: 
      Name  Symbol   Use?  Specificity Formula
      -CH3SOH	^	Y	Mox	-C -H3 -S -O -H
      -NH3	*	Y	[RKNQ\[]	-N -H3
      -H2O	°	Y	[STDE\]]	-H2 -O
      -HPO3	†	Y	ph	-H -P -O3
      -H3PO4	‡	Y	ph	-H3 -P -O4
   */
   /// need to "tokenize"
   for(var i=0; i<allfrags.length; i++){
      // tokenize! and add to fragsandlosses
      allfrags[i]["tok"] = allfrags[i]["seq"].split(/([A-Z]\d*)/).clean("");
      // PUT A MASK HERE!
      allfrags[i]["mask"] = [];
      for(var j=0; j<allfrags[i]["tok"].length; j++){
         allfrags[i]["mask"].push(0);
      }
   }
   var losshash = {};
   var fragstolosefrom = allfrags.slice(0,allfrags.length);
   var losses = parseHeadedTable(lossestable);
   /// each round...
   for(var i=0; i<params.maxlosses; i++){
      var lostthisround = [];
      /// each fragment...
      for(var j=0; j<fragstolosefrom.length; j++){
         // grab some properties...
         var tok = fragstolosefrom[j]["tok"];
         var seq = fragstolosefrom[j]["seq"];
         var frag = fragstolosefrom[j]["frag"];
         var name = fragstolosefrom[j]["name"];
         var type = fragstolosefrom[j]["type"];
         var mask = fragstolosefrom[j]["mask"];
         var lost = fragstolosefrom[j]["lost"];
         var lostform = fragstolosefrom[j]["lostform"];
         /// each loss...
         for(var k=0; k<losses.length; k++){
            var lossspec = losses[k]["specificity"];
            var lossname = losses[k]["name"];
            var lossuse = losses[k]["use"];
            var lossform = losses[k]["formula"];
            if(! lossuse || ! lossuse.match(/y/i))
               continue;
            // make RegExp
            var re = new RegExp("^"+lossspec+"$");
            /// each token
            for(var l=0; l<tok.length; l++){
               // don't do it if it's already been done!
               if(mask[l])
                  continue;
               // check if they match
               if(! tok[l].match(re))
                  continue;
               // first calculate our new name
               var newlost = lost.slice(0,lost.length);
               newlost.push(lossname);
               newlost.sort();
               var newname = frag + " " + newlost.join(" ");
               // now test if it's really new:
               if(losshash[newname]) // s'already done! skip it
                  continue;
               // now we know we're definitely adding it, so mask the aa:
               var newmask = mask.slice(0,mask.length);
               newmask[l] = 1; // this aa can no longer lose :-)
               losshash[newname] = 1; // the name is now taken, so no others can make this ion
               // copy the token array:
               var newtok = tok.slice(0,tok.length);
               var newlostform = lostform.slice(0,lostform.length)
               newlostform.push(lossform);
               // make a new object:
               var o = {
                  "seq" : seq, // doesn't change
                  "frag" : frag,  // doesn't change
                  "mask" : newmask,
                  "name" : newname,
                  "tok" : newtok,
                  "lost" : newlost,
                  "lostform" : newlostform,
                  "type" : type
               };
               // add it to the losses from this round:
               lostthisround.push(o);
               
            } /// end token
            
         } /// end loss
         
      } /// end fragment
      
      // append these losses to the full list... (allfrags)
      allfrags = allfrags.concat(lostthisround);
      // now set the current losses for more loss next round...
      fragstolosefrom = lostthisround.slice(0,lostthisround.length);
   } /// end round
   // tidy up the masks and toks:
   for(var i=0; i<allfrags.length; i++){
      delete allfrags[i]["mask"];
   }
   allfrags[0]["ionformula"] = "";
   allfrags[0]["relform"] = "";
   
   
   // now we should have a list of all frags (allfrags) with losses!
   // next we need to add the ion types!
   
   /*
   iontable format: 
         Type   Use?  Handedness  Formula
         a	Y	L		-C -H -O
         b	Y	L		-H
         c	N	L		N H2
         x	N	R		C O -H
         y	Y	R		+H
         z	N	R		-N -H2
   */
   var ions = parseHeadedTable(iontable);
   
   
   // now generate the a,b,c,x,y,z (whatever's in the iontable) fragments from L and R
   for(var k=0; k<params.fragrounds; k++){ // rounds of ion-typing
      var allions = []; // put the new ions here
      for(var j=0; j<ions.length; j++){
         // are we actually using this definition?
         if(ions[j]["use"] != "Y")
            continue;
         // make a regex to match the handedness in the ion name...
         var re = new RegExp(ions[j]["handedness"]);
         // run through each fragment and apply...
         for(var i=0; i<allfrags.length; i++){
            // do we match the handedness?
            if(allfrags[i]["frag"].match(re)){
               // OK, so replace the handedness in the name with the actual symbol...
               var frag = allfrags[i]["frag"].replace(re,ions[j]["type"]); 
               // collective ion formulae... ion type + losses
               var ionform = allfrags[i]["ionformula"] ? allfrags[i]["ionformula"].split(" ") : [];
               ionform.push(ions[j]["formula"]);
               var pep = '';
               if(allfrags[i]["type"].match(new RegExp(ALPHA)))
                  pep +="alpha";
               if(allfrags[i]["type"].match(new RegExp(BETA)))
                  pep +="beta";
               // make a new object...
               var o = {
                  "name" : frag + " " + allfrags[i]["lost"].join(" "), // recalculated
                  "type" : allfrags[i]["type"] ? allfrags[i]["type"].replace(/(\w\d+).*/,"$1") : "X", // new but old but
                  "seq" : allfrags[i]["seq"], // doesn't change
                  "tok" : allfrags[i]["tok"], // doesn't change
                  "frag" : frag,  // substituted...
                  //"mask" : allfrags[i]["mask"],  // don't need this any more
                  "lost" : allfrags[i]["lost"], // doesn't change (i.e. no need to clone)
                  "lostform" : allfrags[i]["lostform"], // doesn't change (i.e. no need to clone)
                  "ionformula" : ionform.join(" "), // new
                  "relform" : ions[j]["formula"] + " " + allfrags[i]["lostform"].join(" "),
                  
                  "iontype" : ions[j]["type"],  // e.g. b, c, y, z
                  "hand" : ions[j]["handedness"], // left/right
                  
                  "ionnumber" : allfrags[i]["type"].replace(/.*(\d+).*/,"$1"), // e.g. 1, 2, 3
                  "pep" : pep, // alpha/beta
               };
               allions.push(o);
            }
            else if(j==0 && ! allfrags[i]["frag"].match(/[LR]/)) { 
               // we don't match any handedness, so add directly to the output
               allions.push(allfrags[i]);
            }
         } // end of fragment
      } // end of ion type
      allfrags = allions.slice(0,allions.length); // copy them back for the next round
   } // end of round
      
   // generate formulae and isotopes
   
   
   for(var i=0; i<allfrags.length; i++){     
      //// this is just pasted in at the moment and needs work... but:
      /// allfrags[i]["tok"] // should exist... just do item_formula() on it
      /// add to this
      var fa = []; 
      for(var j=0; j<allfrags[i]["tok"].length; j++){
         fa.push(this.item_formula(allfrags[i]["tok"][j])); 
      }
      if(allfrags[i]["relform"]){
         var rels = allfrags[i]["relform"].split(/\s+/);
         for(var j=0; j<rels.length; j++){
            if(rels[j])
               fa.push(rels[j]);
         }
      }
      
		var fc = this.iso.formulachainO(fa);
      var mi = fc.isomax;
      
		      
      var dots = "";
      for(var j=0; j<mi; j++){
         dots += "'"
      }
      if(dots.length > 4){
         dots = dots.length + "'";
      }
      dots = dots.replace(/''''/,PRIME4);
      dots = dots.replace(/'''/,PRIME3);
      dots = dots.replace(/''/,PRIME2);
      dots = dots.replace(/'/,PRIME);
      
      allfrags[i]["name"] += dots;
      allfrags[i]["maxisotope"] = mi;
      allfrags[i]["monomass"] = fc.monomass;
      allfrags[i]["maxmass"] = fc.massmax;
      
	}
   
   // calculate charges upto a maximum
   var allions = [];
   for(var i=0; i<allfrags.length; i++){  
      for(var z=params.minz; z<=params.maxz; z++){
         var monomz = this.mass2mz(allfrags[i]["monomass"],z);
         var maxmz = this.mass2mz(allfrags[i]["maxmass"],z);
         // z string:
         var zstring = "";
         if(z == 1)
            zstring = PLUS;
         else if(z == 2)
            zstring = PLUS + PLUS;
         else 
            zstring = supNumber(z.toString()) + PLUS;
         // copy the fragment...
         var o = {
            "name" : allfrags[i]["name"] + zstring, // add the z string
            "type" : allfrags[i]["type"] || "?", // doesn't change
            "iontype" : allfrags[i]["iontype"] || "?", // doesn't change // e.g. b, c, y, z
            "ionnumber" : allfrags[i]["ionnumber"] || "x", // doesn't change // e.g. 1, 2, 3
            "hand" : allfrags[i]["hand"] || "x", // doesn't change // left/right
            "pep" : allfrags[i]["pep"] || "", // doesn't change // alpha/beta
            "seq" : allfrags[i]["seq"], // doesn't change
            "frag" : allfrags[i]["frag"],  // doesn't change
            "lost" : allfrags[i]["lost"], // doesn't change (i.e. no need to clone)
///            "ionformula" : allfrags[i]["ionformula"], // doesn't change
///            "relform" : allfrags[i]["relform"], // doesn't change
            "maxisotope" : allfrags[i]["maxisotope"], // doesn't change
            "monomass" : allfrags[i]["monomass"], // doesn't change
            "maxmass" : allfrags[i]["maxmass"], // doesn't change
            "monomz" : monomz, // doesn't change
            "maxmz" : maxmz, // doesn't change
            "z" : z, // doesn't change
         };
         o["name"] = o["name"].replace(/\s/g, ""); // we don't want any spaces here!
         allions.push(o);
      }
   }
   
   logLoH("All Ions",allions);
   // this appears to work really nicely, giving a list of hashes that tabulates something like:
   /*
   
frag	ionformula	lost	maxisotope	maxmass	maxmz	monomass	monomz	name	relform	seq	z
ΑˣΒ			2	3789.8900339986	3790.8973104654096	3787.8727041665998	3788.8799806334096	ΑˣΒ″⁺		[SNVKVKDEDPNEYNEFPLR]Z[KGDINPEVSMIR]	1
ΑˣΒ			2	3789.8900339986	1895.95229346611	3787.8727041665998	1894.94362855011	ΑˣΒ″⁺⁺		[SNVKVKDEDPNEYNEFPLR]Z[KGDINPEVSMIR]	2
ΑˣΒ			2	3789.8900339986	1264.3039544663434	3787.8727041665998	1263.6315111890099	ΑˣΒ″³⁺		[SNVKVKDEDPNEYNEFPLR]Z[KGDINPEVSMIR]	3
ΑˣΒ			2	3789.8900339986	948.47978496646	3787.8727041665998	947.97545250846	ΑˣΒ″⁴⁺		[SNVKVKDEDPNEYNEFPLR]Z[KGDINPEVSMIR]	4
ΑˣΒ			2	3789.8900339986	758.98528326653	3787.8727041665998	758.58181730013	ΑˣΒ″⁵⁺		[SNVKVKDEDPNEYNEFPLR]Z[KGDINPEVSMIR]	5
ΑˣΒ			2	3789.8900339986	632.6556154665767	3787.8727041665998	632.31939382791	ΑˣΒ″⁶⁺		[SNVKVKDEDPNEYNEFPLR]Z[KGDINPEVSMIR]	6
ΑˣΒ			2	3789.8900339986	542.42013846661	3787.8727041665998	542.13194849061	ΑˣΒ″⁷⁺		[SNVKVKDEDPNEYNEFPLR]Z[KGDINPEVSMIR]	7
a₁ₐ	-C -H -O		0	88.039853442	89.04712990880999	88.039853442	89.04712990880999	a₁ₐ⁺	-C -H -O 	[S	1
a₁ₐ	-C -H -O		0	88.039853442	45.027203187809995	88.039853442	45.027203187809995	a₁ₐ⁺⁺	-C -H -O 	[S	2
a₁ₐ	-C -H -O		0	88.039853442	30.35389428081	88.039853442	30.35389428081	a₁ₐ³⁺	-C -H -O 	[S	3
a₁ₐ	-C -H -O		0	88.039853442	23.01723982731	88.039853442	23.01723982731	a₁ₐ⁴⁺	-C -H -O 	[S	4
a₁ₐ	-C -H -O		0	88.039853442	18.61524715521	88.039853442	18.61524715521	a₁ₐ⁵⁺	-C -H -O 	[S	5
a₁ₐ	-C -H -O		0	88.039853442	15.680585373809999	88.039853442	15.680585373809999	a₁ₐ⁶⁺	-C -H -O 	[S	6
a₁ₐ	-C -H -O		0	88.039853442	13.584398387095714	88.039853442	13.584398387095714	a₁ₐ⁷⁺	-C -H -O 	[S	7
a₂ₐ	-C -H -O		0	202.0827808892	203.09005735601	202.0827808892	203.09005735601	a₂ₐ⁺	-C -H -O 	[SN	1
   
   */
   
   // proprties of each object:
   
   // frag                 fragment 
   // type                 type of fragment (should be used for tags)
   /// ionformula        formula used to convert to ion
   /// lost	                names of any losses
   // maxisotope	     isotope that should have maximum intensity
   // maxmass	         mass of maximum isotop
   // maxmz	             m/z of maximum isotope
   // monomass	         monoisotopic mass
   // monomz	             monoisotopic m/z
   // name	                 name given to this ion
   /// relform	          relative formula (to get from sequence to this ion with these losses
   // seq                    sequence
   // z                       chargestate
   // hand                  L/R
   // pep                     alpha/beta
   // iontype              a, b, c, etc
   // ionnumber         1, 2, 3 etc
   
   /// those with 3 slashes might get removed from the objects
   
   return allions;
   
   /// here's a clue as to how to format the returns:
	//		b_frags.push(new Array(bfseq,bfmass,"alpha","b"+b+greekSymbol("a")+dots,0,"","",0,0,0,0,0,fc.i,0));
};


AA.prototype.spec_remove_noise = function(spec){
   // two passes over the spectrum, 
   // one for analysis
   // one for collecting the peaks we'll keep for annotation
};

AA.prototype.match2 = function(frags, spec, tol, tolu){
   // the aim of this function is to put fragments onto peaks.
   // peaks in spec are currently a 2 element array of mz, int.
   // we can push the peaks onto the array
   
   // should probably call spec_remove_noise on spec first.
   
   // anyway, in the simplest case:
   for(var i=0; i<spec.length; i++){
      var peak = spec[i];
      var realtol = tol;
      if(tolu == "ppm")
         realtol = tol * peak[0] / 1000000;
      var lo = peak[0] - realtol;
      var hi = peak[0] + realtol;
      for(var j=0; j<frags.length; j++){
         var frag = frags[j];
         if(lo < frag.maxmz && frag.maxmz < hi)
            spec[i].push(frag);
      }
   }
   // STOPHERE
   return spec;
};



AA.prototype.fraglistToStab = function(fraglist, alphaseq, betaseq){
   /*
   		Each row:
			0:seq, 1:mass, 2:alphbeta, 3:ionname, 4:containslinker, 
           5:otherseq, 6:fullseq, 7:has losses, 8:z, 9:zdev, 10:mz, 11:is used
			12: isotope number 13: is non-matched isotope
   */
   
   var ALPHA = String.fromCharCode(0x2090);
   var BETA = String.fromCharCode(0x1D66);
   var ALPHACAP = String.fromCharCode(0x0391);
   var BETACAP = String.fromCharCode(0x0392);
   var DOT = String.fromCharCode(0x02E3);
   var are = new RegExp(ALPHA);
   
   // set up regexes for different ion types:
   var NOBRACKET = "[^\\[\\]]";
   var PLUS = "+";
   var STAR = "*";
   var LBRACKET = "\\[";
   var RBRACKET = "\\]";
   var START = "^";
   var END = "$";
   var NBS = NOBRACKET + STAR;
   var PAIR = LBRACKET+NBS+RBRACKET
   
   var seqpatts = {
      "L" : [START, LBRACKET, END],
      "La" : [START, LBRACKET, PAIR, END],
      "Lb" : [START, PAIR, LBRACKET, END],
      "R" : [START, RBRACKET, END],
      "Ra" : [START, RBRACKET, PAIR, END],
      "Rb" : [START, PAIR, RBRACKET, END],
      "P" : [START, PAIR, PAIR, END],
   };
   
   for(k in seqpatts){
      seqpatts[k] = new RegExp(seqpatts[k].join(NBS));
   }
      
   var table = [];
   for(var i=0; i<fraglist.length; i++){
      var frag = fraglist[i];
      var row = [];
      // first we need to recalculate the fragment sequence from the list,
      var seqbits = frag.seq.split(/Z/);
      if(frag.seq.match(seqpatts["L"])) 
         row.push(seqbits[0]);
      else if(frag.seq.match(seqpatts["La"])) 
         row.push(seqbits[0]);
      else if(frag.seq.match(seqpatts["Lb"])) 
         row.push(seqbits[1]);
      else if(frag.seq.match(seqpatts["R"])) 
         row.push(seqbits[0]);
      else if(frag.seq.match(seqpatts["Ra"])) 
         row.push(seqbits[0]);
      else if(frag.seq.match(seqpatts["Rb"])) 
         row.push(seqbits[1]);
      else if(frag.seq.match(seqpatts["P"])) 
         row.push("ZZPZZ");
      else 
         row.push("ZZ?ZZ");
      
      // mass we already have
      row.push(frag.monomass);
      // then we need to figure out if it's alpha or beta (does it contain ALPHA or BETA?
      var isa = frag.name.match(are);
      row.push(isa ? "alpha" : "beta");
      // the ion name we already have
      row.push(frag.name);
      // does it contain the linker? 
      row.push(seqbits.length > 1 ? 1 : 0);
      // what's the opposite sequence?
      row.push(isa ? betaseq : alphaseq);
      // what's the full sequence of the fragmented peptide
      row.push(frag.seq.replace(/Z/, " + "));
      // does it have losses? (does it contain "-"
      row.push(frag.name.match(/-/));
      // z we have
      row.push(frag.z);
      // zdev we'll ignore (set to 1)
      row.push(1);
      // mz we have
      row.push(frag.maxmz);
      // is used??
      row.push(0);
      // isotope number we have
      row.push(frag.maxisotope);
      // is non-matched isotope??
      row.push(0);
      
      table.push(row);
   }
   
   return table;
};

/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///

AA.prototype.matchFSBy = function(f, s, by, tol, unit){
	// s = sepctrum
	// f = fragments as returned by vfragmentxlinkpeptide2
	// by = intensity || accuracy
	// tol = tolerance
	// unit = ppm || Da/Th
	
	// properties of each object:
   // frag                 type of fragment (should be used for tags)
   // maxisotope	     isotope that should have maximum intensity
   // maxmass	         mass of maximum isotop
   // maxmz	             m/z of maximum isotope
   // monomass	         monoisotopic mass
   // monomz	             monoisotopic m/z
   // name	                 name given to this ion
   // seq                    sequence
   // z                       chargestate
	
	/* could do just the basic matching here, and decide in the display
		function which of the annotations on a peak are actually shown. 
		What we must do here, is make sure two nearby peaks don't end
		up with the same annotation... the way to do this is:
	
		run through the fragments... choose the nearest or most intense
		peak within the tolerance for that fragment.
		
		or:
		
		label all the peaks with all possible annotations... then go through
		each combination for that local area and take the one that gives 
		the lowest overall ppm error.
		
		certainly: any two fragments giving the same m/z should be attached
		to the same peak (could this be done a-priori?)
	*/
	
	// return a match object including annotated spectrum and statistics
};




/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///


AA.prototype.vfragmentsequence = function(sequence){
   // this is a really basic function!  Takes a sequence and generates a list of fragmented sequences...
   // e.g.  [PEPTKzIDE]:[PEPTKzIDE]; (par:par)
   // would be a parent ion of two PEPTKIDEs linked at the Ks
   var list = [];
   /* strategy:  
             split on \s*;\s*
             first element is sequence
                   split on ":"
                   for each
                      split on (\[?[A-Z]\d*[a-z]*\]?)
                      for each n 1..length
                         grab the L ions if not already an L ion
                         grab the R ions if not already an R ion
                         if L matches /z/ join with the other peptide
                         if R matches /z/ join with the other peptide
                         update the name
                         add L and R to list
             second element is fragment names
   */
   var seqSN = sequence.split(/\s*;\s*/);
   var sequences = seqSN[0].split(/\s*\:\s*/);
   var names = seqSN[1].split(/\s*\:\s*/);
   // FOR THE MOMENT, WE'LL ONLY DEAL IN A AND B!!!...
   var seqAs = sequences[0].split(/(\[?[A-Z]\d*[a-z]*\]?)/);
   var seqBs = sequences[1].split(/(\[?[A-Z]\d*[a-z]*\]?)/);
   var seqA = [];
   var seqB = [];
   for(var i=0; i<seqAs.length; i++){
      if(seqAs[i].length > 0){
         seqA.push(seqAs[i]);
      }
   }
   for(var i=0; i<seqBs.length; i++){
      if(seqBs[i].length > 0){
         seqB.push(seqBs[i]);
      }
   }
   
   var nameA = names[0].replace(/\s*\(\s*/g,"");
   var nameB = names[1].replace(/\s*\)\s*/g,"");
   var nameAp = nameA.replace(/par/g,"");
   var nameBp = nameB.replace(/par/g,"");
   var doAL = ! nameA.match(/[L-]/);
   var doAR = ! nameA.match(/[R-]/);
   var doBL = ! nameB.match(/[L-]/);
   var doBR = ! nameB.match(/[R-]/);
   // alpha:
   for(var i=0; i<seqA.length-1; i++){
      // left
      if(doAL){
         var n = i+1;
         var seq = seqA.slice(0,i+1).join("");
         var name = "L"+n + nameAp;
         name += ":";
         seq += ":"; // whether or not it's crosslinked!
         if(seq.match(/z/)){
            name += nameB;
            seq += seqB.join("");
         }
         else {
            name += "-";
         }
         var ion = seq + "; " + name;
         list.push(ion);
      }
      if(doAR){
         /// right
         var n = i+1;
         var nn = seqA.length - n;
         var seq = seqA.slice(n,seqA.length).join("");
         var name = "R"+nn + nameAp;
         name += ":";
         seq += ":"; // whether or not it's crosslinked!
         if(seq.match(/z/)){
            name += nameB;
            seq += seqB.join("");
         }
         else {
            name += "-";
         }
         var ion = seq + "; " + name;
         list.push(ion);
      }
   }
   // beta:
   for(var i=0; i<seqB.length-1; i++){
      // left
      if(doBL){
         var n = i+1;
         var seq = seqB.slice(0,i+1).join("");
         var name = "L"+n + nameBp;
         name = ":" + name;
         seq = ":" + seq; // whether or not it's crosslinked!
         if(seq.match(/z/)){
            name = nameA + name;
            seq = seqA.join("") + seq;
         }
         else {
            name = "-" + name;
         }
         var ion = seq + "; " + name;
         list.push(ion);
      }
      if(doBR){
         /// right
         var n = i+1;
         var nn = seqB.length - n;
         var seq = seqB.slice(n,seqB.length).join("");
         var name = nameBp+"R"+nn;
         name = ":" + name;
         seq = ":" + seq; // whether or not it's crosslinked!
         if(seq.match(/z/)){
            name = nameA + name;
            seq = seqA.join("") + seq;
         }
         else {
            name = "-" + name;
         }
         var ion = seq + "; " + name;
         list.push(ion);
      }
   }
   return list;
};
   


/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///
/// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// //
// /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // /// // ///



AA.prototype.vfragmentxlinkpeptide = function(pep_a_seq, pep_b_seq, a_link_pos, b_link_pos, iontable, lossestable){
	// 1) If pep_x_seq is empty, don't make fragments!
	// 2) If x_link_pos is 0 then don't link!
	
	var pep_a_bfrags = new Array();
	var pep_a_yfrags = new Array();
	var pep_b_bfrags = new Array();
	var pep_b_yfrags = new Array();
	var pep_a_mass = 0;
	var pep_b_mass = 0;
	
	if(pep_a_seq != ""){
		var pep_a_frags = this.vfragmentlinearpeptide(pep_a_seq, iontable, lossestable);
		pep_a_bfrags = pep_a_frags[0];
		pep_a_yfrags = pep_a_frags[1];
		pep_a_mass = pep_a_frags[2];
		var pep_b_frags = this.vfragmentlinearpeptide(pep_b_seq, iontable, lossestable);
		pep_b_bfrags = pep_b_frags[0];
		pep_b_yfrags = pep_b_frags[1];
		pep_b_mass = pep_b_frags[2];
	}
	var totalmass = pep_a_mass + pep_b_mass;
	
	// make beta...
	for(var i=0; i < pep_b_bfrags.length; i++){
		pep_b_bfrags[i][2] = "beta";
		// replace greekSymbol("a") with greekSymbol("b")
		pep_b_bfrags[i][3] = pep_b_bfrags[i][3].replace(new RegExp(greekSymbol("a"),"g"), greekSymbol("b"));
	}
	for(var i=0; i < pep_b_yfrags.length; i++){
		pep_b_yfrags[i][2] = "beta";
		// replace greekSymbol("a") with greekSymbol("b")
		pep_b_yfrags[i][3] = pep_b_yfrags[i][3].replace(new RegExp(greekSymbol("a"),"g"), greekSymbol("b"));
	}
	
	
	// make xlinked if xlinked!  // this is all crap and gets sorted out in recalcIsotopesFromRow
	if(a_link_pos > 0 && b_link_pos > 0){
		// for b-series
		// on alpha
		for(var i=a_link_pos-1; i < pep_a_bfrags.length; i++){
			pep_a_bfrags[i][1] += pep_b_mass; // add the other peptide!
			pep_a_bfrags[i][4] = 1; // contains linker
			pep_a_bfrags[i][5] = pep_b_seq; // other seq
			pep_a_bfrags[i][6] = pep_a_bfrags[i][0] + " + " + pep_b_seq; // full fragment seq
			pep_a_bfrags[i][3] += "[" + greekSymbol("b") + "]";
         //pep_a_bfrags[i] = this.recalcIsotopesFromRow(pep_a_bfrags[i]);
		}
		// on beta
		for(var i=b_link_pos-1; i < pep_b_bfrags.length; i++){
			pep_b_bfrags[i][1] += pep_a_mass; // add the other peptide!
			pep_b_bfrags[i][4] = 1; // contains linker
			pep_b_bfrags[i][5] = pep_a_seq; // other seq
			pep_b_bfrags[i][6] = pep_b_bfrags[i][0] + " + " + pep_a_seq; // full fragment seq
			pep_b_bfrags[i][3] += "[" + greekSymbol("a") + "]";
         //pep_b_bfrags[i] = this.recalcIsotopesFromRow(pep_b_bfrags[i]);
		}
		// and for y-series
		// on alpha
		for(var i=pep_a_yfrags.length-a_link_pos+1; i < pep_a_yfrags.length; i++){
			pep_a_yfrags[i][1] += pep_b_mass; // add the other peptide!
			pep_a_yfrags[i][4] = 1; // contains linker
			pep_a_yfrags[i][5] = pep_b_seq; // other seq
			pep_a_yfrags[i][6] = pep_a_yfrags[i][0] + " + " + pep_b_seq; // full fragment seq
			pep_a_yfrags[i][3] += "[" + greekSymbol("b") + "]";
         //pep_a_yfrags[i] = this.recalcIsotopesFromRow(pep_a_yfrags[i]);
		}
		// on beta
		for(var i=pep_b_yfrags.length-b_link_pos+1; i < pep_b_yfrags.length; i++){
			pep_b_yfrags[i][4] = 1; // contains linker
			pep_b_yfrags[i][5] = pep_a_seq; // other seq
			pep_b_yfrags[i][6] = pep_b_yfrags[i][0] + " + " + pep_a_seq; // full fragment seq
			pep_b_yfrags[i][3] += "[" + greekSymbol("a") + "]";
         //pep_b_yfrags[i] = this.recalcIsotopesFromRow(pep_b_yfrags[i]);
		}
      
      
	}
   
   // recalculate ALL
   for(var i=0; i < pep_a_bfrags.length; i++){
      pep_a_bfrags[i] = this.recalcIsotopesFromRow(pep_a_bfrags[i]);
   }
   for(var i=0; i < pep_b_bfrags.length; i++){
      pep_b_bfrags[i] = this.recalcIsotopesFromRow(pep_b_bfrags[i]);
   }
   for(var i=0; i < pep_a_yfrags.length; i++){
      pep_a_yfrags[i] = this.recalcIsotopesFromRow(pep_a_yfrags[i]);
   }
   for(var i=0; i < pep_b_yfrags.length; i++){
      pep_b_yfrags[i] = this.recalcIsotopesFromRow(pep_b_yfrags[i]);
   }
   
	return new Array(pep_a_bfrags, pep_a_yfrags, pep_b_bfrags, pep_b_yfrags, totalmass);
}

AA.prototype.recalcIsotopesFromRow = function(row){
   /* now need to recalculate the isotope stuff!
      row[0] // frag seq
      row[5] // other seq
      row[3] // ion name
      row[12] // isotope
      row[1] // mass
   */
   // parse sequence:  (copied from vfragmentlinearpeptide)
   var pep_seq = row[0] + row[5];
   if(row[4] == 1) pep_seq += "Z"; // contains linker
   var rlms = this.parse_seq(pep_seq);

   // how about losses?
   var losses = row[3].split(/(_\w+)/);
   
   losses.shift(); // gets rid of parts before first loss
   losses.pop(); // gets rid of parts after last loss
   
   // make formula:   (copied from vfragmentlinearpeptide)
   var fa = [];
   for(var i=0; i<rlms.length; i++){
      fa.push(this.item_formula(rlms[i][0]) + this.item_formula(rlms[i][1]) + this.item_formula(rlms[i][2]) );
   }

   var fc = this.iso.formulachainO(fa);
   row[12] = fc.isomax;
   row[1] = fc.massmax;
   
   // redo dots
   row[3] = row[3].replace(/'+/g,"");
   var dots = "";
   for(var j=0; j<fc.isomax; j++){
      dots += "'"
   }
   row[3] += dots;
   
   return row;
}

// IMPORTANT FUNCTION THAT ACTUALLY DOES A LOT...
AA.prototype.makePeaks2 = function(matchedspec, a_pep_len, b_pep_len, colours, namesubs){
   // we go first by intensity, then by ppm...
   
   
   // some useful items, could be configurable in the future:
   var ALPHA = String.fromCharCode(0x2090);
   var BETA = String.fromCharCode(0x1D66);
   var ALPHACAP = String.fromCharCode(0x0391);
   var BETACAP = String.fromCharCode(0x0392);
   var DOT = String.fromCharCode(0x02E3);
   var PRIME = String.fromCharCode(0x2032);
   var PRIME2 = String.fromCharCode(0x2033);
   var PRIME3 = String.fromCharCode(0x2034);
   var PRIME4 = String.fromCharCode(0x2057);
   var PLUS = String.fromCharCode(0x207A);
   // and to get subscript numbers: subNumber(n.toString())
   
   // matched losses ... 
   var namesubexp = {};
   for(var p in namesubs){
      var patt = p;
      patt = patt.replace(/([*+?!\[\]\^])/g,"\\$1");
      namesubexp[p] = new RegExp(patt, "g");
   }
   var matchedlosses = {};
   
   // sort matched spec by ic... descending, unless is has no matches, in which case send it to the beginning!
   matchedspec.sort(function(a,b){return b.length > 2 ? b[1]-a[1] : -1});
   
   // matchedspec in spec are currently a 2 element array of mz, int.
   // further elements are matches push in...
   // matches have:         
         // frag                 fragment 
         // type                 type of fragment (should be used for tags)
         /// ionformula        formula used to convert to ion
         // lost	                names of any losses
         // maxisotope	     isotope that should have maximum intensity
         // maxmass	         mass of maximum isotop
         // maxmz	             m/z of maximum isotope
         // monomass	         monoisotopic mass
         // monomz	             monoisotopic m/z
         // name	                 name given to this ion
         /// relform	          relative formula (to get from sequence to this ion with these losses
         // seq                    sequence
         // z                       chargestate
         // hand                  L/R
         // pep                     alpha/beta
         // iontype              a, b, c, etc
         // ionnumber         1, 2, 3 etc

   // keep track of what we've already assigned:
   var frago = new Object();  // a fragment "hash table"... keyed by m/z
   var iono = new Object();  // a fragment "hash table"... keyed by m/z
   
   // for each peak... (unexplained first, then most intense first)
   for ( var i = 0 ; i < matchedspec.length ; i++){
      
      // this fragment:
      var mz = matchedspec[i][0];
      var ic = matchedspec[i][1];
      
      // collect the ion explanation, if there is one
      var ion;
      var ismatched = false;
      var isalpha = false;
      // are there any ions matched?  if so, find the one with the lowest ppm error.
      if(matchedspec[i].length > 2){ // we have matches!
         var thismatch = matchedspec[i][2];
         var ppmerr_min = 10000;
         for(var j = 2; j < matchedspec[i].length ; j++){
            var ppmerr = Math.abs(mz - matchedspec[i][j].maxmz);
            if(ppmerr < ppmerr_min){
               thismatch = matchedspec[i][j];
            }
         }
         if(iono[thismatch.name] == null){ // this ion not used before
            ismatched = true;
            ion = thismatch;
            iono[thismatch.name] = 1;
         }
        // alert([ion.frag,ion.lost,ion.maxisotope,ion.z]);
      }
      
      
      // these are per peak
      var fixedtext = ""; // text that will be displayed in the selected state
      var hovertext = ""; // text that will be displayed on-hover in the selected state
      var iontype = ""; // what type of ion is this?
      var iontag = ""; // tags that can be use to search for this peak
      var ionnumber = 0; // e.g. the 8 in y8
      var peplength = 0;
      
      if(ismatched){
         fixedtext = ion.name + " - " + mz.toFixed(2);
         for(var p in namesubs){
            if(fixedtext.match(namesubexp[p]) && namesubs[p])
               matchedlosses[p] = namesubs[p];
            fixedtext = fixedtext.replace(namesubexp[p],namesubs[p]);
         }
      }
      
      // have we seen this peak before?  if not, make one:
      if(frago[mz] == null)
         frago[mz] = v.addPeak(mz, ic, fixedtext, hovertext, "");
         
      var peak = frago[mz]; // look up this peak
      
      // type of peak...
      // 0:other  1:A-b  2:A-y  3:B-b  4:B-y  5:A-b(s)  6:A-y(s)  7:B-b(s)  8:B-y(s)  
      var peakType = 'unmatched';
      
      peak.userData = "Unmatched peak\nm/z "+mz + " (obs.)\nIntensity "+ic;

      if(ismatched){ 
         
         /// everything f.something has to change!
         peakType = ion.iontype + "_" + ion.pep;
         if(ion.lost.length > 0)
            peakType += "_lossy";
         
         peplength = ion.pep == "alpha" ? a_pep_len : b_pep_len; 
                        // length of the peptide that fragmented
         // grab some info...
         iontype = ion.iontype;
         ionnumber = ion.ionnumber;
         iontag = ion.type;
         
         // ppm error:
         var ppm = 1000000*(ion.maxmz-mz)/mz;
         
         peak.userData = ion.name+"\n"+ppm.toFixed(2) + " ppm\nm/z "+mz + " (obs.)\nIntensity "+ic;
         
         hovertext = mz.toFixed(this.mzDP);
         hovertext += " | "+ppm.toFixed(1)+" ppm";
             
         // MODE NAME:
         // we create a mode-name base on the type of ion...
         var modename = "mode"+peakType; // makes sense!
         var hovername = "hover"+peakType; // oh yeah!
         
         // add this text (so text can change!)
         peak.addModeFText(modename,fixedtext);
         peak.addModeFText(hovername,fixedtext);
         peak.addModeHText(modename,hovertext);
         peak.addModeHText(hovername,hovertext);
         
         // colours:
         // this colour object is nice :-) ///// where do these colours come from?
         var thisColour = colours[peakType];
         var thisRGB = hexColourToRGB(thisColour);
         var thisLightened = thisRGB.lighten().toHex();
         var thisDarkened = thisRGB.darken().toHex();
         var thisMuted = thisRGB.muted().toHex();
         var thisMuted2 = thisRGB.lighten().muted2().toHex();
                           
         // Default mode... the one that everybody start off in...
         peak.setDefaultMode(thisMuted2, thisMuted2, thisMuted2, "font-size:12pt;");
         peak.setFTextMode("_default_","#000000",0,""); // don't display text!
	 
         // Selected mode... when you select the peaks
         peak.addMode(modename,thisColour,1,"font-size:12pt;");
         // make this peak go into "hover" mode when we hover on it from this mode
         peak.setAssocPeakHover(modename, peak, hovername); 
         peak.setHTextMode(modename,"#000000",0,""); // don't display hover text!
/// ??         if(f.IsPrimaryMatch != "")
///  ??          peak.setModePrimaryFlag(modename, f.IsPrimaryMatch);
//         peak.setModePrimaryFlag(modename, "1");
         
         // Hover mode...  defined the hover mode we want
         peak.addMode(hovername, thisDarkened, 1, "font-size:12pt;");
         
         // SETUP MODE-HOVER
         var res = null; // results from a searchTags search...
         var fragres = null;
         
         if(ion.pep == "alpha"){ // alpha peptide
            // firstly, let's make the associates fragment lines visible and grey...
            var fraglineres = v.searchTags("PEPTIDE1 FRAG " + iontag);
            fraglineres.setMode("_default_","#cccccc",1,"");
            fraglineres.reApplyMode();
            
            // now lets search for the relevant items...
            if(! ion.seq.match(/Z/)){
               res = v.searchTags("PEPTIDE1 " + iontag);
            }
            else {
               // all of the opposite peptide and the linker!
               res = v.searchTags("PEPTIDE2 AA | LINKER | PEPTIDE1 " + iontag);
            }
            fragres = v.searchTags("PEPTIDE1 FRAG " + iontag);
            
            v.registerModeTag(modename, "PEPTIDE1", iontag);
         }
         else if(ion.pep == "beta"){ // beta peptide
            // firstly, let's make the associates fragment lines visible and grey...
            var fraglineres = v.searchTags("PEPTIDE2 FRAG " + iontag);
            fraglineres.setMode("_default_","#cccccc",1,"");
            fraglineres.reApplyMode();
            
            // now lets search for the relevant items...
            if(! ion.seq.match(/Z/)){
               res = v.searchTags("PEPTIDE2 " + iontag);
            }
            else {
               // all of the opposite peptide and the linker!
               res = v.searchTags("PEPTIDE1 AA | LINKER | PEPTIDE2 " + iontag);
            }
            fragres = v.searchTags("PEPTIDE2 FRAG " + iontag);
            
            v.registerModeTag(modename, "PEPTIDE2", iontag);
            
         }
         else {
            alert(ion.pep + " is not what's expected");
         }
         
         if(res != null){
            peak.setAssocItemsHover(modename, res, "SEL");
            // activates the "SEL" mode for all those items!
            
     //       for(var f = 0 ; f < fragres.itemlist.length ; f++ ){
     //          fragres.itemlist[f].setHover("_default_", peak.ftext.id, modename);
     //          fragres.itemlist[f].setHover("_default_", peak.htext.id, modename);
     //          fragres.itemlist[f].setHover("_default_", peak.fline.id, modename);
     //      }
         }   
         else {
            alert("no res");
         }
         
         // add tags to the actual fragment!
         peak.addTags(ion.pep + " " + iontag);
         peak.assocTagMode(ion.pep + " " + iontag, modename);
         
         
/*         
         debug += "<tr>";
         debug += "<td>" + f.alphabeta + "</td>";
         debug += "<td>" + iontag + "</td>";
         debug += "<td>" + peakType + "</td>";
         debug += "<td>" + modename + "</td>";
         debug += "</tr>";
*/         
      }
 //     else if(ion.ismatchedisotope == "isotope"){
 //        // if it's a non-annotated isotope, colour it grey
 //        peak.setDefaultMode("#cccccc", "#cccccc", "#cccccc", "font-size:8pt;");
 //     } 
      else {
         // otherwise, it's not explained... type 0!
         // make mode name...
         var modename = "mode"+peakType; 
         // make the colours:
         var thisColour = colours[peakType];
         var thisRGB = hexColourToRGB(thisColour);
         var thisMuted = thisRGB.lighten().muted2().toHex();
         // set the default and selected modes:
         peak.setDefaultMode(thisMuted, thisMuted, thisMuted, "font-size:8pt;");
         peak.addMode(modename,thisColour,1,"font-size:8pt;");
      }
   }
//   debug += "</table>";
//   document.getElementById("messages").innerHTML = debug;
   return matchedlosses;
}





//////////////////   AUXILIARY ANNOTATOR END   ////////////////// 

















//////////////////   SUPPORTING FUNCTIONS...   ////////////////// 

/// adapted from http://www.bennadel.com/blog/2160-Adding-A-Splice-Method-To-The-Javascript-String-Prototype.htm
String.prototype.splice = function(index,howManyToDelete,stringToInsert){
   var characterArray = this.split( "" );
   Array.prototype.splice.apply(characterArray, arguments);
   return characterArray.join( "" );
};



function parseHeadedTable(t){
   var tab = parseTabTable(t);
   var h = tab.shift();
   for(var i=0; i<h.length; i++){
      h[i] = h[i].scriptify();
   }
   for(var i=0; i<tab.length; i++){
      var o = {};
      for(var j=0; j<h.length; j++){
         o[h[j]] = tab[i][j];
         o[j] = tab[i][j];
      }
      tab[i] = o;
   }
   return tab;
}



String.prototype.scriptify = function(){
   var sa = this.replace(/[^\w\s]/,"").split(/\s+/);
   if(sa[0].substr(1,1).match(/[a-z]/))
      sa[0] = sa[0].toLowerCase();
   for(var i=1; i<sa.length; i++){
      var first = sa[i].substr(0,1);
      sa[i] = first.toUpperCase() + sa[i].substring(1);
   }
   return sa.join("");
};



function parseTabTable(text){
   var rows = text.split(/(?:\r|\n|\r\n|\n\r)/);
   for(var i=0; i<rows.length; i++){
      rows[i] = rows[i].split(/\t/);
   }
   return rows;
}

function logtext(title,text){
	document.getElementById("outputs").value 
      += title + "\n\n" + text + "\n\n";
}

function logtable(title,table){
	var text = title + "\n\n";
	for(var i=0; i<table.length; i++){
		text += table[i].join("\t") + "\n";
	}
	text += "\n\n";
	document.getElementById("outputs").value += text;
}

function loghash(title,hash){
	var text = title + "\n\n";
	for(var k in hash){
		text += k + "\t" + hash[k] + "\n";
	}
	text += "\n\n";
	document.getElementById("outputs").value += text;
}

function loglist(title,table){
	var text = title + "\n\n";
	for(var i=0; i < table.length; i++){
		text += table[i] + "\n";
	}
	text += "\n\n";
	document.getElementById("outputs").value += text;
}

function logLoH(title,listofhashes){
   var text = title + "\n\n";
   text += dumpLoH(listofhashes);
	text += "\n\n";
	document.getElementById("outputs").value += text;
}

function dumpLoH(loh){
   var head = [];
   for(k in loh[0]){
      head.push(k);
   }
   head.sort();
   table = [head.join("\t")];
	for(var i=0; i<loh.length; i++){
      var row = [];
      for(var j=0; j<head.length; j++){
         var v = loh[i][head[j]];
         if(v.toString().match(/^\d+\.\d+/)){
            v = v.toFixed(4);
            v = v.toString();
            while(v.length < 9){
               v = " "+v;
            }
         }
         row.push(v);
      }
      table.push(row.join("\t"));
   }
   return table.join("\n");
}

function alerttab(title,table){
	var text = title + "\n\n";
	for(var i=0; i+1<table.length; i++){
		text += table[i].join("\t") + "\n";
	}
	alert(text);
}


function sorttabbestfirst(a,b){
	// sort first by losses... if b has fewer, it should go first...
	if(a[7] != b[7])
		return a[7] - b[7];
	// then by z deviation if b is lower, it should go first
	return Math.abs(a[9]) - Math.abs(b[9])
}


function copyarray(arr){
	var copy = new Array();
	for(var i=0; i < arr.length; i++){
		copy.push(arr[i]);
	}
	return copy;
}
function appendarray(arr,arr2){
	for(var i=0; i < arr2.length; i++){
		arr.push(arr2[i]);
	}
	return arr;
}



function supNumber(str) {
  if(str.length == 0) {
    return "";
  }
  var table = {
   43 : 0x207A, // +
   45 : 0x207B, // -
   48 : 0x2070, // 0
   49 : 0x00B9, // 1
   50 : 0x00B2, // 2
   51 : 0x00B3, // 3
   52 : 0x2074, // 4
   53 : 0x2075, // 5
   54 : 0x2076, // 6
   55 : 0x2077, // 7
   56 : 0x2078, // 8
   57 : 0x2079, // 9
  };
  var a = str.charCodeAt(0);
  var c = table[a];
  return String.fromCharCode(c + supNumber(str.substring(1))); 
}

















//////////////////   ORIGINAL CODE...   ////////////////// 



// SALMAN, L00K:
// THINGS YOU NEED TO DO WHEN FIRST CREATING THE PAGE

//// moved this bit to aa-app
/*
function onLoad(){
   svg = addSVG();
   di = new jbSvgDrawingInterface(document, svg);
   d = new jbDrawing(di);
   
// SALMAN, L00K:
// THESE ARE THINGS I DO THAT YOU DON'T NEED TO I SUPPOSE
	
	
   colourChecks();
   //makeSpecLinks();
}
*/

// SALMAN, L00K:
// EVERY TIME I LOAD A NEW SPECTRUM I CALL THIS TO DO IT:
function makespectrum(i){
   document.getElementById("waitbox").style.visibility = "inherit";
   setTimeout("makespectrum_stage2("+i+");",200);
// SALMAN, L00K:
// WHICH CALLS THE FUNCTION BELOW, WHICH IS WHERE THE GOOD STUFF HAPPENS...
}


// SALMAN, L00K:
// YOU SHOULD PROBABLY CALL IT DIRECTLY
function makespectrum_stage2(i){



// SALMAN, L00K:
// Remove the old spectrum and set a new viewer:
   d.removeAll();
   v = new jbViewer(d,SVGW - 20, SVGH - 30);
   
   

// SALMAN, L00K:
// LEAVE THIS STUFF HERE FOR THE MOMENT:


// POPS INFO ON A RESIDUE
   v.handleResidueDblClick = function(pep,R){
      alert(pep+" "+R);
   }
   
// MAKES PEAKS HIGHLIGHTED WHEN HOVERING ON FRAGMENTS
   v.fragsensor.mouseover = jimisFragSensor;
   

// SALMAN, L00K:
// THIS IS CRAP I DO ON THE PAGE, YOU CAN PROBABLY REMOVE IT:
   /*
   var te = document.getElementById("title");
   var ae = document.getElementById("ms"+i);
   
   te.innerHTML = "Xi Spectrum Viewer Demo: " + ae.innerHTML;
   
   for(var s=0; s<spectra.length; s++){
      var star = document.getElementById("star"+s);
      if(s==i){
         star.style.visibility = "inherit";
      }
      else {
         star.style.visibility = "hidden";
      }
      
   }
   */
   

// SALMAN, L00K:
// HERE YOU NEED TO MAKE AN ARRAY OF jbSTPeak objects   
// YOU'LL FIND THE jbSTPeak OBJECT AT THE VERY END OF THIS FILE
   var peaks = jbMakeSTPeaks(spectra[i]); // an array of fragment objects
   
// SALMAN, L00K:
// NOW YOU CAN LEAVE THIS AS-IS OF THE MOMENT

   var l1;
   var l2;
   var p1;
   var p2;
   for(var s = 0; s < peaks.length; s++){
      if(peaks[s].LinkerPosition1 + "" != ""){
         l1 = peaks[s].LinkerPosition1;
         l2 = peaks[s].LinkerPosition2;
         p1 = peaks[s].Peptide1;
         p2 = peaks[s].Peptide2;
         break;
      }
   }
   
   
   if(l1 == null)
      l1 = "0";
   if(l2 == null)
      l2 = "0";
   if(p1 == null)
      p1 = "";
   if(p2 == null)
      p2 = "";
   
   
   // write a discriptive string...
      
   var g = d.group(SVGW-20,SVGH-15);
   g.text("customtext1",0,0,"end",0,"grey",
      "font-family:Calibri;font-size:10pt;",
      "Spectrum " + (i+1) + ": " + peaks[0].Run 
         + " #" + peaks[0].ScanNumber
         + " (" + peaks[0].crosslinker + ")");
   
   g.text("customtext2",0,-13,"end",0,"grey",
      "font-family:Calibri;font-size:8pt;",
      peaks[0].FastaHeader1 + ": " + peaks[0].Peptide1);
   
   g.text("customtext3",0,-26,"end",0,"grey",
      "font-family:Calibri;font-size:8pt;",
      peaks[0].FastaHeader2 + ": " + peaks[0].Peptide2);
   
// can check the width if necessary:
//   var box = g.getBBox();
   
   
   // set up peptides.
   setPeps(p1,p2,l1,l2);
   makePeaks(peaks);
   hookEvents(svg,v);
   v.autoZoom();
      
   v.specsensor.clickCallback = function(p){
      var v = JBGLOBALEVENTMANAGER.viewer;
      var a = v.spec.activeArea();
	if(p.y < a.y){
		// zoom
		var mz = v.zoomer.xToMz(p.x);
		var middle = v.zoomer.b/2 + v.zoomer.e/2;
		var diff = mz - middle;
		v.zoomer.doZoom(v.zoomer.b + diff, v.zoomer.e + diff);
	}
      else {
	      // measure
         var disregardAnnotationStatus = 1; // set to null and see what happens
         var nearestPeak
            = JBGLOBALEVENTMANAGER.viewer.zoomer.findNearest(p.x, p.y, 
                 disregardAnnotationStatus);
         alert(nearestPeak[0].peak.userData);
      }
   }
   
   
// SALMAN, L00K:
// THIS IS MY WAIT-BOX... YOU CAN GET RID OF THIS!
   
   document.body.style.cursor = "default";
   document.getElementById("waitbox").style.visibility = "hidden";
   
   handleChecks(); // look up the checkboxes' values and act on them!
   v.zoomer.afterZoomCallback = handleChecks;
   
}

function resetSonsor(){
   
   v.specsensor.clickCallback = function(p){
      var v = JBGLOBALEVENTMANAGER.viewer;
      var a = v.spec.activeArea();
	if(p.y < a.y){
		// zoom
		var mz = v.zoomer.xToMz(p.x);
		var middle = v.zoomer.b/2 + v.zoomer.e/2;
		var diff = mz - middle;
		v.zoomer.doZoom(v.zoomer.b + diff, v.zoomer.e + diff);
	}
      else {
	      // measure
         var disregardAnnotationStatus = 1; // set to null and see what happens
         var nearestPeak
            = JBGLOBALEVENTMANAGER.viewer.zoomer.findNearest(p.x, p.y, 
                 disregardAnnotationStatus);
         alert(nearestPeak[0].peak.userData + "\n\n(peak nearest to where you clicked)");
      }
   }
}


function resetSVG(){
   
   d.removeAll();
   v = new jbViewer(d,SVGW - 20, SVGH - 30);
   
// POPS INFO ON A RESIDUE
   v.handleResidueDblClick = function(pep,R){
      alert(pep+" "+R);
   }
   
// MAKES PEAKS HIGHLIGHTED WHEN HOVERING ON FRAGMENTS
   	v.fragsensor.mouseover = jimisFragSensor;
}


function jimisFragSensor(viewer, frags, ab, pep, ii){

	var modes = new Array();

    for(var j=0; j<frags.length; j++){
    	var frag = frags[j];
    	var tags = frag.vert.tags;
	    var Bn = tags.replace(/^.*\b(L\d+)\b.*$/g,"$1");
	    var Yn = tags.replace(/^.*\b(R\d+)\b.*$/g,"$1");


        if(frag.vert.modes["_default_"].visible){
           	frag.vert.changeMode("SEL");
           	if(frag.left.modes["_default_"].visible)
              	frag.left.changeMode("SEL");
           	if(frag.right.modes["_default_"].visible)
              	frag.right.changeMode("SEL");

            modes.push(ab+" "+Bn);
            modes.push(ab+" "+Yn);
        }
    }
    
    var res = viewer.searchTags("PEAK");
   	for(var i=0; i < res.itemlist.length ; i++){
   		// each peak ... does it have any of these modes? if not, set default
    	var hasMode = false;
    	for(var j=0; j<modes.length; j++){
    		var atms = res.itemlist[i].parent.assoctagmodes;
    		var jatm = atms[modes[j]];
    		if(jatm != null && ! hasMode){
    			res.itemlist[i].changeMode(jatm);
    			hasMode = true;
    		}
    	}
    	if(! hasMode)
    		res.itemlist[i].changeMode("_default_");
   	}
           
};






var SKIPHIDE = 0;

function hideOverlappingSubordinates(){
	
   if(SKIPHIDE)
      return;
      
	var ftextscoll = v.searchTags("PEAK FIXEDTEXT");
	ftextscoll.itemlist.sort(sortItemsByParentIC); // should now be most intense first.
	var shownBBoxes = new Array(); // things that we're allowing to be shown get registered here.
   
   
   
	for(var i=0; i<ftextscoll.itemlist.length; i++){ 
		var item = ftextscoll.itemlist[i];
		var peak = item.parent;
		var bbox = item.getBBox();
		if(bbox.width < 0.1 || bbox.height < 0.1 || bbox.x < 0.1 || bbox.y < 0.1){
			// it's too small or probably not being shown currently
		}
		else if(item.currentmode == "_default_"){
		}
		else {
			var intersect = false;
			for(var j=0; j<shownBBoxes.length; j++){				
				
				/*
					// have a sneaky peek..
					var bbox2 = shownBBoxes[j];
					var r1 = v.d.rect("TEMPRECT1",bbox.x,bbox.y,bbox.width,bbox.height,"black","white",2);
					var r2 = v.d.rect("TEMPRECT2",bbox2.x,bbox2.y,bbox2.width,bbox2.height,"red","yellow",2);
					//alert([peak.fixedtext, item.currentmode]);
					v.d.remove(r2);
					v.d.remove(r1);
					// end of sneaky peek code.
				*/
				
				
				if( bboxesIntersect(shownBBoxes[j], bbox)){
					
					intersect = true;
					break;
				}
			}
			if(intersect){ // hide it
				item.changeMode("_default_");
			}
			else { // add it to the visible list
				shownBBoxes.push(bbox);
			}
		}
	}
}

function sortItemsByParentIC(a,b){
	return b.parent.ic - a.parent.ic;
}

function bboxesIntersect(A,B){
	A.x2 = A.x + A.width;
	A.y2 = A.y + A.height;
	B.x2 = B.x + B.width;
	B.y2 = B.y + B.height;
	if(
		( 
			(A.x < B.x && B.x < A.x2) // Ax1 < Bx1 < Ax2                 x1 of B is within A
		   ||	                                                                                 // OR
			(B.x < A.x && A.x < B.x2) // Bx1 < Ax1 < Bx2            x1 of A is within B
		) 
	  &&                                                                                          // AND
		(
			(A.y <= B.y && B.y <= A.y2) // Ay1 < By1 < Ay2                   y1 of B is within A
		   ||	                                                                                  // OR
			(B.y <= A.y && A.y <= B.y2) // By1 < Ay1 < By2              y1 of A is within B
		)
	){
		return true;
	}
	else {
		return false;
	}
}


// SALMAN, L00K:
// THIS REGISTERS MOUSE EVENTS... AND THE KEYBOARD!
function hookEvents(svg,v){
   svg.onmousemove = v.eventmanager.mousemover;
   svg.onmousedown = v.eventmanager.mousedown;
   svg.onmouseup = v.eventmanager.mouseup;
   
   if(window.addEventListener)
        document.addEventListener('DOMMouseScroll',  v.eventmanager.mousescroll, false);
 
   document.onmousewheel = v.eventmanager.mousescroll;
   /*window.onkeypress = function(e){
      if(e.type == "keypress" && e.charCode == 101)
         exportSVG();
   }*/
}



// SALMAN, L00K:
// DO THIS WHEN A CHECKBOX IS CHECKED
function handleChecks(){
   var modenames = new Array(); // WE'LL COLLECT THE INDICES OF THE NAMES FOR WHICH BOXES ARE CHECKED...
   for(var i=0; i<names.length; i++){
      var check = document.getElementById(names[i]);
      if(check.checked == true)
         modenames.push("mode"+i); // e.g. mode0 is for "other" peaks, mode1 is for "beta b-ion" peaks, etc
   }
   // EACH INDEX CORRESPONDS TO A MODE, SO NOW WE ACTIVATE THOSE MODES...
   v.modeScanAll(modenames); // THIS LIGHTS UP THE RELEVANT PEAKS IN THE APPROPRIATE COLOURS
   
		hideOverlappingSubordinates();
}





// SALMAN, L00K:
// PROBABLY DON'T TOUCH THE REST OF THIS SCRIPT!






// SOME COLOUR FUNCTIONS:

var COLOURS = new Array();

function randomColour(){
   var r,g,b;
   var maxdiff = 0;
   var mincloseness = 100;
   var intensity = 0;
   while(maxdiff < 100 || mincloseness < 40 || intensity > 150){
      r = parseInt(Math.random()*255);
      g = parseInt(Math.random()*255);
      b = parseInt(Math.random()*255);
      maxdiff = Math.max(r,g,b) - Math.min(r,g,b);
      intensity = Math.max(r,g,b);
      brightness = (r+g+b)/3;
      for(var i=0; i< COLOURS.length; i++){
         var o = COLOURS[i];
         var diff = Math.abs(o.r - r) + Math.abs(o.g - g) + Math.abs(o.b - b);
         if(i==0)
            mincloseness = diff;
         else if(diff < mincloseness)
            mincloseness = diff;
      }
   }
   var c = [r,g,b].join(",");
   var o = new Object();
   o.r = r;
   o.g = g;
   o.b = b;
   COLOURS.push(o);
   return "rgb("+c+")";
}







// MAKE SOME LINKS THAT WILL CREATE EACH SPECTRUM
function makeSpecLinks(){
   
   var sld = document.getElementById("speclinks");
   for(var i = 0 ; i < spectra.length ; i ++){
      var c = randomColour();
      sld.innerHTML += "<span id=\"star"+i+"\" style=\"color:"+c+";\" class=\"speca spec0 hidey\">*</span> <a id=\"ms"+i+"\" style=\"color:"+c+";\" class=\"speca spec0\" href=\"javascript:makespectrum("+i+");\">Spectrum "+(i+1)+"</a><br/>";
   }
   //<span id="star1" class="speca spec1">*</span> <a id="ms1" class="speca spec1" href="javascript:makespectrum(1);">Spectrum 2 (ok)</a><br/>
   //<span id="star2" class="speca spec2">*</span> <a id="ms2" class="speca spec2" href="javascript:makespectrum(2);">Spectrum 3 (poor)</a><br/>
}







// GIVE SOME INFO ON A PEAK
function GrabPeakInfo(sbItemArray){
   var peak = sbItemArray[0].parent;
   var wanted = new Array("mz","ic","fixedtext","hovertext","userData");
   var mesg = "";
   for( var i = 0; i < wanted.length ; i ++ ){
      mesg += wanted[i]+"="+peak[wanted[i]]+"\n";
   }
   return mesg;
}





// POP UP SOME HELP
function help(){
   alert(
        "HELP\n\nDrag on the spectrum to measure m/z.\n"
      + "Drag on the x-axis to zoom in.\n"
      + "Click on the x-axis to re-centre.\n"
      + "Click on the zoom icons to zoom in and out and to undo zooms.\n"
      + "Check the ion-types to view the peaks of that type on the spectrum.\n"
      + "Mouse over peaks to see what fragments they relate to.\n"
      + "Click peaks for further information.\n"
      + "Mouse over fragments to see what peaks they relate to.\n"
      + "Click on a fragment to freeze the peaks it relates to so you can further investigate.\n"
      + "            (click it again to cancel)\n"
      + "Click on the export icon, or hit 'e' key to export SVG\n"
   );
}

// SALMAN, L00K:
// THIS IS HOW I SET PEPTIDES.  YOU CALL THIS FROM makespectrum_stage2
function setPeps(p1,p2,l1,l2){
   var pp = v.setPeptides(p1,p2,l1,l2,30, v.h - 30);
   var pep1items = v.searchTags("PEPTIDE1");
   var pep2items = v.searchTags("PEPTIDE2");
   var linkeritems = v.searchTags("LINKER");
   pep1items.addMode("SEL","#ff0000",1,"INHERIT");
   pep2items.addMode("SEL","#0000ff",1,"INHERIT");
   linkeritems.addMode("SEL","#00ff00",1,"INHERIT");

}



//MORE COLOUR FUNCTIONS
function hexColourToRGB(hex){
   if(hex == null){
      alert("hex is undefined!");
      return new jbRGB(0,0,0);
      console.trace();
   }
   var hexh = hex.replace(/#/g, "");
   var rhex = hexh.substring(0,2);
   var ghex = hexh.substring(2,4);
   var bhex = hexh.substring(4,6);
   var r = h2d(rhex);
   var g = h2d(ghex);
   var b = h2d(bhex);
   return new jbRGB(r,g,b);
}

function jbRGB(r,g,b){
   this.r = r;
   this.g = g;
   this.b = b;
}
jbRGB.prototype.toHex = function(){
   return "#" + d2h(this.r) + d2h(this.g) + d2h(this.b);
}
jbRGB.prototype.greyed = function(){
   var mean = parseInt((this.r+ this.g + this.b)/3);
   return new jbRGB(mean,mean,mean);
}
jbRGB.prototype.lighten = function(){
   var white = new jbRGB(255,255,255);
   return this.average(white);
}
jbRGB.prototype.darken = function(){
   var white = new jbRGB(0,0,0);
   return this.average(white);
}
jbRGB.prototype.average = function(anotherRGB){
   return new jbRGB(
      parseInt((this.r + anotherRGB.r)/2), 
      parseInt((this.g + anotherRGB.g)/2), 
      parseInt((this.b + anotherRGB.b)/2)
   );
}
jbRGB.prototype.muted = function(){
   var grey = this.greyed();
   return this.average(grey);
}
jbRGB.prototype.muted2 = function(){
   var grey = this.greyed();
   var muted = this.muted();
   return grey.average(muted);
}

function d2h(d) {
   var h = d.toString(16); 
   while(h.length < 2){
      h = "0"+h;
   } 
   return h;
}
function h2d(h) {
   return parseInt(h,16);
} 


/*
var test = hexColourToRGB("#ff9900");
var grey = test.greyed();
var mute = test.average(grey);
alert(test.toHex() + grey.toHex() + mute.toHex());
*/


// COLOUR IN THE CHECKBOXES

function colourChecks(){
   for(var i=0; i<names.length; i++){
      var checklabel = document.getElementById(names[i]+"label");
      checklabel.style.color = hexColourToRGB(colours[i]).darken().toHex(); // other!
      var check = document.getElementById(names[i]);
   }
}







// IMPORTANT FUNCTION THAT ACTUALLY DOES A LOT...
function makePeaks(peaks){
   var frago = new Object();  // a fragment "hash table"... keyed by m/z
   
   // frags
   // this has the following properties:
      
   // Run ScanNumber crosslinker FastaHeader1 Peptide1 FastaHeader2 Peptide2
   // MatchedPeptide alphabeta LinkerPosition1 LinkerPostion2 FragmentName
   // Fragment NeutralMass Charge CalcMZ ExpMZ MS2Error IsotopPeak Description
   // virtual BasePeak AbsoluteIntesity RelativeIntensity IsPrimaryMatch Dashes
  
  
//   var debug = "<table><tr><th>f.alphabeta</th><th>iontag</th><th>peakType</th><th>modename</th></tr>";
  
   // I've sorted the peaks so the unexplained peaks are painted first
   // and the primary matches come next.  So I don't handle that stuff here.
   for ( var i = 0 ; i < peaks.length ; i++){
      
      // this fragment:
      var f = peaks[i];
   
      var fixedtext = ""; // text that will be displayed in the selected state
      var hovertext = ""; // text that will be displayed on-hover in the selected state
      var iontype = ""; // what type of ion is this?
      var iontag = ""; // tags that can be use to search for this peak
      var ionnumber = 0; // e.g. the 8 in y8
      var peplength = f.alphabeta == "alpha" ? f.Peptide1.length : f.Peptide2.length;
                        // length of the peptide that fragmented
      

      // beware!  we'll only use the first text found in this case...
      // maybe later I'll plumb in some extra hover text...
      
      
      // have we seen this peak before?  if not, make one:
      if(frago[f.ExpMZ] == null)
         frago[f.ExpMZ] = v.addPeak(f.ExpMZ, f.AbsoluteIntesity, f.FragmentName, hovertext);
         
      var peak = frago[f.ExpMZ]; // look up this peak
      
      
      
      // type of peak...
      // 0:other  1:A-b  2:A-y  3:B-b  4:B-y  5:A-b(s)  6:A-y(s)  7:B-b(s)  8:B-y(s)  
      var peakType = 0;
      

      if(f.FragmentName){ // if it has this then it must be annotated
         // grab some info...
         iontype = f.FragmentName.substring(0,1);
         ionnumber = f.FragmentName.replace(/^\w(\d+).*$/g, "$1");
         iontag = iontype.match(/[abc]/) ? "B"+ionnumber : "Y"+ionnumber;
         //ppm:
         var ppm = 1000000 * parseFloat(f.MS2Error) / f.ExpMZ;
	      
	hovertext = f.Fragment;
	hovertext += "; "+f.IsotopPeakNo;
	
        hovertext += "; "+ppm.toFixed(1)+" ppm";
                  
         peakType = iontype.match(/[abc]/) ? 1 : 2;
         if(f.alphabeta == "beta"){
            peakType += 2;
         }
         if(f.FragmentName.match(/_/)){
            peakType += 4;
         }
         // so now the following should hold:
         // 0:other  1:A-b  2:A-y  3:B-b  4:B-y  5:A-b(s)  6:A-y(s)  7:B-b(s)  8:B-y(s)  
           
         // MODE NAME:
         // we create a mode-name base on the type of ion...
         var modename = "mode"+peakType; // makes sense!
         var hovername = "hover"+peakType; // oh yeah!
         
         // add this text (so text can change!)
         peak.addModeFText(modename,f.FragmentName);
         peak.addModeFText(hovername,f.FragmentName);
         peak.addModeHText(modename,hovertext);
         peak.addModeHText(hovername,hovertext);
         
         // colours:
         // this colour object is nice :-)
         var thisColour = colours[peakType];
         var thisRGB = hexColourToRGB(thisColour);
         var thisLightened = thisRGB.lighten().toHex();
         var thisDarkened = thisRGB.darken().toHex();
         var thisMuted = thisRGB.muted().toHex();
         var thisMuted2 = thisRGB.lighten().muted2().toHex();
         
         
         // Default mode... the one that everybody start off in...
         peak.setDefaultMode(thisMuted2, thisMuted2, thisMuted2, "font-size:8pt;");
         peak.setFTextMode("_default_","#000000",0,""); // don't display text!
	 
         // Selected mode... when you select the peaks
         peak.addMode(modename,thisColour,1,"font-size:8pt;");
         // make this peak go into "hover" mode when we hover on it from this mode
         peak.setAssocPeakHover(modename, peak, hovername); 
         peak.setHTextMode(modename,"#000000",0,""); // don't display hover text!
         if(f.IsPrimaryMatch != "")
            peak.setModePrimaryFlag(modename, f.IsPrimaryMatch);
         
         // Hover mode...  defined the hover mode we want
         peak.addMode(hovername, thisDarkened, 1, "font-size:8pt;");
         
         // SETUP MODE-HOVER
         var res = null; // results from a searchTags search...
         var fragres = null;
         
         if(f.alphabeta == "alpha"){ // alpha peptide
            // firstly, let's make the associates fragment lines visible and grey...
            var fraglineres = v.searchTags("PEPTIDE1 FRAG " + iontag);
            fraglineres.setMode("_default_","#cccccc",1,"");
            fraglineres.reApplyMode();
            
            // now lets search for the relevant items...
            if(f.Description.match(/linear/)){
               res = v.searchTags("PEPTIDE1 " + iontag);
            }
            else if(f.Description.match(/crosslinked/)){
               // all of the opposite peptide and the linker!
               res = v.searchTags("PEPTIDE2 AA | LINKER | PEPTIDE1 " + iontag);
            }
            fragres = v.searchTags("PEPTIDE1 FRAG " + iontag);
            
            v.registerModeTag(modename, "PEPTIDE1", iontag);
         }
         else if(f.alphabeta == "beta"){ // beta peptide
            // firstly, let's make the associates fragment lines visible and grey...
            var fraglineres = v.searchTags("PEPTIDE2 FRAG " + iontag);
            fraglineres.setMode("_default_","#cccccc",1,"");
            fraglineres.reApplyMode();
            
            // now lets search for the relevant items...
            if(f.Description.match(/linear/)){
               res = v.searchTags("PEPTIDE2 " + iontag);
            }
            else if(f.Description.match(/crosslinked/)){
               // all of the opposite peptide and the linker!
               res = v.searchTags("PEPTIDE1 AA | LINKER | PEPTIDE2 " + iontag);
            }
            fragres = v.searchTags("PEPTIDE2 FRAG " + iontag);
            
            v.registerModeTag(modename, "PEPTIDE2", iontag);
            
         }
         
         if(res != null){
            peak.setAssocItemsHover(modename, res, "SEL");
            // activates the "SEL" mode for all those items!
            
      //      for(var f = 0 ; f < fragres.itemlist.length ; f++ ){
      //         fragres.itemlist[f].setHover("_default_", peak.ftext.id, modename);
      //         fragres.itemlist[f].setHover("_default_", peak.htext.id, modename);
       //        fragres.itemlist[f].setHover("_default_", peak.fline.id, modename);
       //     }
         }   
         
         // add tags to the actual fragment!
         peak.addTags(f.alphabeta + " " + iontag);
         peak.assocTagMode(f.alphabeta + " " + iontag, modename);
         
         
/*         
         debug += "<tr>";
         debug += "<td>" + f.alphabeta + "</td>";
         debug += "<td>" + iontag + "</td>";
         debug += "<td>" + peakType + "</td>";
         debug += "<td>" + modename + "</td>";
         debug += "</tr>";
*/         
      }
      else if(f.IsotopPeak == "isotope"){
         // if it's a non-annotated isotope, colour it grey
         peak.setDefaultMode("#cccccc", "#cccccc", "#cccccc", "font-size:8pt;");
      } 
      else {
         // otherwise, it's not explained... type 0!
         // make mode name...
         var modename = "mode"+peakType; 
         // make the colours:
         var thisColour = colours[peakType];
         var thisRGB = hexColourToRGB(thisColour);
         var thisMuted = thisRGB.lighten().muted2().toHex();
         // set the default and selected modes:
         peak.setDefaultMode(thisMuted, thisMuted, thisMuted, "font-size:8pt;");
         peak.addMode(modename,thisColour,1,"font-size:8pt;");
      }
   }
//   debug += "</table>";
//   document.getElementById("messages").innerHTML = debug;
}




// spectrum reading...


function jbMakeSTPeaks(table){
   var stFrags = new Array();
   for(var i = 0; i < table.length; i++){
      var tableSlice = table[i];
      stFrags.push(new jbSTPeak(tableSlice));
   }
   return stFrags;
}





// SALMAN L00K:
// jbSTPeak OBJECT... give it an array of !STRINGS! with the following items
// in the same order...
// OR
// Create your own Object() that has these properties... but note the types below...
// IF YOU DON'T KNOW WHAT THE FIELDS ARE, ASK LUTZ!

   // Run ScanNumber crosslinker FastaHeader1 Peptide1 FastaHeader2 Peptide2
   // MatchedPeptide alphabeta LinkerPosition1 LinkerPostion2 FragmentName
   // Fragment NeutralMass Charge CalcMZ ExpMZ MS2Error IsotopPeak Description
   // virtual BasePeak AbsoluteIntesity RelativeIntensity IsPrimaryMatch Dashes
  
function jbSTPeak (tableSlice){ // meaning row, really.
   var i = 0;
   // All strings at input and remain so unless otherwise stated...
   this.Run = tableSlice[i++];
   this.ScanNumber = parseInt(tableSlice[i++]);
   this.crosslinker = tableSlice[i++];
   this.FastaHeader1 = tableSlice[i++];
   this.Peptide1 = tableSlice[i++];
   this.FastaHeader2 = tableSlice[i++];
   this.Peptide2 = tableSlice[i++];
   this.MatchedPeptide = tableSlice[i++];
   this.alphabeta = tableSlice[i++];
   // LinkerPositions are Integers!
   this.LinkerPosition1 =  tableSlice[i] + "" == "" ? "" : parseInt(tableSlice[i]);
   i++;
   this.LinkerPosition2 =  tableSlice[i] + "" == "" ? "" : parseInt(tableSlice[i]);
   i++;
   this.FragmentName = tableSlice[i++];
   this.Fragment = tableSlice[i++];
   this.NeutralMass = tableSlice[i++];
   this.Charge = tableSlice[i++];
   // CalcMZ, ExpMZ and MS2Error are Floats!
   this.CalcMZ = tableSlice[i] == "" ? "" : parseFloat(tableSlice[i]);
   i++;
   this.ExpMZ = tableSlice[i] == "" ? "" : parseFloat(tableSlice[i]);
   i++;
   this.MS2Error = tableSlice[i] == "" ? "" : parseFloat(tableSlice[i]);
   i++;
   
   this.IsotopPeak = tableSlice[i++];
   this.IsotopPeakNo = tableSlice[i++];
   this.Description = tableSlice[i++];
   if(this.IsotopPeak > 0)
      alert([this.CalcMZ,this.ExpMZ,this.MS2Error,this.IsotopPeak],this.Description);
	
   this.virtual = tableSlice[i++];
   this.BasePeak = tableSlice[i++];
   // AbsoluteIntesity and RelativeIntensity are Floats!   
   this.AbsoluteIntesity = tableSlice[i] == "" ? "" : parseFloat(tableSlice[i]);
   i++;
   this.RelativeIntensity = tableSlice[i] == "" ? "" : parseFloat(tableSlice[i]);
   i++;
   // IsPrimaryMatch is an Integer!
   this.IsPrimaryMatch =  tableSlice[i] == "" ? "" : parseInt(tableSlice[i]);
   i++;
   this.Dashes = tableSlice[i++]; // whatever these are for?!
}


/// from http://stackoverflow.com/questions/281264/remove-empty-elements-from-an-array-in-javascript


Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {         
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

