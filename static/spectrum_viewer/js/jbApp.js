 
/* 
	jbApp


item classes that can be styled:

div.jbApp
div.jbAppFrame
td.jbAppEditTableCell
table.jbAppEditTable

*/




// Add a new app to an existing div:
Element.prototype.jbApp = function(){
	return new jbApp(this);
};

// new app!
function jbApp(e){
	this.e = e; // in every item the html element with be known as e
	this.e.setAttribute("class","jbApp");
	this.uidc = 0;
	this.top = this;
	this.parent = null;
	this.children = new Array();
	this.maxz = 10;
	this.winxy = 50;
}
// this we can do with an app...
jbApp.prototype.Frame = function(){
	return new jbAppFrame(this);
};
jbApp.prototype.UID = function(){
	return "jbAppElement_" + this.uidc++;
};
jbApp.prototype.nextXY = function(){
	this.winxy += 15;
	if(this.winxy > 200)
		this.winxy = 65;
	return this.winxy;
}

function hideElement(id){
	document.getElementById(id).style.display = "none";
}

var THEMOVED = null;
var PREVIOUSMOUSE = null;
var MOVEDSTARTX = 0;
var MOVEDSTARTY = 0;

function MouseWhere(e){	
	var posx = 0;
	var posy = 0;
	if (!e) e = window.event || window.Event;
	if (e.pageX || e.pageY) 	{
		posx = e.pageX;
		posy = e.pageY;
	}
	else if (e.clientX || e.clientY) 	{
		posx = e.clientX + document.body.scrollLeft
			+ document.documentElement.scrollLeft;
		posy = e.clientY + document.body.scrollTop
			+ document.documentElement.scrollTop;
	}
	else {
		alert("ERROR!");
	}
	// posx and posy contain the mouse position relative to the document
	// Do something with this information
	return {"x":posx,"y":posy};
}

function moverhook(evt){
	var e = evt.target;
	if(PREVIOUSMOUSE != null) return;
	// e is the anchor inside the element belonging to the window.
	THEMOVED = e.parentNode;
	PREVIOUSMOUSE = {"onmousemove":document.body.onmousemove,"onmouseup":document.body.onmouseup};
	document.body.onmousemove = movedmover;
	document.body.onmouseup = moverunhook;
	var point = MouseWhere(evt);
	MOVEDSTARTX = point.x;
	MOVEDSTARTY = point.y;
	e.appWindow.toTop();
}
function movedmover(evt){
	var point = MouseWhere(evt);
	var dx = point.x - MOVEDSTARTX;
	var dy = point.y - MOVEDSTARTY;
	THEMOVED.style.left = (parseInt(THEMOVED.style.left) + parseInt(dx)) + "px";
	THEMOVED.style.top = (parseInt(THEMOVED.style.top) + parseInt(dy)) + "px";
	MOVEDSTARTX = point.x;
	MOVEDSTARTY	= point.y;
}
function moverunhook(evt){
	THEMOVED = null;
	if (PREVIOUSMOUSE == null) return;
	document.body.onmousemove = PREVIOUSMOUSE.onmousemove;
	document.body.onmouseup = PREVIOUSMOUSE.onmouseup;
	PREVIOUSMOUSE = null;
}

// window

function jbAppWindow(p,title,content){
	this.parent = p;
	this.top = p.top;
	this.title = title ? title : "";
	this.id = this.top.UID();
	this.aid = this.top.UID();
	this.pid = this.top.UID();
	this.w = this.parent.e.addElement(
		"div", {
			"class" : "jbAppWindow",
			"id" : this.id,
			"style" : "position:absolute; display: none; top: 0; left: 0;",
		}, 
		[
			{"type":"div", "params":{"class":"jbAppWindowTitle"}, "content":
				[
					{"type" : "a",
					"params" : {"id": this.id+"Closer","href":"#","style":"float:right;"},
					"content":"_"}
				]
			},
			{"type":"a", "params":{"class":"jbAppWindowTitle","style":"display:block;","id":this.aid}, "content":title},
			{"type":"div", "params":{"class":"jbAppWindowPane","style":"clear:both;","id":this.pid}, "content":content},
		]
	);
   this.w.jbAppWindow = this;
	this.pane = document.getElementById(this.pid);
	this.e = this.pane;
	this.a = document.getElementById(this.aid);
	this.a.appWindow = this;
	this.a.onmouseup = moverunhook;
	this.a.onmousedown = moverhook;
	this.parent.children.push(this);
	this.children = new Array();
}

jbAppWindow.prototype.setCloser = function(togglefunction){
   document.getElementById(this.id+"Closer").setAttribute("onclick", togglefunction);
};
jbAppWindow.prototype.show = function(){
	this.w.style.display = 'block';
	this.toTop();
};
jbAppWindow.prototype.showRandomPlace = function(){
	this.w.style.display = 'block';
	var xy = this.top.nextXY();
	this.w.style.top = xy+"px";
	this.w.style.left = xy+"px";
	this.toTop();
};
jbAppWindow.prototype.showAt = function(x,y){
	this.w.style.display = 'block';
	this.w.style.top = y+"px";
	this.w.style.left = x+"px";
	this.toTop();
};
jbAppWindow.prototype.at = function(x,y){
	this.w.style.top = y+"px";
	this.w.style.left = x+"px";
};
jbAppWindow.prototype.hide = function(){
	this.w.style.display = 'none';
};
jbAppWindow.prototype.toTop = function(){
	this.top.maxz ++;
	this.w.style.zIndex = this.top.maxz;
};
jbAppWindow.prototype.clear = function(){
	this.e.clearElement();
};

// things we can do with a window...
jbAppWindow.prototype.EditTable = function(){
	return new jbAppEditTable(this);
};
jbAppWindow.prototype.TableFrameButton = function(title){
	return new jbAppTableFrameButton(this,title);
};
jbAppWindow.prototype.FrameButton = function(title){
	return new jbAppFrameButton(this,title);
};
jbAppWindow.prototype.Menu = function(title){
	return new jbAppMenu(this,title);
};
jbAppWindow.prototype.TabFrame = function(title,orient){
	return new jbAppTabFrame(this,title,orient);
};

// tabframe...
function jbAppTabFrame(p, title, orient){
	this.parent = p;
	this.top = p.top;
   this.orient = orient;
	this.title = title ? title : "";
	this.id = this.top.UID();
	this.e = this.parent.e.addElement(
		"div", {
			"class" : "jbAppTabFrame",
			"id" : this.id,
		}, title);
   this.e.jbAppTabFrame = this;
	this.parent.children.push(this);
   this.labeldiv = this.e.addElement("div", {"class" : "jbAppTabFrameLabels jbAppTab"+orient});
   this.contentdiv = this.e.addElement("div", {"class" : "jbAppTabFrameContent"});
   this.activeclass = "jbAppTab"+orient+"Active";
}
jbAppTabFrame.prototype.addTab = function(label, element){
   this.contentdiv.appendChild(element);
   element.show = function(){jbAppTabEvent('"+this.e.id+"', '"+element.id+"');};
   var classes = "jbAppTab"+this.orient;
   if(this.contentdiv.children.length == 1)
      classes += "Active";
   
   this.labeldiv.addElement("a",{
         "id":element.id+"TabA",    "href":"#",
         "onclick":"jbAppTabEvent('"+this.e.id+"', '"+element.id+"');",
         "class":classes},
         label);
   
   if(this.contentdiv.children.length == 1)
      element.style.display = "block";
   else 
      element.style.display = "none";
};


// global tab function:
function jbAppTabEvent(tabframeid, tabcontentid){
   var tf = document.getElementById(tabframeid).jbAppTabFrame;
   var ac = tf.activeclass;
   for(var i=0; i<tf.contentdiv.children.length; i++){
      tf.contentdiv.children[i].style.display = tf.contentdiv.children[i].id == tabcontentid ? "block" : "none";
   }
   for(var i=0; i<tf.labeldiv.children.length; i++){
      var classes = tf.labeldiv.children[i].getAttribute("class");
      classes = classes.replace(ac,"");
      tf.labeldiv.children[i].setAttribute("class", 
            classes +
            tf.labeldiv.children[i].id == tabcontentid + "TabA" ? ac : ""
      );
   }
}

// frame
function jbAppFrame(p,title){
	this.parent = p;
	this.top = p.top;
	this.title = title ? title : "";
	this.id = this.top.UID();
	this.e = this.parent.e.addElement(
		"div", {
			"class" : "jbAppFrame",
			"id" : this.id,
		}, title);
	this.parent.children.push(this);
	this.children = new Array();
}
// things we can do with a frame...
jbAppFrame.prototype.EditTable = function(){
	return new jbAppEditTable(this);
};
jbAppFrame.prototype.TableFrameButton = function(title){
	return new jbAppTableFrameButton(this,title);
};
jbAppFrame.prototype.FrameButton = function(title){
	return new jbAppFrameButton(this,title);
};
jbAppFrame.prototype.Menu = function(title){
	return new jbAppMenu(this,title);
};




function menuStructToHTML(p,e,struct){
	for(k in struct){
		var v = struct[k];
		var id = p.top.UID();
		var attr = {"id":id};
		if(k.match(/^>/)){
			k = k.replace(/^>/, '');
			attr["class"] = "right";
		}
		var li = e.addElement("li",attr,k);
		
		if(typeof(v) == "object"){
			if(v.length){
				var id = p.top.UID();
				var ul = li.addElement("ul",{"id":id},v);
			}
			else {
				var id = p.top.UID();
				var ul = li.addElement("ul",{"id":id},null);
				menuStructToHTML(p,ul,v);
			}
		}
		else if(typeof(v) == "function"){
			li.onclick = v;
		}
	}
}

// menu
function jbAppMenu(p,menustruct){
	this.parent = p;
	this.top = p.top;
	this.id = this.top.UID();
	this.e = this.parent.e.addElement(
		"ul", {
			"class" : "menu",
			"id" : this.id,
		}, null);
	this.parent.children.push(this);
	this.children = new Array();
	menuStructToHTML(p,this.e,menustruct);
}
jbAppMenu.prototype.TableFrameButton = function(title){
	return new jbAppTableFrameButton(this,title);
};
jbAppMenu.prototype.FrameButton = function(title){
	return new jbAppFrameButton(this,title);
};



// framebutton
function jbAppFrameButton(p,title){
	this.title = title ? title : "[]";
	this.parent = p;
	this.top = p.top;
	this.id = this.top.UID();
	this.e = this.parent.e.addElement("a", {
		"class" : "jbAppFrameButton",
		"id" : this.id,
		}, this.title);
	this.e.onclick = jbAppFrameButtonShow;
	this.e.jbAppFrameButton = this;
	this.parent.children.push(this);
	this.children = new Array();
	this.callback = null;
	this.frame = new jbAppFrame(this);
	//this.frame.e.jbAppFrame = this.frame;
	this.frame.e.style.display = "none";
}
function jbAppFrameButtonShow(ev){
	var e = ev.target;
	var bbox = e.getBBox();
	var fd = e.jbAppFrameButton.frame.e; // frame div
	fd.style.position = "absolute";
	fd.style.top = (bbox.y2 - 2) + "px";
	fd.style.left = bbox.x + "px";
	if(fd.style.display == "block")
		fd.style.display = "none";
	else
		fd.style.display = "block";
}


// tableframebutton
function jbAppTableFrameButton(p,title){
	this.title = title ? title : "[]";
	this.parent = p;
	this.top = p.top;
	this.id = this.top.UID();
	this.e = this.parent.e.addElement("span",{},"");
	this.ea = this.e.addElement("a", {
		"class" : "jbAppFrameButton",
		"id" : this.id,
		}, this.title);
	this.ea.onclick = jbAppTableFrameButtonShow;
	this.ea.jbAppTableFrameButton = this;
	this.parent.children.push(this);
	this.children = new Array();
	this.callback = null;
}
jbAppTableFrameButtonShow = function(ev){
	var e = ev.target;
	var tfb = e.jbAppTableFrameButton;
	
	if (tfb.showing) return;
	tfb.showing = true;
	
	var bbox = e.getBBox();
	var fr = new jbAppFrame(tfb);
	fr.e.style.position = "absolute";
	fr.e.style.top = bbox.y2;
	fr.e.style.left = bbox.x;
	
	
	
	var et = fr.EditTable(tfb.title);
	et.setData(tfb.data);
	et.setCellTypes(tfb.celltypes);
	et.setColumnTypes(tfb.columntypes);
	et.setHeaderRow(tfb.headerrow);
	
	var buttons = fr.e.addElement("div",{"class" : "jbAppTableFrameButtonButtons"},null);
	var ok = buttons.addElement("a",{"class" : "jbAppTableFrameButtonOK"},"OK");
	var cancel = buttons.addElement("a",{"class" : "jbAppTableFrameButtonCancel"},"Cancel");
	
	ok.onclick = jbAppTableFrameButtonOK;
	ok.jbAppTableFrameButton = tfb;
	ok.jbAppTableFrameButtonFrame = fr;
	ok.jbAppEditTable = et;
	
	cancel.onclick = jbAppTableFrameButtonCancel;
	cancel.jbAppTableFrameButton = tfb;
	cancel.jbAppTableFrameButtonFrame = fr;
}
function jbAppTableFrameButtonOK(ev){
	var tfb = ev.target.jbAppTableFrameButton;
	var fr = ev.target.jbAppTableFrameButtonFrame;
	var et = ev.target.jbAppEditTable;
	tfb.data = et.data;
	fr.e.clearElement();
	tfb.e.removeChild(fr.e);
	tfb.callback(true);
	tfb.showing = false;
}
function jbAppTableFrameButtonCancel(ev){
	var tfb = ev.target.jbAppTableFrameButton;
	var fr = ev.target.jbAppTableFrameButtonFrame;
	fr.e.clearElement();
	tfb.e.removeChild(fr.e);
	tfb.callback(false);
	tfb.showing = false;
}

jbAppTableFrameButton.prototype.setTitle = function(d){
	this.title = d;
};
jbAppTableFrameButton.prototype.setData = function(d){
	this.data = d;
};
jbAppTableFrameButton.prototype.setCellTypes = function(d){
	this.celltypes = d;
};
jbAppTableFrameButton.prototype.setColumnTypes = function(d){
	this.columntypes = d;
};
jbAppTableFrameButton.prototype.setHeaderRow = function(d){
	this.headerrow = d;
};
jbAppTableFrameButton.prototype.setCallback = function(d){
	this.callback = d;
};




// edit table
function jbAppEditTable(p){
	this.parent = p;
	this.top = p.top;
	this.id = this.top.UID();
	this.e = this.parent.e.addElement("table", {
		"class" : "jbAppEditTable",
		"id" : this.id,
		}, "[]");
	this.parent.children.push(this);
	this.onchange = function(){}
}
// edit table event handlers...
var jbAppEditTableEditingCell = null;
function jbAppEditTableEditCell(ev){
	if(jbAppEditTableEditingCell !== null){
		var e = jbAppEditTableEditingCell;
		var pji = jbAppEditTableEditingCell.id.split(/_/);
		var i = parseInt(pji.pop());
		var j = parseInt(pji.pop());
		
		var et = e.jbAppEditTable;
		
		var edid = jbAppEditTableEditingCell.id + "_edit";
		var ed = document.getElementById(edid);
		var v = ev == "escape" ? et.data[j][i] : ed.value;
		
		var celltype =  et.getCellType(i,j);
		
		if(celltype["type"] == "float")
			v = parseFloat(v);
		if(celltype["type"] == "int")
			v = parseInt(v);
					
		e.clearElement();
		e.setText(v);
		et.data[j][i] = v;
		
		jbAppEditTableEditingCell =null;
		
		et.onchange();
	}
	if(ev && ev != "escape"){
		var e = ev.target ? ev.target : ev;
		var et = e.jbAppEditTable;
		var posn = e.getBBox();
		var id = e.id;
		var pji = id.split(/_/);
		var i = parseInt(pji.pop());
		var j = parseInt(pji.pop());
		
		var edid = id + "_edit";
		
		e.clearElement();
		var ed = e.addElement("textarea", {
			"style" : "width:"+posn.w+"px; height:"+posn.h+"px;",
			id : edid
		}, et.data[j][i]);
		ed.focus();
		ed.select();
		ed.onkeypress = jbAppEditTableTAKeyRelease;
		ed.onblur = jbAppEditTableTABlur;
		
		var maybeW = 0.7 * (ed.value.length+1) * parseInt( ed.getStyle("font-size") );
		if(maybeW > parseInt(ed.style.width))
			ed.style.width = maybeW;
		
		jbAppEditTableEditingCell = e;
	}
}
function jbAppEditTableTABlur(e){
	var el = e.target;
	var id = el.id.split(/_/);
	id.pop();
	id[2]++;
	jbAppEditTableEditCell(document.getElementById(id.join("_")));
	e.preventDefault();
}
function jbAppEditTableTAKeyRelease(e){
	var el = e.target;
	var id = el.id.split(/_/);
	id.pop();
	if(e.keyCode == 13) { // enter
		id[3]++;
		jbAppEditTableEditCell(document.getElementById(id.join("_")));
		e.preventDefault();
	} 
	else if(e.keyCode == 9) { // tab
		id[4]++;
		jbAppEditTableEditCell(document.getElementById(id.join("_")));
		e.preventDefault();
	} 
	else if(e.keyCode == 27) { // escape
		jbAppEditTableEditCell("escape");
		e.preventDefault();
	} 
	else if(el.value.match(/[\t\n]/)){
		var t = parseTable(el.value);
	}
	else if(e.keyCode == 8 || e.keyCode == 46) { // backspace / delete
	} 
	else {
		var maybeW = 0.7 * (el.value.length+2) * parseInt( el.getStyle("font-size") );
		if(maybeW > parseInt(el.style.width))
			el.style.width = maybeW;
	}
}
function jbAppEditTableSelectChange(ev){
	var sel = ev.target;
	var e = sel.parentNode;
	var et = e.jbAppEditTable;
	var id = e.id;
	var pji = id.split(/_/);
	var i = parseInt(pji.pop());
	var j = parseInt(pji.pop());
	
	var celltype =  et.getCellType(i,j);
	var v = this.options[this.selectedIndex].value;
		
	if(celltype["datatype"] == "float")
		v = parseFloat(v);
	if(celltype["datatype"] == "int")
		v = parseInt(v);

	et.data[j][i] = v;
	et.onchange();
};

// things we can do with an edit table...
jbAppEditTable.prototype.setData = function(ts){
	if(typeof(ts) == "string") // make it a table if it's a string
		ts = parseTable(ts);
	this.data = ts;
	this.repaint();
};
jbAppEditTable.prototype.setColumnTypes = function(cts){
	this.columntypes = cts;
	this.repaint();
};
jbAppEditTable.prototype.setCellTypes = function(ts){
	if(typeof(ts) == "string") // make it a table if it's a string
		ts = parseTable(ts);
	this.celltypes = ts;
	this.repaint();
};
jbAppEditTable.prototype.setHeaderRow = function(hr){
	this.headerrow = hr;
	this.repaint();
}
jbAppEditTable.prototype.getCellType = function(i,j){
	var celltype = "freetext";
	if(this.celltypes && this.celltypes[j] && this.celltypes[j][i])
		celltype = this.celltypes[j][i];
	else if (this.columntypes && this.columntypes[i])
		celltype = this.columntypes[i];
	if(typeof(celltype) != "object")
		celltype = {"type" : celltype};
	return celltype;
}
jbAppEditTable.prototype.repaint = function(){
	this.e.clearElement();
	
	if(this.headerrow){
		var tr = document.createElement('tr');
		for(var i=0; i<this.headerrow.length; i++){
			var tdid = this.id + "_header_" + i;
			var td = tr.addElement(
				"th",
				{	"class" : "jbAppEditTableHeader",
					"id" : tdid},
				this.headerrow[i] ? this.headerrow[i] : ""
			); 
		}
		this.e.appendChild(tr);
	}
	
	for(var j=0; j<this.data.length; j++){
		var tr = document.createElement('tr');
		for(var i=0; i<this.data[j].length; i++){
			var tdid = this.id + "_cell_" + j + "_" + i;
			
			var celltype = this.getCellType(i,j);
			
			if(celltype["type"] == "list"){ // list!
				var td = tr.addElement(
					"td",
					{	"class" : "jbAppEditTableCell",
						"id" : tdid}, null
				);
				var sel = td.addElement("select",{},null);
				sel.onchange = jbAppEditTableSelectChange;
				for(item in celltype["options"]){
					if(this.data[j][i] == item)
						sel.addElement("option", {"value" : item, "selected":"selected"}, celltype["options"][item]);
					else 
						sel.addElement("option", {"value" : item}, celltype["options"][item]);
				}
				td.jbAppEditTable = this;
			}
			else if(celltype["type"] == "label"){ // non-editable...
				var td = tr.addElement(
					"td",
					{	"class" : "jbAppEditTableCell",
						"id" : tdid},
					this.data[j][i]
				);
						// no click event!
				td.jbAppEditTable = this;
			}
			else { // the default is freetext...
				var td = tr.addElement(
					"td",
					{	"class" : "jbAppEditTableCell jbAppEditTableCellFreeText",
						"id" : tdid},
					this.data[j][i]
				);
				td.onclick = jbAppEditTableEditCell;
				td.jbAppEditTable = this;
			}
		}
		this.e.appendChild(tr);
	}
};

var A = ["hello","there"];
var B = {"hello":"there"};





////// some additonal stuff to make Elements more useful!

Element.prototype.clearElement = function(){
	if ( this.hasChildNodes() ){
		while ( this.childNodes.length >= 1 ){
			this.removeChild( this.firstChild );       
		} 
	}
};


Element.prototype.addElement = function(type, params, contents){
	var e = document.createElement(type);
	params = params ? params : {};
	for(p in params){
		e.setAttribute(p, params[p]);
	}
	if(contents != null){
		if(typeof(contents) == "object"){
			for(var i=0; i<contents.length; i++){
				if(typeof(contents[i]) == "object"){
					if(contents[i].length){
						e.addElement(contents[i][0],contents[i][1],contents[i][2]);
					}
					else if(contents[i]["type"]){
						e.addElement(contents[i]["type"],contents[i]["params"],contents[i]["content"]);
					}
				}
				else {
					e.appendText(contents[i]);
				}
			}
		}
		else {
			e.appendText(contents);
		}
	}
	this.appendChild(e);
	return e;
};

Element.prototype.setText = function(text){
	this.clearElement();
	this.appendChild(document.createTextNode(text));
};

Element.prototype.appendText = function(text){
	this.appendChild(document.createTextNode(text));
};


/* getElementWidth and getElementHeight are from http://www.cjboco.com/blog.cfm/post/determining-an-elements-width-and-height-using-javascript/ */
Element.prototype.getElementWidth = function() {
	if (typeof this.clip !== "undefined") {
		return this.clip.width;
	}
	else {
		if (this.style.pixelWidth) {
			return this.style.pixelWidth;
		}
		else {
			return this.offsetWidth;
		}
	}
};
Element.prototype.getElementHeight = function() {
	if (typeof this.clip !== "undefined") {
		return this.clip.Height;
	}
	else {
		if (this.style.pixelHeight) {
			return this.style.pixelHeight;
		}
		else {
			return this.offsetHeight;
		}
	}
};
Element.prototype.findPos = function() {
	var obj = this;
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		do {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	}
	return [curleft,curtop];
};
Element.prototype.getBBox = function(){
	var pos = this.findPos();
	var w = this.getElementWidth();
	var h = this.getElementHeight();
	var posn = new Object();
	posn.width = posn.w = w;
	posn.height = posn.h = h;
	posn.x = posn.x1 = pos[0];
	posn.y = posn.y1 = pos[1];
	posn.x2 = pos[0] + w -1;
	posn.y2 = pos[1] + h -1;
	return posn;
};












// http://www.quirksmode.org/dom/getstyles.html
function getStyle(el,styleProp){
	var x = document.getElementById(el);
	if (x.currentStyle)
		var y = x.currentStyle[styleProp];
	else if (window.getComputedStyle)
		var y = document.defaultView.getComputedStyle(x,null).getPropertyValue(styleProp);
	return y;
}

Element.prototype.getStyle = function (styleProp){
	if (this.currentStyle)
		return this.currentStyle[styleProp];
	else if (window.getComputedStyle)
		return document.defaultView.getComputedStyle(this,null).getPropertyValue(styleProp);
}





// parses a tab delimited table from a string... called by run
function parseTable(s){
	var rows = s.split(/[\n\r]+/);
	for(var i=0; i<rows.length; i++){
		rows[i] = rows[i].split(/\t/);
	}
	return rows;
}

// some helper functions...

function dumpItem(n,t,s){ // recursively list the stuff in a data structure
	// n is the name ..  name of this object
	// t is the thing ... the object whose name is n, not the object containing n!
	// s is the spacing ...  fill an extra tab each time to make it pretty :-)
	var d = ""; // dump string
	if(typeof(t) == "object"){ // it's an object
		d += s+n+" = Object(\n"; // first line
		for(item in t){ // dump each thing inside it...
			d += dumpItem(item, t[item], s + "\t");
		}
		d += s+")\n"; // close it off
	}
	else if(typeof(t) == "string"){ // quote it
		d += s+n+" = \""+t+"\"\n";
	}
	else if(typeof(t) == "number"){ // raw
		d += s+n+" = "+t+"\n";
	}
	else { // unknown ? write the type...
		d += s+n+" = "+typeof(t)+" ("+t+")\n";
	}
	return d;
}
function dump(o){ // shortcut to get started with data dumping
	return dumpItem("dump",o,"");
}
function dumpalert(o){ // shortcut to alert a dump
	alert(dump(o));
}
