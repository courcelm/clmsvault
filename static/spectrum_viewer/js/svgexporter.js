
function exportSVG(){

   var svgcode = document.getElementById("container").innerHTML;
   var nohiddentexts = svgcode.replace(/<text[^>]*visibility\:\s*hidden[^>]*>[^<]*<\/text>/gi,"");
   var nohiddenlines = nohiddentexts.replace(/<line[^>]*visibility\:\s*hidden[^>]*><\/line>/gi,"");
   var withnl = nohiddenlines.replace(/(<\/[^>]*>)/gi,"$1\n");
   var noNan = withnl.replace(/"NaN"/g,"\"0\"");
   //alert(nohidden);
   
   var win = window.open(
      'data:image/svg,'
       + encodeURIComponent(
         "<"+"?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"
         + "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
         + "<!-- Created with xiSPEC (http://spectrumviewer.org/) -->\n"
         + noNan
      ),
      'XiExport'
   );

}

function postSVG(filename){


   var svgcode = document.getElementById("container").innerHTML;
   //alert(svgcode);
   var nohiddentexts = svgcode.replace(/<text[^>]*visibility\:\s*hidden[^>]*>[^<]*<\/text>/gi,"");
   var nohiddenlines = nohiddentexts.replace(/<line[^>]*visibility\:\s*hidden[^>]*><\/line>/gi,"");
   var withnl = nohiddenlines.replace(/(<\/[^>]*>)/gi,"$1\n");
   var noNan = withnl.replace(/"NaN"/g,"\"0\"");
   //alert(nohidden);
   //alert(noNan);


   mimeType = 'text/plain';

   var link = document.createElement('a');
   document.body.appendChild(link);
   link.setAttribute('download', filename);
   link.setAttribute('href', 'data:' + mimeType + ';charset=utf-8,' + encodeURIComponent(noNan));
   link.click();

   /*
   post_to_url("http://129.215.14.148/jb/xisv/export.php",{"svg":"<"+"?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"
         + "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
         + "<!-- Created with xiSPEC (http://spectrumviewer.org/) -->\n"
         + noNan});
   */
}

function postDATA(){
   // what are the inputs we need to post?
   var textareas = new Array("e", "formulae", "colourtable", "iontable", "losslist", "specent");
   var textinputs = new Array("mzDPin", "pepent1", "pepent2", "resent1", "resent2", "ppm", "maxz");
   var checkboxes = new Array("ispep2", "isxlink");
   // collect params
   var params = {};
   // from textareas
   for(var i=0; i<textareas.length; i++){
      params[textareas[i]] = document.getElementById(textareas[i]).value;
   }
   // from text inputs
   for(var i=0; i<textinputs.length; i++){
      params[textinputs[i]] = document.getElementById(textinputs[i]).value;
   }
   // from checkboxes
   for(var i=0; i<checkboxes.length; i++){
      if(document.getElementById(checkboxes[i]).checked)
         params[checkboxes[i]] = ' checked="checked" ';
   }
   //alert(ObjectDumper(params));
   var xlink = window.location.toString().match(/xlink/) ? '&xlink' : '';
   post_to_url("index.php?posted" + xlink,params);
}

function post_to_url(path, params, method, target) {
    method = method || "post"; // Set method to post by default, if not specified.
   target = target || "_blank";

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    form.setAttribute("target", target);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}
