"""
Copyright 2015-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries
import os

# Import Django related libraries

# Import project libraries


def get_secret(name):
    """
    Gets secret from file or environment variable
    :param name: String
    :return: String
    """

    # Look first in file for secret
    path = os.environ.get('SECRET_PATH', '')

    file_ = os.path.join(path, name)

    if os.path.exists(file_):

        with open(file_, 'r') as f:

            return f.readline().strip()

    return os.environ[name]



