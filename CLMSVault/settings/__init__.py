"""
Copyright 2015-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries
import sys

# Import Django related libraries

# Import project libraries


# Look if user has defined settings from command line

loaded = False

for arg in sys.argv:

    if '--settings' in arg:
        loaded = True

# Other wise check if local or production file is there
if loaded is False:

    # Local settings
    try:
        from .local import *

    except ImportError:

        # Production settings
        try:
            from .production import *

        except ImportError:

            pass
