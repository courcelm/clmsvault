# Copyright 2013-2018 Mathieu Courcelles
# Mike Tyers's lab / IRIC / Universite de Montreal


# Import standard libraries
from socket import gethostname, gethostbyname

# Import Django related libraries

# Third party libraries
import raven
from raven.transport.requests import RequestsHTTPTransport


# Import project libraries
from .base import *
from ..secrets import get_secret

DEBUG = os.environ.get('DEBUG', False)

ADMINS = (
    (os.environ.get('ADMIN_NAME', ''), os.environ.get('ADMIN_EMAIL', '')),
)

MANAGERS = ADMINS

if DEBUG:
    DEBUG = True

    def toolbar_callback(request):
        return True

    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_TOOLBAR_CALLBACK': toolbar_callback
    }

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_secret('SECRET_KEY')

# Mail server configuration
EMAIL_USE_TLS = True
EMAIL_HOST = os.environ.get('EMAIL_HOST', '')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = get_secret('EMAIL_HOST_PASSWORD')
EMAIL_PORT = 587
EMAIL_SUBJECT_PREFIX = os.environ.get('SITE_NAME', '[CLMSVault] ')

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', '').split()
ALLOWED_HOSTS += ['0.0.0.0', gethostname(), gethostbyname(gethostname())]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'HOST': os.environ.get('MYSQL_HOST', ''),
        'NAME': os.environ.get('MYSQL_DB', ''),
        'USER': os.environ.get('MYSQL_USER', ''),
        'PASSWORD': get_secret('MYSQL_PASSWORD'),
        'PORT': '3306',  # Set to empty string for default. Not used with sqlite3.
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        }
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Montreal'

PERCOLATOR_BIN = r'/usr/bin/percolator'

if os.getenv('SENTRY', None):

    INSTALLED_APPS.append('raven.contrib.django.raven_compat')
    MIDDLEWARE.insert(2, 'raven.contrib.django.raven_compat.middleware.'
                         'Sentry404CatchMiddleware')
    RAVEN_CONFIG = {
        'dsn': get_secret('SENTRY'),
        # If you are using git, you can also automatically configure the
        # release based on the git info.
        'release': os.getenv('GIT_SHA'),
        'transport': RequestsHTTPTransport,
    }

MIDDLEWARE.insert(0, 'x_forwarded_for.middleware.XForwardedForMiddleware')

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/django_cache',
    }
}


# Django Defender
MIDDLEWARE.append('defender.middleware.FailedLoginMiddleware')
DEFENDER_LOGIN_FAILURE_LIMIT = 10
DEFENDER_BEHIND_REVERSE_PROXY = True
DEFENDER_REDIS_URL = 'redis://redis:6379/0'

