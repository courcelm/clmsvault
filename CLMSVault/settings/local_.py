"""
Copyright 2013-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""


# Import standard libraries

# Import Django related libraries

# Import project libraries
from .base import *
from unipath import Path

PROJECT_DIR = Path(__file__).ancestor(3)

DEBUG = True
TEMPLATE_DEBUG = DEBUG


ADMINS = (
     ('Admin name', 'admin@name.ca'),
)

MANAGERS = ADMINS


DATABASES = {

    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': PROJECT_DIR + '/CLMSVault.db', # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}


# SECURITY WARNING: keep the secret key used in production secret!
# You can generate a new secret_key here:
# http://www.miniwebtool.com/django-secret-key-generator/
SECRET_KEY = 'cur%pf+1k5=mf2d=jaurhr(djv&ve&&y%4px=pouav)0ytn@7-'


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Montreal'

PERCOLATOR_BIN = r'C:\Program Files (x86)\percolator-v2-07\bin\percolator.exe'
