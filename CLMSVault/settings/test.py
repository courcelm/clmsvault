# # Copyright 2013 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 




# Import standard librariesdjang

# Import Django related libraries

# Import project libraries
from .base import *


########## IN-MEMORY TEST DATABASE

DATABASES = {

    "default": {
    
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    
    },

}

os.environ['REUSE_DB'] = "1" 

SOUTH_TESTS_MIGRATE = False