# # Copyright 2013 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries

# Import Django related libraries
from django.test import TestCase

# Import project libraries
import mock_up
from ..models import FastaDB
from ..parser.pLinkParser import pLinkParser
from ..parser.xQuestParser import xQuestParser
from ..parser.sequences import sequencesMatcher



def setUpModule():
    """
    Mock up database objects for test.
    """
    mock_up.setUp()


class PaserTestCase(TestCase):
    """
    Tests for Parsers. 
    """


    def setUp(self):
        pass

    def tearDown(self):
        pass


    def test_pLink(self):
        """
        Test pLink methods.
        """
        
        # formatPeptideSequence(peptide) test
        self.assertEqual(pLinkParser.formatPeptideSequence('TTTXTTT'), 
                         'TTTXTTT')
        self.assertEqual(pLinkParser.formatPeptideSequence('TTTCTTT'), 
                         'TTTCmTTT')
        self.assertEqual(pLinkParser.formatPeptideSequence('TTTCTTCT'), 
                         'TTTCmTTCmT')
         
         
    def test_xQuest(self):
        """
        Test xQuest methods.
        """
        
        # formatPeptideSequence(peptide) test
        self.assertEqual(xQuestParser.formatPeptideSequence('ZTTTTT'), 
                         'TTTTT')
        self.assertEqual(xQuestParser.formatPeptideSequence('TTTCTTT'), 
                         'TTTCmTTT')
        self.assertEqual(xQuestParser.formatPeptideSequence('TTTCTTCT'), 
                         'TTTCmTTCmT')
        self.assertEqual(xQuestParser.formatPeptideSequence('TTTXTTCT'), 
                         'TTTMoxTTCmT')
         
         
    def test_sequencesMatcher(self):
        """
        Test sequencesMatcher methods.
        """
        
        seq_set = FastaDB.objects.get(pk=2).fastadb_sequence_set.all()
         
        # Prepare sequenceMatcher
        sM = sequencesMatcher(2)
         
        # Match protein sequences
        self.assertEqual(sM.sequencePk(seq_set[0].raw_description[0:4]).pk, 
                         seq_set[0].pk)
        self.assertEqual(sM.sequencePk('BAD'), None)
        