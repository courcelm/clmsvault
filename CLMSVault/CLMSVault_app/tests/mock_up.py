# # Copyright 2013 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries
import random

# Import Django related libraries

# Import project libraries
from ..models import (FastaDB, 
                      FastaDb_Sequence,
                      Project, 
                      )

from models_factories import (CLPeptideFactory, 
                              FastaDBFactory,
                              FastaDb_SequenceFactory,
                              ProjectFactory,
                              RawDatasetFactory,
                              )



def setUp():
    """
    Mock up database objects for test. It uses factory_boy to replace
    slow fixtures.
    """
        
    # Fake some projects
    for _ in xrange(0, 2):
        ProjectFactory()

    projects = Project.objects.all()


    # Fake some FastaDB
    for _ in xrange(0, 2):
        FastaDBFactory()
    
    fastadb = FastaDB.objects.all()
    
    # Fake some FastaDb_Sequence
    for _ in xrange(0, 10):
        FastaDb_SequenceFactory(fastadb=random.choice(fastadb))
    
    sequences = FastaDb_Sequence.objects.all()
    
    # Fake some RawDatasets
    for _ in xrange(0, 5):
        RawDatasetFactory(project=random.choice(projects),
                          fasta_db=random.choice(fastadb))
    
    # Fake some CLPeptide
    for _ in xrange(0, 25):
        CLPeptideFactory(fs_prot1_id=random.choice(sequences), 
                         fs_prot2_id=random.choice(sequences))
