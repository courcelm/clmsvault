# # Copyright 2013 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 




# Import standard librariesdjang
from datetime import datetime
import factory
import random
import string

# Import Django related libraries
from django.contrib.auth.models import User
from django.http import HttpRequest

# Import project libraries
from ..models import Dataset



def random_datetime():
    """
    Returns datetime object with random year, month, day, hour, minute.
    """

    year = random.choice(range(1950, 2100))
    month = random.choice(range(1, 12))
    day = random.choice(range(1, 28))
    hour = random.choice(range(0, 23))
    minute = random.choice(range(0,59))

    return datetime(year, month, day, hour, minute)


def random_string(length=10):
    """
    Returns a random string made of ascii characters of specified length.
    """
    
    return u''.join(random.choice(string.ascii_letters) for _ in range(length))




# Factory boy Django documentation
# https://factoryboy.readthedocs.org/en/latest/orms.html 


class ProjectFactory(factory.django.DjangoModelFactory):
    """
    Factory to create a Project object.
    """
    
    FACTORY_FOR = 'CLMSpipeline_app.Project'
    
    FACTORY_DJANGO_GET_OR_CREATE = ('name',)


    creation_date = factory.LazyAttribute(lambda t: random_datetime())
    
    name = factory.LazyAttribute(lambda t: random_string())



class FastaDBFactory(factory.django.DjangoModelFactory):
    """
    Factory to create a FastaDB object.
    Note some attributes are not yet implemented.
    """
    
    FACTORY_FOR = 'CLMSpipeline_app.FastaDB'
    
    FACTORY_DJANGO_GET_OR_CREATE = ('name',)

    
    creation_date = factory.LazyAttribute(lambda t: random_datetime())
    
    name = factory.LazyAttribute(lambda t: random_string())
    
    file = factory.django.FileField(filename='FastaDB_file.csv')
    
    identifier_regexp = r'^([^\s]+)'
    
#     gene_name_regexp = models.CharField(max_length=50, blank=True)
#     
#     description_regexp = models.CharField(max_length=50, blank=True)
#     
#     species_regexp = models.CharField(max_length=50, blank=True)
#     
#     parsing_status = models.BooleanField(default=False)
#      
#     parsing_log = models.CharField(max_length=1000, blank=True, null=True)
#     
#     sequence_count = models.IntegerField(default=0)
    
    update = factory.LazyAttribute(lambda t: [True, False][random.randrange(1)])

    

class FastaDb_SequenceFactory(factory.django.DjangoModelFactory):
    """
    Factory to create a FastaDb_Sequence object.
    """
    
    FACTORY_FOR = 'CLMSpipeline_app.FastaDb_Sequence'
 
 
    fastadb = factory.SubFactory(FastaDBFactory)
 
    identifier = factory.LazyAttribute(lambda t: random_string())

    gene_name = factory.LazyAttribute(lambda t: random_string())
     
    raw_description = factory.LazyAttribute(lambda t: random_string())
     
    description = factory.LazyAttribute(lambda t: random_string())
      
    species = factory.LazyAttribute(lambda t: random_string())
     
    sequence = factory.LazyAttribute(lambda t: random_string(length=int(random.uniform(100, 2000))).upper())



class DatasetFactory(factory.django.DjangoModelFactory):
    """
    Factory to create a Dataset object.
    Note some attributes are not yet implemented.
    """
    
    FACTORY_FOR = 'CLMSpipeline_app.Dataset'
    
    FACTORY_DJANGO_GET_OR_CREATE = ('name',)
    
    
    creation_date = factory.LazyAttribute(lambda t: random_datetime())
    
    name = factory.LazyAttribute(lambda t: random_string())
    
    project = factory.SubFactory(ProjectFactory)
    
    #cross_linker = models.ForeignKey(CrossLinker, null=True)
    
    #instrument_name = models.ForeignKey(Instrument, null=True)
    
    fasta_db = factory.SubFactory(FastaDBFactory)
    
    #search_algorithm = models.ForeignKey(SearchAlgorithm, null=True)

    description = factory.LazyAttribute(lambda t: random_string())

    
    
class RawDatasetFactory(factory.django.DjangoModelFactory):
    """
    Factory to create a RawDataset object.
    Note some attributes are not yet implemented.
    """
    
    FACTORY_FOR = 'CLMSpipeline_app.RawDataset'
    
    FACTORY_DJANGO_GET_OR_CREATE = ('name',)
    
    
    creation_date = factory.LazyAttribute(lambda t: random_datetime())
    
    name = factory.LazyAttribute(lambda t: random_string())
    
    project = factory.SubFactory(ProjectFactory)
    
    #cross_linker = models.ForeignKey(CrossLinker, null=True)
    
    #instrument_name = models.ForeignKey(Instrument, null=True)
    
    fasta_db = factory.SubFactory(FastaDBFactory)
    
    #search_algorithm = models.ForeignKey(SearchAlgorithm, null=True)

    description = factory.LazyAttribute(lambda t: random_string())

    file = factory.django.FileField(filename='Rawdataset_file.csv')
     
    extra_file = factory.django.FileField(filename='RawDataset_extra_file.csv')
 
    parsing_status = factory.LazyAttribute(lambda t: [True, False][random.randrange(1)])
     
    parsing_log = ''



class CLPeptideFactory(factory.django.DjangoModelFactory):
    """
    Factory to create a CLPeptide object.
    """    
    
    FACTORY_FOR = 'CLMSpipeline_app.CLPeptide'
    
    
    #dataset = factory.SubFactory(DatasetFactory) # See at bottom to fix ManyToManyField problem
    
    run_name = factory.LazyAttribute(lambda t: random_string())
    
    scan_number = factory.LazyAttribute(lambda t: random.randrange(10000))
     
    precursor_mz = factory.LazyAttribute(lambda t: random.uniform(300, 2000))
     
    precursor_charge = factory.LazyAttribute(lambda t: random.randrange(7) + 1)
     
    precursor_intensity = factory.LazyAttribute(lambda t: random.uniform(100, 10000000))
     
    rank = factory.LazyAttribute(lambda t: random.randrange(10))
     
    match_score = factory.LazyAttribute(lambda t: random.uniform(0, 100))
     
    spectrum_intensity_coverage = factory.LazyAttribute(lambda t: random.uniform(0, 100))
     
    total_fragment_matches = factory.LazyAttribute(lambda t: random.randrange(100))
     
    delta = factory.LazyAttribute(lambda t: random.gauss(0, 2))
    
    error = factory.LazyAttribute(lambda t: random.uniform(0, 20))

    display_protein1 = factory.LazyAttribute(lambda t: t.fs_prot1_id.raw_description)
      
    fs_prot1_id = factory.SubFactory(FastaDb_SequenceFactory)
     
    peptide_position1 = factory.LazyAttribute(lambda t: random.randrange(len(t.fs_prot1_id.sequence)) + 1)
     
    peptide1 = factory.LazyAttribute(lambda t: t.fs_prot1_id.sequence[t.peptide_position1 - 1: t.peptide_position1 + random.randrange(20)])
    
    pep1_link_pos = factory.LazyAttribute(lambda t: random.randrange(len(t.peptide1)) + 1)
    
    peptide_wo_mod1 = factory.LazyAttribute(lambda t: t.peptide1)
    
    display_protein2 = factory.LazyAttribute(lambda t: t.fs_prot2_id.raw_description)
      
    fs_prot2_id = factory.SubFactory(FastaDb_SequenceFactory)
     
    peptide_position2 = factory.LazyAttribute(lambda t: random.randrange(len(t.fs_prot2_id.sequence)) + 1)
     
    peptide2 = factory.LazyAttribute(lambda t: t.fs_prot2_id.sequence[t.peptide_position2 - 1: t.peptide_position2 + random.randrange(20)])
    
    pep2_link_pos = factory.LazyAttribute(lambda t: random.randrange(len(t.peptide2)) + 1)
    
    peptide_wo_mod2 = factory.LazyAttribute(lambda t: t.peptide2)
    
    autovalidated = factory.LazyAttribute(lambda t: [True, False][random.randrange(1)])
     
    validated = factory.LazyAttribute(lambda t: random_string())
     
    rejected = factory.LazyAttribute(lambda t: [True, False][random.randrange(1)])
     
    notes = factory.LazyAttribute(lambda t: random_string())
     
    link_type = factory.LazyAttribute(lambda t: ['Inter-protein', 
                                                 'Intra-protein',
                                                 'Intra-peptide',
                                                 'Dead-end'][random.randrange(4)])
     
    cross_link = factory.LazyAttribute(lambda t: [True, False][random.randrange(1)])
     
    not_decoy = factory.LazyAttribute(lambda t: [True, False][random.randrange(1)])


    @classmethod
    def _prepare(cls, create, **kwargs):
        """
        Fix 'dataset' ManyToManyField problem
        """
        
        d = random.choice(Dataset.objects.all())
        clpep = super(CLPeptideFactory, cls)._prepare(create, **kwargs)
        clpep.dataset.add(d)
        
        return clpep






class FakeMessages:
    """
    Mocks the Django message framework, makes it easier to get the messages out.
    Code from
    http://chase-seibert.github.io/blog/2012/07/27/faster-django-view-unit-tests-with-mocks.html  
    """
  
    messages = []  
  
    def add(self, level, message, extra_tags):  
        self.messages.append(str(message))  
 
    @property  
    def pop(self):  
        return self.messages.pop()  
  
  
def FakeRequestFactory(*args, **kwargs):  
    """
    FakeRequestFactory, FakeMessages and FakeRequestContext are good for 
    mocking out django views; they are MUCH faster than the Django test client. 
    Code from
    http://chase-seibert.github.io/blog/2012/07/27/faster-django-view-unit-tests-with-mocks.html  
    """  
  
    user = UserFactory()  
    if kwargs.get('authenticated'):  
        user.is_authenticated = lambda: True  
  
    request = HttpRequest()  
    request.user = user  
    request._messages = FakeMessages()  
    request.session = kwargs.get('session', {})  
    
    if kwargs.get('POST'):  
        request.method = 'POST'  
        request.POST = kwargs.get('POST')  
    else:  
        request.method = 'GET'  
        request.POST = kwargs.get('GET', {})  
  
    return request  
  
  
class UserFactory(factory.Factory):  
    """
    Factory to create a User object.
    Code from
    http://chase-seibert.github.io/blog/2012/07/27/faster-django-view-unit-tests-with-mocks.html  
    """ 
    
    FACTORY_FOR = User
    
     
    username = factory.Sequence(lambda i: 'test_%s' % i)
      
    first_name = 'Test'
      
    last_name = 'Test'
      
    email = factory.Sequence(lambda i: 'test%s@test.com' % i)
    
