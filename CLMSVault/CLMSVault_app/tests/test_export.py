# # Copyright 2013 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries

# Import Django related libraries
from django.test import TestCase
from django.test.client import RequestFactory

# Import project libraries
from ..admin import DatasetAdminMixin
from ..export import (compareRunIds_csv,
                      dataset_stats_csv,
                      interaction_matrix,
                      make_MascotReport_csv, 
                      make_Xi_csv,
                      psi_mi_tab25,
                      )
import mock_up
from ..models import (CLPeptide,
                      RawDataset
                      )


def setUpModule():
    """
    Mock up database objects for test.
    """
    mock_up.setUp()
    

class ExportTestCase(TestCase):
    """
    Tests for Export methods. 
    """

    
    def test_compareRunIds_csv(self):
        """
        Test for compareRunIds_csv response.
        """
        
        factory = RequestFactory()
        request = factory.get('/')
        
        datasets = RawDataset.objects.all()

        form_data = {'unique_key': 'sequence'}

        form = DatasetAdminMixin.CompareForm(data=form_data)
        
        form.is_valid()
        
        response =  compareRunIds_csv(None, request, datasets, form, 'Test')


        self.assertEqual(response.status_code, 200)
        
        self.assertEqual(response.items()[0][1] == 'text/csv', True)
        
        self.assertEqual(response.content.startswith('"Unique Key"'), True)
    
    
    def test_dataset_stats_csv(self):
        """
        Test for dataset_stats_csv response.
        """
        
        factory = RequestFactory()
        request = factory.get('/')
        
        datasets = RawDataset.objects.all()
        
        response =  dataset_stats_csv(None, request, datasets, 'Test')


        self.assertEqual(response.status_code, 200)
        
        self.assertEqual(response.items()[0][1] == 'text/csv', True)
        
        self.assertEqual(response.content.count('"Dataset","Run"'), 1)
    
    
    def test_interaction_matrix(self):
        """
        Test for interaction_matrix response.
        """
        
        factory = RequestFactory()
        request = factory.get('/')
        
        
        clpep = CLPeptide.objects.all()
        
        response =  interaction_matrix(None, request, clpep, 'Test')
        
        self.assertEqual(response.status_code, 200)
        
        self.assertEqual(response.items()[0][1] == 'text/csv', True)
        
        self.assertEqual(response.content.startswith('Interaction'), True)
        
    
    def test_make_MascotReport_csv(self):
        """
        Test for make_MascotReport_csv response.
        """
        
        factory = RequestFactory()
        request = factory.get('/')
        
        clpep = CLPeptide.objects.all()
        
        response =  make_MascotReport_csv(None, request, clpep, 'Test')


        self.assertEqual(response.status_code, 200)
        
        self.assertEqual(response.items()[0][1] == 'text/csv', True)
        
        self.assertEqual(response.content.startswith('searchlog='), True)
    
    
    def test_make_Xi_csv(self):
        """
        Test for make_Xi_csv response.
        """
        
        factory = RequestFactory()
        request = factory.get('/')
        
        
        clpep = CLPeptide.objects.all()
        
        response =  make_Xi_csv(None, request, clpep, 'Test')
        
        self.assertEqual(response.status_code, 200)
        
        self.assertEqual(response.items()[0][1] == 'text/csv', True)
        
        self.assertEqual(response.content.count('"run_name","scan_number"'), 1)
        
        
    def test_psi_mi_tab25(self):
        """
        Test for psi_mi_tab25 response.
        """
        
        factory = RequestFactory()
        request = factory.get('/')
        
        
        clpep = CLPeptide.objects.all()
        
        response =  psi_mi_tab25(None, request, clpep, 'Test')
        
        self.assertEqual(response.status_code, 200)
        
        self.assertEqual(response.items()[0][1] == 'text/plain', True)
        
        self.assertEqual(response.content.startswith('#ID Interactor A'), True)