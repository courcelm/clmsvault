# # Copyright 2013-2014 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries

# Import Django related libraries
from django.test import TestCase

# Import project libraries
import mock_up
from ..models import (CLPeptide, 
                      )
from ..queryset_operation import clpeptide_set_2_protein_sequences



def setUpModule():
    """
    Mock up database objects for test.
    """
    mock_up.setUp()
    

class QuerysetTestCase(TestCase):
    """
    Tests for queryset_operation module. 
    """


    def setUp(self):
        pass


    def tearDown(self):
        pass
    
            
    def test_clpeptide_set_2_protein_sequences(self):
        """
        Tests for clpeptide_set_2_protein_sequences and
        clpeptide_set_2_protein_set. 
        """
        
        clpeptide_set = CLPeptide.objects.all()
        protein_sequences = clpeptide_set_2_protein_sequences(clpeptide_set)
        
        self.assertEqual(clpeptide_set[0].fs_prot1_id.sequence, protein_sequences[clpeptide_set[0].fs_prot1_id.id])
        

