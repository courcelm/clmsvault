# # Copyright 2013-2014 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries

# Import Django related libraries
from django.test import TestCase

# Import project libraries
import mock_up
from ..pdb_structure import (CLDistance,
                             findPeptidePositionsInAligment,
                             sequences_alignment,
                             
                             )



def setUpModule():
    """
    Mock up database objects for test.
    """
    mock_up.setUp()
    

class PDBTestCase(TestCase):
    """
    Tests for pdb_structure. 
    """


    def setUp(self):
        pass


    def tearDown(self):
        pass
    
    
    def test_CLDistance(self):
        """
        Test for CLDistance class
        """
        
        # Init test
        cldistance = CLDistance(10, None, None)
        self.assertEqual(True, hasattr(cldistance, 'distance'))
        self.assertEqual(True, hasattr(cldistance, 'residue_1'))
        self.assertEqual(True, hasattr(cldistance, 'residue_2'))
        
            
    def test_findPeptidePositionsInAligment(self):
        """
        Tests for findPeptidePositions 
        """
        
        fasta_seq_aligned = 'HRFKDLGEEHFKD-GLVL'
        pdb_seq_aligned   = 'HRFKDLAEE--KD-GLVL'
        link_index = 1
        
        # Test un-gapped search
        hit = findPeptidePositionsInAligment('HRFK', fasta_seq_aligned, pdb_seq_aligned, link_index, 0)[0]
        self.assertEqual(hit['final_pep_seq'], list('hRFK'))
        
        # Test gapped search
        hit = findPeptidePositionsInAligment('HRFKDLGEEHFKD', fasta_seq_aligned, pdb_seq_aligned, link_index, 0)[0]
        self.assertEqual(hit['final_pep_seq'], list('hRFKDLAEEKD'))
        
        # Test identity filter
        hit = findPeptidePositionsInAligment('DLGEEHFKD', fasta_seq_aligned, pdb_seq_aligned, link_index, 1)
        self.assertEqual(hit, [])
        
        # Test length filter
        hit = findPeptidePositionsInAligment('KDG', fasta_seq_aligned, pdb_seq_aligned, link_index, 0)
        self.assertEqual(hit, [])
        
        # Test no match
        hit = findPeptidePositionsInAligment('XXXXXX', fasta_seq_aligned, pdb_seq_aligned, link_index, 0)
        self.assertEqual(hit, [])
    
    
    def test_sequences_alignment(self):
        """
        Test for sequences_alignment
        """
        
        fasta_sequences = dict()
        fasta_sequences[1] = 'HRFKDLGEEHFKDGLVL'
        
        pdb_sequences = dict()
        pdb_sequences[1] = 'KDLGAEEH'
        
        
        alignments = sequences_alignment(fasta_sequences, pdb_sequences, 0)
        
        self.assertEqual(alignments[1][0]['pdb_seq_aligned'], '---KDLGAEEH-------')