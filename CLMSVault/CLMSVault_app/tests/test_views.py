# # Copyright 2013-2014 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries
from mock import patch

# Import Django related libraries
from django.test import TestCase

# Import project libraries
import mock_up
from ..models import CLPeptide
from models_factories import (FakeRequestFactory,
                              UserFactory
                              )
from ..views import xiNET_view




def setUpModule():
    """
    Mock up database objects for test.
    """
    mock_up.setUp()
  
  
class ViewsTestCase(TestCase):  
    
    
    def setUp(self):  
        """
        Using django-nose to only run ONCE, not once per test 
        using globals for easy reference in the tests
        
        Mock user authentification example from
        http://chase-seibert.github.io/blog/2012/07/27/faster-django-view-unit-tests-with-mocks.html
        """
      
        global request, user_being_viewed  
        request = FakeRequestFactory()
        user_being_viewed = UserFactory()  
      
        def render_to_response_echo(*args, **kwargs):  
            """
            Mocked render_to_response that just returns what was passed in, 
            also puts the template name into the results dict.
            """
              
            context = args[1]
            locals_tmp = context.dicts[1]
            locals_tmp.update(dict(template_name=args[0]))

            return locals_tmp
      
        patch('CLMSpipeline.CLMSpipeline_app.views.render_to_response',  
              render_to_response_echo).start()
    
  
  
    