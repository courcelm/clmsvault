# # Copyright 2013 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries

# Import Django related libraries
from django.test import TestCase

# Import project libraries
import mock_up
from ..models import (CLPeptide, 
                      FastaDB, 
                      FastaDb_Sequence,
                      RawDataset,
                      remove_CLPeptideRD,
                      upload_path_handler,
                      )


def setUpModule():
    """
    Mock up database objects for test.
    """
    mock_up.setUp()
    

class ModelTestCase(TestCase):
    """
    Tests for Models. 
    """


    def setUp(self):
        pass


    def tearDown(self):
        pass
    
            
    def test_CLPeptide(self):
        """
        Tests for CLPeptide methods 
        """
        
        # fixAutovalidated() test
        clpep = CLPeptide.objects.all()[0]
        clpep.autovalidated = 'true'
        clpep.fixAutovalidated()
        self.assertEqual(clpep.autovalidated, True)
        clpep.autovalidated = 'false'
        clpep.fixAutovalidated()
        self.assertEqual(clpep.autovalidated, False)
        
        
        # guessLinkType() test
        clpep.peptide_wo_mod1 = 'A'
        clpep.peptide_wo_mod2 = 'A'
        clpep.guessLinkType()
        self.assertEqual(clpep.link_type, 'Inter-protein')
        self.assertEqual(clpep.cross_link, True)
          
        clpep.peptide_wo_mod2 = 'B'
        clpep.display_protein1 = 'A'
        clpep.display_protein2 = 'B'
        clpep.guessLinkType()
        self.assertEqual(clpep.link_type, 'Inter-protein')
        self.assertEqual(clpep.cross_link, True)
          
        clpep.peptide_wo_mod2 = 'B'
        clpep.display_protein1 = 'A'
        clpep.display_protein2 = 'A'
        clpep.guessLinkType()
        self.assertEqual(clpep.link_type, 'Intra-protein')
        self.assertEqual(clpep.cross_link, True)
          
        clpep.pep2_link_pos = -1
        clpep.display_protein2 = ''
        clpep.guessLinkType()
        self.assertEqual(clpep.link_type, 'Dead-end')
        self.assertEqual(clpep.cross_link, False)
          
        clpep.pep2_link_pos = 1
        clpep.peptide_wo_mod2 = '-'
        clpep.guessLinkType()
        self.assertEqual(clpep.link_type, 'Intra-peptide')
        self.assertEqual(clpep.cross_link, False)
          
      
        # isDecoy() test
        clpep.display_protein1 = 'DECOY'
        clpep.isDecoy()
        self.assertEqual(clpep.not_decoy, False)
        clpep.display_protein2 = 'DECOY'
        clpep.isDecoy()
        self.assertEqual(clpep.not_decoy, False)
        clpep.display_protein1 = ''
        clpep.isDecoy()
        self.assertEqual(clpep.not_decoy, False)
        clpep.display_protein2 = ''
        clpep.isDecoy()
        self.assertEqual(clpep.not_decoy, True)
          
          
        # uniqueKey test
        clpep.peptide_wo_mod1 = 'A'
        clpep.peptide_wo_mod2 = 'B'
          
        separator = '~~'
        params = dict()
        params['sequence'] = 1
        self.assertEqual(len(clpep.uniqueKey(params).split(separator)), 2)
        params['sequenceMods'] = 1
        self.assertEqual(len(clpep.uniqueKey(params).split(separator)), 4)
        params['sequenceCpos'] = 1
        self.assertEqual(len(clpep.uniqueKey(params).split(separator)), 8)
        params['proteinPpos'] = 1
        self.assertEqual(len(clpep.uniqueKey(params).split(separator)), 12)
        params['charge'] = 1
        self.assertEqual(len(clpep.uniqueKey(params).split(separator)), 13)
 
 
    def test_RawDataset(self):
        """
        Tests for RawDataset methods 
        """
        
        d1 = RawDataset.objects.all()[0]
   
        self.assertEqual(upload_path_handler(d1, d1.file), 
                         'RawDataset/%s-%s' % (d1.pk, d1.file))
        self.assertEqual(d1.get_admin_url(), 
                         '/admin/CLMSpipeline_app/rawdataset/%s/' % d1.pk)
        self.assertEqual(d1.formated_url(), 
                         '<a href="/admin/CLMSpipeline_app/rawdataset/%s/">%s</a><br />' 
                         % (d1.pk, d1))
        self.assertEqual(d1.formated_url_short(), 
                         '<a href="/admin/CLMSpipeline_app/rawdataset/%s/">[%s]</a><br />' 
                         % (d1.pk, d1.pk))
        
        
        # Check removal of cross-linked peptides
        v1 = len(d1.clpeptide_set.all())
        v2 = 0
        for clpep in d1.clpeptide_set.all():
            if clpep.dataset.count() == 1:
                v2 += 1
        remove_CLPeptideRD(None, d1)
        v3 = len(d1.clpeptide_set.all())
                
        self.assertEqual(v3, v1 - v2)
      
      
    def test_FastaDb_Sequence(self):
        """
        Tests for FastaDb_Sequence methods 
        """
        
        # Test extractFeatures(self, fastadb_instance)
        o1 = FastaDb_Sequence.objects.all()[0]
        fastadb_instance = FastaDB.objects.all()[0]
        self.assertEqual(o1.extractFeatures(fastadb_instance),'')
          
          
        fastadb_instance.identifier_regexp = 'BAD'
        self.assertEqual(o1.extractFeatures(fastadb_instance),'identifier')
        
