# Copyright 2013-2017 Mathieu Courcelles
# Mike Tyers's lab / IRIC / Universite de Montreal

"""
This module contains methods to export dataset to various
format.
"""

# Import standard libraries
from collections import OrderedDict
import csv
from numpy import linalg as LA

# Import Django related libraries
from django.http import HttpResponse
from django.template import loader

# Import project libraries
from .parser.exception import InvalidFileFormatException
from .pdb_structure import compute_cl_distance
from .queryset_operation import dataset_set_2_clpeptide_set


def clpep_header():
    """
    Return all the fields to write header for CLPeptide
    """

    fields = ['run_name',
              'scan_number',
              'precursor_mz',
              'precursor_charge',
              'precursor_intensity',
              'rank',
              'match_score',
              'spectrum_intensity_coverage',
              'total_fragment_matches',
              'delta',
              'error',
              'peptide1',
              'display_protein1',
              'peptide_position1',
              'pep1_link_pos',
              'peptide2',
              'display_protein2',
              'peptide_position2',
              'pep2_link_pos',
              'autovalidated',
              'validated',
              'rejected',
              'notes',
              ]

    return fields


def compareRunIds_csv(self, request, queryset, form, tableName):
    """
    Compares cross-linked peptides across MS runs and datasets.
    Outputs results to CSV format. 
    """

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = ('attachment; filename=' + tableName +
                                       '_' + request.META['QUERY_STRING'] + '.csv')

    writer = csv.writer(response, quoting=csv.QUOTE_ALL)

    header_list = OrderedDict([('Unique Key', ''), ])

    uniquePep_dict = dict()

    selected_unique_key = dict()

    for key in form.cleaned_data['unique_key'].split('-'):
        selected_unique_key[key] = True;


        # Iterates through datasets
    for dataset in queryset:

        header_list[str(dataset)] = ''

        # Iterates over Clpeptides
        for clpep in dataset.clpeptide_set.all():

            dataset_str = str(dataset)

            dataset_run_name = dataset_str + '__' + clpep.run_name

            header_list[dataset_run_name] = ''

            unique_key = clpep.uniqueKey(selected_unique_key)

            if not unique_key in uniquePep_dict:
                uniquePep_dict[unique_key] = dict()

            if not dataset_str in uniquePep_dict[unique_key]:
                uniquePep_dict[unique_key][dataset_str] = [clpep, 0]
            elif uniquePep_dict[unique_key][dataset_str][0].match_score < clpep.match_score:
                uniquePep_dict[unique_key][dataset_str][0] = clpep

            uniquePep_dict[unique_key][dataset_str][1] += 1

            if not dataset_run_name in uniquePep_dict[unique_key]:
                uniquePep_dict[unique_key][dataset_run_name] = [clpep, 0]
            elif uniquePep_dict[unique_key][dataset_run_name][0].match_score < clpep.match_score:
                uniquePep_dict[unique_key][dataset_run_name][0] = clpep

            uniquePep_dict[unique_key][dataset_run_name][1] += 1

            # Keep an example of the highest scoring peptide
            if (('pep' in uniquePep_dict[unique_key] and
                         uniquePep_dict[unique_key]['pep'].match_score > clpep.match_score) or
                    not 'pep' in uniquePep_dict[unique_key]):
                uniquePep_dict[unique_key]['pep'] = clpep

    # Write header
    header_fields = list(header_list.keys())
    clpep_fields = clpep_header()
    header_fields.extend(clpep_fields)
    writer.writerow(header_fields)

    count_dict = header_list.copy()
    for dataset_run_name in count_dict:
        count_dict[dataset_run_name] = 0
    sum_dict = count_dict.copy()

    # Write peptide rows
    for uniquePep_key in sorted(uniquePep_dict.keys()):

        tmp_dict = header_list.copy()
        tmp_dict['Unique Key'] = uniquePep_key

        for dataset_run_name in list(tmp_dict.keys())[1:]:

            if dataset_run_name in uniquePep_dict[uniquePep_key]:
                if form.cleaned_data['peptide_count']:
                    tmp_dict[dataset_run_name] = '%.2f (%s)' % (
                        uniquePep_dict[uniquePep_key][dataset_run_name][0].match_score,
                        uniquePep_dict[uniquePep_key][dataset_run_name][1])
                else:
                    tmp_dict[dataset_run_name] = '%.2f' % (
                        uniquePep_dict[uniquePep_key][dataset_run_name][0].match_score)

                count_dict[dataset_run_name] += 1
                sum_dict[dataset_run_name] += uniquePep_dict[uniquePep_key][dataset_run_name][0].match_score

        # Get values for clpep
        columns = []

        for value in clpep_fields:
            try:
                columns.append(uniquePep_dict[uniquePep_key]['pep'].__getattribute__(value))
                ## can't get something like container.location
                # columns.append( eval('obj.%s'%value) )
            except:
                columns.append('')  ## capture 'None' fields

        values = list(tmp_dict.values())
        values.extend(columns)

        writer.writerow(values)

    # Write some stats
    count_dict['Unique Key'] = 'Count'
    writer.writerow(count_dict.values())

    sum_dict['Unique Key'] = 'Match score Sum'
    writer.writerow(sum_dict.values())

    return response


def dataset_stats_csv(self, request, queryset, tableName):
    """
    Write dataset statistics about cross-linked peptides to CSV file.
    """

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = ('attachment; filename=' + tableName + '.csv')

    writer = csv.writer(response, quoting=csv.QUOTE_ALL)

    fields = OrderedDict([('Dataset', ''),
                          ('Run', ''),
                          ('PSM', 0),
                          ('Inter-protein', 0),
                          ('Inter-protein(NoDimerNoDecoy)', 0),
                          ('Intra-protein', 0),
                          ('Intra-peptide', 0),
                          ('Dead-end', 0),
                          ('Linear peptide', 0),
                          ('Min. match score', 10000000),
                          ('Average match score', 0),
                          ('Max. match score', 0),
                          ('Autovalidated', 0),
                          ('Decoy', 0),
                          ('False positive ratio', '-'),
                          ('z1', 0),
                          ('z2', 0),
                          ('z3', 0),
                          ('z4', 0),
                          ('z5', 0),
                          ('z6', 0),
                          ('z7', 0),
                          ('z8', 0),
                          ('z9', 0),
                          ])

    writer.writerow(fields.keys())

    # Iterates through datasets
    for dataset in queryset:

        fields_dict = OrderedDict([(dataset.name, fields.copy())])
        fields_dict[dataset.name]['Dataset'] = '[%s] %s' % (dataset.pk,
                                                            dataset.name)
        fields_dict[dataset.name]['Run'] = 'dataset'

        # Iterates over Clpeptides
        for clpep in dataset.clpeptide_set.all():

            if not clpep.run_name in fields_dict:
                fields_dict[clpep.run_name] = fields.copy()
                fields_dict[clpep.run_name]['Dataset'] = '[%s] %s' % (dataset.pk,
                                                                      dataset.name)
                fields_dict[clpep.run_name]['Run'] = clpep.run_name

            fields_dict[dataset.name]['PSM'] += 1
            fields_dict[clpep.run_name]['PSM'] += 1
            fields_dict[dataset.name][clpep.link_type] += 1
            fields_dict[clpep.run_name][clpep.link_type] += 1

            if (clpep.link_type == 'Inter-protein' and
                        clpep.fs_prot1_id != clpep.fs_prot2_id and
                        clpep.not_decoy is True):
                fields_dict[dataset.name]['Inter-protein(NoDimerNoDecoy)'] += 1
                fields_dict[clpep.run_name]['Inter-protein(NoDimerNoDecoy)'] += 1

            fields_dict[dataset.name]['z' + clpep.precursor_charge] += 1
            fields_dict[clpep.run_name]['z' + clpep.precursor_charge] += 1

            fields_dict[dataset.name]['Average match score'] += clpep.match_score
            fields_dict[clpep.run_name]['Average match score'] += clpep.match_score

            if clpep.match_score < fields_dict[dataset.name]['Min. match score']:
                fields_dict[dataset.name]['Min. match score'] = clpep.match_score

            if clpep.match_score < fields_dict[clpep.run_name]['Min. match score']:
                fields_dict[clpep.run_name]['Min. match score'] = clpep.match_score

            if clpep.match_score > fields_dict[dataset.name]['Max. match score']:
                fields_dict[dataset.name]['Max. match score'] = clpep.match_score

            if clpep.match_score > fields_dict[clpep.run_name]['Max. match score']:
                fields_dict[clpep.run_name]['Max. match score'] = clpep.match_score

            if clpep.autovalidated:
                fields_dict[dataset.name]['Autovalidated'] += 1
                fields_dict[clpep.run_name]['Autovalidated'] += 1

            if not clpep.not_decoy:
                fields_dict[dataset.name]['Decoy'] += 1
                fields_dict[clpep.run_name]['Decoy'] += 1

        for dataset_run in sorted(fields_dict.keys()):

            if fields_dict[dataset_run]['PSM'] != 0:
                fields_dict[dataset_run]['Average match score'] /= fields_dict[dataset_run]['PSM']
            else:
                fields_dict[dataset_run]['Average match score'] = 0

            # Skip MS run stats if unique filter applied to whole dataset
            if (hasattr(dataset, 'clpeptidefilter') and
                        dataset.clpeptidefilter is not None):

                if dataset.clpeptidefilter.unique_in == 'dataset':
                    if fields_dict[dataset_run]['Run'] != 'dataset':
                        continue
                elif dataset.clpeptidefilter.unique_in == 'msrun':
                    if fields_dict[dataset_run]['Run'] == 'dataset':
                        continue

            if fields_dict[dataset_run]['PSM'] != 0:
                fields_dict[dataset_run]['False positive ratio'] = '%.2f' \
                                                                   % (float(fields_dict[dataset_run]['Decoy']) / float(
                    fields_dict[dataset_run]['PSM']))

            writer.writerow(fields_dict[dataset_run].values())

    return response


def make_Percolator_tsv(self, request, clpeptide_set, tableName, fh=None, nv=False):
    """
    Export selected objects as TSV file
    """

    clpeptide_set = clpeptide_set.filter(cross_link=True)

    response = HttpResponse(content_type='text/tsv')
    response['Content-Disposition'] = ('attachment; filename=' + tableName +
                                       '_Percolator_' + request.META['QUERY_STRING'] + '.tsv')

    # Output to file
    if fh is not None:
        response = fh

    response.write(
        'ClPeptideId\tLabel\tScanNr\tScore\tdScore\tCharge\tMass\tPPM\tLenShort\tLenLong\tLenSum\tPeptide\tProteinId1\tProteinId2\n')

    writer = csv.writer(response, delimiter='\t')

    for clpep in clpeptide_set:
        columns = []
        columns.append(clpep.id)
        label = 1
        if clpep.not_decoy is False:
            label = -1
        columns.append(label)
        columns.append(clpep.scan_number)

        v = [clpep.match_score,
             clpep.delta,
             int(clpep.precursor_charge),
             clpep.precursor_mz * int(clpep.precursor_charge) - int(clpep.precursor_charge) * 1.00727646677,
             clpep.error,
             min(len(clpep.peptide_wo_mod1), len(clpep.peptide_wo_mod2)),
             max(len(clpep.peptide_wo_mod1), len(clpep.peptide_wo_mod2)),
             len(clpep.peptide_wo_mod1) + len(clpep.peptide_wo_mod2)
             ]

        # Normalize vector as suggested by Sebastien
        if nv:
            v = v / LA.norm(v)

        columns.extend(v)

        #         columns.append(clpep.match_score)
        #         columns.append(clpep.delta)
        #         columns.append(clpep.precursor_charge)
        #         columns.append(clpep.precursor_mz * int(clpep.precursor_charge) - int(clpep.precursor_charge) * 1.00727646677)
        #         columns.append(clpep.error)
        #         columns.append(min(len(clpep.peptide_wo_mod1), len(clpep.peptide_wo_mod2)))
        #         columns.append(max(len(clpep.peptide_wo_mod1), len(clpep.peptide_wo_mod2)))
        #         columns.append(len(clpep.peptide_wo_mod1) + len(clpep.peptide_wo_mod2))
        columns.append(''.join(
            ['-.', clpep.peptide1, '(', str(clpep.pep1_link_pos), ')--', clpep.peptide2, '(', str(clpep.pep2_link_pos),
             ').-']))
        columns.append('"' + clpep.display_protein1 + '"')
        columns.append('"' + clpep.display_protein2 + '"')
        # columns = [str(x).encode() for x in columns]
        # print(columns)
        writer.writerow(columns)

    return response


def make_Xi_csv(self, request, queryset, tableName, clpep_distances=False):
    """
    Export selected objects as CSV file
    """

    fields = clpep_header()

    if clpep_distances == True:
        fields.append('min_distance')
        fields.append('distances')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = ('attachment; filename=' + tableName +
                                       '_' + request.META['QUERY_STRING'] + '.csv')

    response.write('Filter: { ' + request.get_full_path() + ' }\n\n')

    writer = csv.writer(response, quoting=csv.QUOTE_ALL)
    writer.writerow(fields)

    fix_fields = ['pep1_link_pos', 'pep2_link_pos', 'peptide_position1', 'peptide_position2']

    for obj in queryset:
        columns = []

        for value in fields:
            try:
                if value in fix_fields:
                    columns.append(obj.__getattribute__(value) - 1)
                else:
                    columns.append(obj.__getattribute__(value))
                    ## can't get something like container.location
                    # columns.append( eval('obj.%s'%value) )
            except:
                columns.append('')  ## capture 'None' fields

        writer.writerow(columns)

    return response


def interaction_matrix(self, request, clpeptide_set, tableName):
    """
    Export selected objects to protein interactions matrix
    CSV file.
    """

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = ('attachment; filename=' + tableName +
                                       '_' + request.META['QUERY_STRING'] +
                                       '_interaction_matrix.csv')

    writer = csv.writer(response, delimiter=',', quoting=csv.QUOTE_NONE)

    # Filter inter-protein cross-linked peptides
    clpeptide_set = clpeptide_set.filter(link_type='Inter-protein',
                                         not_decoy=True)

    # Merge interaction in one entry and compute sum of scores
    protein_pairs = dict()
    protein_ids = dict()

    for clpep in clpeptide_set:

        key = [clpep.fs_prot1_id.pk, clpep.fs_prot2_id.pk]
        key.sort()

        # Keep protein names
        if clpep.fs_prot1_id.pk not in protein_ids:
            protein_ids[clpep.fs_prot1_id.pk] = '%s (%s)' % (clpep.fs_prot1_id.gene_name,
                                                             clpep.fs_prot1_id.identifier)

        if clpep.fs_prot2_id.pk not in protein_ids:
            protein_ids[clpep.fs_prot2_id.pk] = '%s (%s)' % (clpep.fs_prot2_id.gene_name,
                                                             clpep.fs_prot2_id.identifier)

        key = tuple(key)

        if key not in protein_pairs:
            protein_pairs[key] = [0, 0]

        protein_pairs[key][0] += clpep.match_score
        protein_pairs[key][1] += 1

    # Sort proteins and write header
    sorted_proteins = sorted([(value, key) for (key, value) in protein_ids.items()])

    columns = ['Interaction matrix']

    for protein_name, protein_id in sorted_proteins:
        columns.append(protein_name)

    writer.writerow(columns)

    # Write values to file
    for protein_name, protein_id in sorted_proteins:
        columns = [protein_name]

        for protein_name2, protein_id2 in sorted_proteins:

            key = [protein_id, protein_id2]
            key.sort()
            key = tuple(key)

            if key in protein_pairs:
                columns.append('%.2f (%s)' % (protein_pairs[key][0], protein_pairs[key][1]))
            else:
                columns.append('')

        writer.writerow(columns)

    return response


# def make_XiNET_csv(self, request, queryset, tableName):
#     """
#     Export selected objects as XiNET CSV file
#     """
#     
#     fields = ['match_score',
#               'display_protein1',
#               'display_protein2',
#               'residue1',
#               'residue2',
#               ]
# 
#     response = HttpResponse(mimetype='text/csv')
#     response['Content-Disposition'] = ('attachment; filename=' + tableName + 
#                                        '_' + request.META['QUERY_STRING'] + '_xiNET.csv')
#     
#     response.write('Score,Protein1,Protein2,Residue1,Residue2\n')
# 
#     writer = csv.writer(response)
# 
# 
#     
# 
#     for obj in queryset:
#         columns = []
#         
#         obj.residue1 = obj.__getattribute__('peptide_position1') + obj.__getattribute__('pep1_link_pos') - 1
#         obj.residue2 = obj.__getattribute__('peptide_position2') + obj.__getattribute__('pep2_link_pos') - 1
# 
#         for value in fields:
#             try:
#                 columns.append( obj.__getattribute__(value))  
#                 ## can't get something like container.location
#                 #columns.append( eval('obj.%s'%value) )
#             except:
#                 columns.append('')  ## capture 'None' fields
# 
#         writer.writerow( columns )
# 
#     return response


def make_ProteoProfile_csv(self, request, queryset, tableName):
    """
    Export selected objects as ProteoProfile CSV file
    Thibault's lab internal format
    """

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = ('attachment; filename=ProteoProfile_' + tableName +
                                       '_' + request.META['QUERY_STRING'] + '.csv')

    fields = ['Search Log Num', 'FileName', 'idIpeptide', 'UniProt ID',
              'UniProt URL', 'PIR URL', 'EntrezID', 'Entrez URL',
              'Protein Description', 'Species', 'Mass', 'Num of Peptides',
              'Peptide QueryNum', 'Peptide Sequence', 'Pep Modification',
              'Protein Assignments', 'Peptide Start', 'Peptide End',
              'Pep Score', 'Pep Rank', 'Pep Observed Mz', 'Pep Calc Mass',
              'Peptide Observed Mass', 'Peptide Charge', 'Pep Elution Time',
              'Pep Sample File', 'Peptide URL', 'Protein Score', 'Prot. PI',
              'Prot. Seq Length', 'Seq Coverage', 'PubMedID', 'MedLineID',
              'Scan Number']

    writer = csv.writer(response)

    # Write header    
    writer.writerow(['searchlog='])
    writer.writerow(['FileName=;'])
    writer.writerow(['database=CLMSVault'])
    writer.writerow(fields)

    for obj in queryset:
        columns = []
        columns.append('')
        columns.append(obj.run_name)
        columns.append(obj.pk)

        identifier1 = ''
        description1 = ''
        species1 = ''
        try:
            identifier1 = obj.fs_prot1_id.identifier
            description1 = obj.fs_prot1_id.description
            species1 = obj.fs_prot1_id.species
        except:
            if obj.peptide1:
                identifier1 = 'DECOY'
                description1 = 'DECOY'
                species1 = 'Unknown'

        identifier2 = ''
        description2 = ''
        species2 = ''
        try:
            identifier2 = obj.fs_prot2_id.identifier
            description2 = obj.fs_prot2_id.description
            species2 = obj.fs_prot2_id.species
        except:
            if obj.peptide2:
                identifier2 = 'DECOY'
                description2 = 'DECOY'
                species2 = 'Unknown'

        separator = '~~'

        columns.append(separator.join([identifier1, identifier2]))
        columns.append('')
        columns.append('')
        columns.append('')
        columns.append('')
        columns.append(separator.join([description1, description2]))
        columns.append(separator.join([species1, species2]))
        columns.append(0)
        columns.append(0)
        columns.append(0)
        columns.append(separator.join([obj.peptide1, obj.peptide2]))
        columns.append(obj.link_type + '-' + str(obj.pep1_link_pos) + separator + str(obj.pep2_link_pos))
        columns.append('-')
        columns.append('.'.join([str(obj.peptide_position1), str(obj.peptide_position2)]))
        columns.append('.'.join([str(obj.peptide_position1 + len(obj.peptide_wo_mod1) - 1),
                                 str(obj.peptide_position2 + len(obj.peptide_wo_mod2) - 1)]))
        columns.append(obj.match_score)
        columns.append(1)
        columns.append(obj.precursor_mz)
        columns.append(0)

        observed_mass = obj.precursor_mz * float(obj.precursor_charge) - float(obj.precursor_charge) * 1.007825035

        columns.append(observed_mass)
        columns.append(int(obj.precursor_charge))
        columns.append(-1)
        columns.append('')
        columns.append('-')
        columns.append(0)
        columns.append(0)
        columns.append(0)
        columns.append('')
        columns.append('')
        columns.append('')
        columns.append(obj.scan_number)

        writer.writerow(columns)

    return response


def make_xTract_csv(self, request, queryset, tableName):
    """
    Export selected objects as xTract CSV file
    """

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = ('attachment; filename=xTract_' + tableName +
                                       '_' + request.META['QUERY_STRING'] + '.csv')

    fields = ['scan', 'seq', 'prot', 'mod', 'score', 'type', 'mz', 'z', 'tr', 'fdr', 'uxID', 'preINT']

    writer = csv.writer(response)

    # Write header
    writer.writerow(fields)

    for obj in queryset:

        if obj.fs_prot1_id is None or obj.fs_prot2_id is None:
            continue

        columns = []

        scan = '%s.c.%s.%s.%s' % (obj.run_name, obj.scan_number,
                                  obj.scan_number, int(obj.precursor_charge))

        columns.append(scan)

        alpha = {
            'sequence': obj.peptide_wo_mod1,
            'protein': obj.fs_prot1_id.identifier,
            'link_pos': obj.pep1_link_pos,
            'peptide_pos': obj.peptide_position1,
            'mod_list': list()
        }

        if obj.peptide_wo_mod1 != obj.peptide1:

            obj.peptide1 = obj.peptide1.replace('ox', '[ox]')
            obj.peptide1 = obj.peptide1.replace('cm', '[cm]')

            mod_pos = 0
            mod = ''
            pos = 0

            for res in obj.peptide1:

                if res == '[':
                    mod_pos = pos
                elif res == ']':
                    alpha['mod_list'].append((mod_pos, mod))
                    mod_pos = 0
                    mod = ''

                elif mod_pos:
                    mod += res
                else:

                    pos += 1

        beta = {
            'sequence': obj.peptide_wo_mod2,
            'protein': obj.fs_prot2_id.identifier,
            'link_pos': obj.pep2_link_pos,
            'peptide_pos': obj.peptide_position2,
            'mod_list': list()
        }

        # code duplicated (refactor)
        if obj.peptide_wo_mod2 != obj.peptide2:

            obj.peptide2 = obj.peptide2.replace('ox', '[ox]')
            obj.peptide2 = obj.peptide2.replace('cm', '[cm]')

            mod_pos = 0
            mod = ''
            pos = 0

            for res in obj.peptide2:

                if res == '[':
                    mod_pos = pos
                elif res == ']':
                    beta['mod_list'].append((mod_pos, mod))
                    mod_pos = 0
                    mod = ''

                elif mod_pos:
                    mod += res
                else:

                    pos += 1

        swap = False

        if len(beta['sequence']) > len(alpha['sequence']):

            swap = True

        elif len(beta['sequence']) == len(alpha['sequence']):

            sorted_peptides = sorted([alpha['sequence'], beta['sequence']])

            if sorted_peptides[0] != alpha['sequence']:
                swap = True

        if swap:
            alpha, beta = beta, alpha

        seq = '%s-%s-a%s-b%s' % (alpha['sequence'], beta['sequence'],
                                 alpha['link_pos'], beta['link_pos'])

        columns.append(seq)

        prot = '%s:%s' % (alpha['protein'], beta['protein'])

        columns.append(prot)

        combined_mods = list()

        mod_dict = {
            '57.02': '57.02146',
            '15.99': '15.994915',
            '156.08': 'na',
            'cm': '57.02146',
            'ox': '15.994915',
        }

        for mod in alpha['mod_list']:
            mod_fix = mod_dict[mod[1]]
            mod_str = '%s-a=%s' % (mod[0], mod_fix)
            combined_mods.append(mod_str)

        for mod in beta['mod_list']:
            mod_fix = mod_dict[mod[1]]
            mod_str = '%s-b=%s' % (mod[0], mod_fix)
            combined_mods.append(mod_str)

        columns.append(':'.join(combined_mods))

        columns.append(obj.match_score)
        columns.append('xlink:light')
        columns.append(obj.precursor_mz)
        columns.append(obj.precursor_charge)
        columns.append(obj.retention_time)
        columns.append(0)

        uxid = '%s:%s:x:%s:%s' % (alpha['protein'],
                                  alpha['peptide_pos'] + alpha['link_pos'] - 1,
                                  beta['protein'],
                                  beta['peptide_pos'] + beta['link_pos'] - 1,)

        columns.append(uxid)
        columns.append('')

        writer.writerow(columns)

    return response


def make_xlink_analyzer_csv(self, request, queryset, table_name, form):
    """
    Export selected objects as XlinkAnalyzer CSV file
    :param self: Self
    :param request: Django request object
    :param queryset: CLPeptide queryset
    :param table_name: String
    :param form: PDBSelectionForm
    :return: HttpResponse
    """

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = ('attachment; filename=XlinkAnalyzer_'
                                       + table_name + '_'
                                       + request.META['QUERY_STRING'] + '.csv')

    # Write header
    writer = csv.writer(response)

    fields = ['Id', 'Protein1', 'Protein2', 'AbsPos1', 'AbsPos2', 'score']
    writer.writerow(fields)

    max_score = None
    pdb = form.get_PDB()

    ukey = dict()
    ukey['proteinPpos'] = True

    try:
        clpeptide_set, alignments = compute_cl_distance(queryset,
                                                        pdb,
                                                        form.cleaned_data['protein_identity'],
                                                        form.cleaned_data['peptide_identity'],
                                                        unique_key=ukey)
    except InvalidFileFormatException as e:
        return HttpResponse('<h1>%s</h1>' % e)

    for clpep in clpeptide_set:

        if clpep.fs_prot1_id is None or clpep.fs_prot2_id is None:
            continue

        alpha = {
            'sequence': clpep.peptide_wo_mod1,
            'protein': clpep.fs_prot1_id.identifier,
            'link_pos': clpep.pep1_link_pos,
            'peptide_pos': clpep.peptide_position1,
        }

        beta = {
            'sequence': clpep.peptide_wo_mod2,
            'protein': clpep.fs_prot2_id.identifier,
            'link_pos': clpep.pep2_link_pos,
            'peptide_pos': clpep.peptide_position2,
        }

        swap = False

        if len(beta['sequence']) > len(alpha['sequence']):

            swap = True

        elif len(beta['sequence']) == len(alpha['sequence']):

            sorted_peptides = sorted([alpha['sequence'], beta['sequence']])

            if sorted_peptides[0] != alpha['sequence']:
                swap = True

        if swap:
            alpha, beta = beta, alpha

        seq = '%s-%s-a%s-b%s' % (alpha['sequence'], beta['sequence'],
                                 alpha['link_pos'], beta['link_pos'])

        if max_score is None:
            max_score = clpep.match_score

        for cldistance in clpep.distances:

            if cldistance.distance == '':
                continue

            if swap:
                cldistance.residue_1_str, cldistance.residue_2_str = cldistance.residue_2_str, cldistance.residue_1_str

            columns = list()
            columns.append(seq)
            columns.append(cldistance.residue_1.get_parent().get_id())
            columns.append(cldistance.residue_2.get_parent().get_id())
            columns.append(cldistance.residue_1.get_id()[1])
            columns.append(cldistance.residue_2.get_id()[1])
            columns.append(int(clpep.match_score / max_score * 100))

            writer.writerow(columns)

    return response


def psi_mi_tab25(self, request, clpeptide_set, tableName):
    """
    Export selected objects to PSI-MI TAB 2.5
    tabular file.
    """

    fields = ['#ID Interactor A',
              'ID Interactor B',
              'Alt IDs Interactor A',
              'Alt IDs Interactor B',
              'Aliases Interactor A',
              'Aliases Interactor B',
              'Interaction Detection Method',
              'Publication 1st Author',
              'Publication Identifiers',
              'Taxid Interactor A',
              'Taxid Interactor B',
              'Interaction Types',
              'Source Database',
              'Interaction Identifiers',
              'Confidence Values'
              ]

    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = ('attachment; filename=' + tableName +
                                       '_' + request.META['QUERY_STRING'] + '_PSI-MI_TAB_2.5.txt')

    writer = csv.writer(response, delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='\x07')
    writer.writerow(fields)

    # Filter inter-protein cross-linked peptides
    clpeptide_set = clpeptide_set.filter(link_type='Inter-protein',
                                         not_decoy=True)

    # Merge interaction in one entry and compute sum of scores
    protein_pairs = dict()

    for clpep in clpeptide_set:

        if clpep.fs_prot1_id is None or clpep.fs_prot2_id is None:
            continue

        key = [clpep.fs_prot1_id.pk, clpep.fs_prot2_id.pk]
        key.sort()
        key = tuple(key)

        if key not in protein_pairs:
            protein_pairs[key] = [clpep, 0, 0, []]

        protein_pairs[key][1] += clpep.match_score
        protein_pairs[key][2] += 1
        protein_pairs[key][3].append(clpep.pk)

    # Write values to file
    for key, value in protein_pairs.items():
        columns = []

        clpep = value[0]

        columns.append('custom_id:%s|CLMSVault:%s' % (clpep.fs_prot1_id.identifier, key[0]))
        columns.append('custom_id:%s|CLMSVault:%s' % (clpep.fs_prot2_id.identifier, key[1]))
        columns.append('entrez gene/locuslink:%s' % (clpep.fs_prot1_id.gene_name))
        columns.append('entrez gene/locuslink:%s' % (clpep.fs_prot2_id.gene_name))
        columns.append('-')
        columns.append('-')
        columns.append('psi-mi:"MI:0031"(protein cross-linking with a bifunctional reagent)')
        columns.append('-')
        columns.append('pubmed:-')
        columns.append('taxid:-')
        columns.append('taxid:-')
        columns.append('psi-mi:"MI:0915"(physical association)')
        columns.append('psi-mi:"MI:0489"(source database)')
        columns.append('CLMSVault:%s' % value[3])
        columns.append('%.2f (%s)' % (value[1], value[2]))

        writer.writerow(columns)

    return response


def psi_mi_xml25(self, request, queryset, tableName):
    """
    Export selected objects to PSI-MI XML 2.5
    tabular file.
    """

    expShortDescription = 'CLMSVault Datasets' + str([x.pk for x in queryset])
    expFullDescription = 'CLMSVault Datasets' + str([(str(x.pk) + '-' + str(x.name)) for x in queryset])

    # Filter inter-protein cross-linked peptides
    clpeptide_set = dataset_set_2_clpeptide_set(queryset)
    clpeptide_set = clpeptide_set.filter(link_type='Inter-protein',
                                         not_decoy=True)

    # Merge interaction in one entry and compute sum of scores
    proteins = dict()
    protein_pairs = dict()

    for clpep in clpeptide_set:

        if clpep.fs_prot1_id is None or clpep.fs_prot2_id is None:
            continue

        key = [clpep.fs_prot1_id.pk, clpep.fs_prot2_id.pk]
        key.sort()
        key = tuple(key)
        proteins[clpep.fs_prot1_id.pk] = clpep.fs_prot1_id
        proteins[clpep.fs_prot2_id.pk] = clpep.fs_prot2_id

        if key not in protein_pairs:
            protein_pairs[key] = [clpep, 0, 0, []]

        protein_pairs[key][1] += clpep.match_score
        protein_pairs[key][2] += 1
        protein_pairs[key][3].append(clpep.pk)

    t = loader.get_template('psi-mi_25.xml')
    c = {'expShortDescription': expShortDescription,
         'expFullDescription': expFullDescription,
         'proteins': proteins,
         'protein_pairs': protein_pairs,
         'pCounter': 0}

    response = HttpResponse(t.render(c), content_type='text/xml')
    response['Content-Disposition'] = ('attachment; filename=' + tableName +
                                       '_' + request.META['QUERY_STRING'] + '_PSI-MI_XML_2.5.xml')

    return response
