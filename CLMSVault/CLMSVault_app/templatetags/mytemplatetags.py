## Copyright 2013-2014 Mathieu Courcelles
## Mike Tyers's lab / IRIC / Universite de Montreal 

# Import standard libraries
from collections import OrderedDict
import copy

# Import Django related libraries
from django import template
from django.conf import settings
from django.template import Library

register = Library()


@register.filter
def demoSite(value):
    allowed_hosts = getattr(settings, 'ALLOWED_HOSTS', '')

    if 'demo' in str(allowed_hosts):
        return True

    return False


@register.filter
def inhouseSite(value):
    allowed_hosts = getattr(settings, 'ALLOWED_HOSTS', '')

    if 'iric.ca' in str(allowed_hosts) or 'tyerslab.com' in str(allowed_hosts):
        return True

    return False


@register.filter
def subtract(value, arg):
    return value - arg


@register.filter
def participantCounter(value):
    participantCounter.x += 1
    return participantCounter.x


participantCounter.x = 0


@register.filter
def participantReset(value):
    participantCounter.x = 0
    return u''


@register.filter
def zero2one(value):
    if value == 0:
        return 1
    else:
        return value


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key, '-')


# Form splitting/Fieldset templatetag
# Modified from https://djangosnippets.org/snippets/1019/
def get_fieldset(parser, token):
    try:
        method, name, fields, as_, variable_name, from_, form = token.split_contents()

    except ValueError:
        raise template.TemplateSyntaxError('bad arguments for %r' % token.split_contents()[0])

    return FieldSetNode(fields.split(','), name, variable_name, form)


get_fieldset = register.tag(get_fieldset)


# Form splitting/Fieldset templatetag
# Modified from https://djangosnippets.org/snippets/1019/
class FieldSetNode(template.Node):
    def __init__(self, fields, name, variable_name, form_variable):
        self.fields = fields
        self.variable_name = variable_name
        self.form_variable = form_variable
        self.name = name

    def render(self, context):
        form = template.Variable(self.form_variable).resolve(context)
        new_form = copy.copy(form)
        new_form.fields = OrderedDict([(key, form.fields[key]) for key in self.fields])
        new_form.name = self.name.replace('_', ' ')

        def as_grapelli():
            "Returns this form rendered as HTML <p>s."
            return new_form._html_output(
                normal_row='<div class="grp-row grp-cells-1"><div class="l-2c-fluid l-d-4"><div class="c-1">%(label)s<a%(html_class_attr)s></a></div> <div class="c-2">%(field)s%(help_text)s</div></div></div>',
                error_row='<div class="grp-row grp-cells-1">%s (fix below)</div>',
                row_ender='',
                help_text_html='<br />%s',
                errors_on_separate_row=True)

        new_form.as_grapelli = as_grapelli()
        context[self.variable_name] = new_form

        return u''
