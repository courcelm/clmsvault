# Copyright 2013-2017 Mathieu Courcelles
# Mike Tyers's lab / IRIC / Universite de Montreal


# Import standard libraries
import csv
import logging
import re

# Import Django related libraries

# Import project libraries
from ..models import CLPeptide
from ..models import RawDataset
from .exception import InvalidFileFormatException
from .sequences import sequencesMatcher

kojak_version = {
    '1.0-1.5': 'Scan Number\tObs Mass\tCharge\tPSM Mass\tPPM Error\tScore\tdScore\tPep. Diff.\tPeptide #1\tLink #1\tProtein #1\tPeptide #2\tLink #2\tProtein #2\tLinker Mass',
    '1.6-dev': 'Scan Number\tRet Time\tObs Mass\tCharge\tPSM Mass\tPPM Error\tScore\tdScore\tPep. Diff.\tPeptide #1\tLink #1\tProtein #1\tPeptide #2\tLink #2\tProtein #2\tLinker Mass',
    '1.6': 'Scan Number\tRet Time\tObs Mass\tCharge\tPSM Mass\tPPM Error\tScore\tdScore\tPeptide #1 Score\tPeptide #1\tLinked AA #1\tProtein #1\tProtein #1 Site\tPeptide #2 Score\tPeptide #2\tLinked AA #2\tProtein #2\tProtein #2 Site\tLinker Mass'
}


class KojakParser:
    """
    Parser for Kojak results.
    """

    @staticmethod
    def parseResults(dataset):
        """
        This method parses Kojak results file provided by the dataset.
        Sucess or failure of this procedure is noted in the dataset object.
        """

        # Prepare sequenceMatcher
        sM = sequencesMatcher(dataset.fasta_db.pk)

        try:
            with open(dataset.file.path, 'r') as f:

                # Set run name with file name
                run_name = dataset.file.path
                run_name = re.sub('(^.+[/\\\]\d+-)', '', run_name)
                run_name = run_name.replace('.kojak.txt', '')

                # Check header to validate file format
                line = f.readline().rstrip('\n\r')

                # Is this file merged?
                if line.startswith('run_name='):
                    run_name = line.split('=')[1]
                    line = f.readline().rstrip('\n\r')

                    # Check header to validate file format
                header_template = 'Kojak version'

                if not line.startswith(header_template):
                    raise InvalidFileFormatException(
                        'Uploaded extra file is not recognized as a '
                        'Kojak results file (bad file header row 1)')

                # Create CSV reader
                reader = csv.DictReader(f, delimiter='\t')

                header_file = '\t'.join(name for name in reader.fieldnames)

                version = None

                for item_version, header in kojak_version.items():

                    if header == header_file:
                        version = item_version
                        break
                else:
                    raise InvalidFileFormatException(
                        'Uploaded extra file is not recognized as a Kojak '
                        'results file (bad file header row)')

                protpos_pattern = re.compile('(^.+)\((\d+)\)')

                pep_pos_dict = dict()

                clpep_list = []
                through_list = []

                # import time
                # start_time = time.time()

                # next_id = CLPeptide.objects.latest('id').id + 1


                # Access the through model directly
                # ThroughModel = CLPeptide.dataset.through


                # Iterate through lines and create clPeptide
                for row in reader:

                    # Is this file merged?
                    if row['Scan Number'].startswith('run_name='):
                        run_name = row['Scan Number'].split('=')[1]
                        continue

                    if row['Scan Number'].startswith('Scan') or row['Scan Number'].startswith('Kojak'):
                        continue

                    # Discard scan without match
                    if row['Charge'] == '0':
                        continue

                    # Format each field to the right format for the data model
                    fields = dict()

                    fields['run_name'] = run_name
                    fields['scan_number'] = row['Scan Number']
                    charge = float(row['Charge'])
                    fields['precursor_mz'] = '%4f' % ((float(row['Obs Mass']) + charge * 1.00727646677) / charge)
                    fields['precursor_charge'] = row['Charge']
                    fields['precursor_intensity'] = -1
                    fields['rank'] = -1
                    fields['match_score'] = row['Score']
                    fields['spectrum_intensity_coverage'] = -1
                    fields['total_fragment_matches'] = -1
                    fields['delta'] = row['dScore']
                    fields['error'] = row['PPM Error']

                    row['Peptide #1'] = row['Peptide #1'].replace('-15N', '')
                    fields['peptide1'] = row['Peptide #1']
                    fields['peptide_wo_mod1'] = re.sub('(\[.+?\])', '', row['Peptide #1'])

                    fields['peptide_position1'] = -1

                    row['Peptide #2'] = row['Peptide #2'].replace('-15N', '')
                    fields['peptide2'] = row['Peptide #2']
                    fields['peptide_wo_mod2'] = re.sub('(\[.+?\])', '', row['Peptide #2'])
                    fields['peptide_position2'] = -1

                    if version == '1.6':
                        fields['pep1_link_pos'] = int(row['Linked AA #1'])
                        fields['pep2_link_pos'] = int(row['Linked AA #2'])

                        fields['n_score_a'] = float(row['Peptide #1 Score'])
                        fields['n_score_b'] = float(row['Peptide #2 Score'])

                    else:
                        fields['pep1_link_pos'] = int(row['Link #1'])
                        fields['pep2_link_pos'] = int(row['Link #2'])

                    if 'Ret Time' in row:
                        fields['retention_time'] = float(row['Ret Time']) * 60

                    fields['display_protein1'] = row['Protein #1'].split(';')[0]
                    fields['display_protein1'] = re.sub('(^>)', '', fields['display_protein1'])
                    result = protpos_pattern.match(fields['display_protein1'])

                    if result:
                        fields['display_protein1'] = result.group(1)
                        fields['peptide_position1'] = int(result.group(2)) - int(fields['pep1_link_pos']) + 1

                    fields['display_protein2'] = row['Protein #2'].split(';')[0]
                    fields['display_protein2'] = re.sub('(^>)', '', fields['display_protein2'])
                    result = protpos_pattern.match(fields['display_protein2'])

                    if result:
                        fields['display_protein2'] = result.group(1)
                        fields['peptide_position2'] = int(result.group(2)) - int(fields['pep2_link_pos']) + 1



                    fields['autovalidated'] = False
                    fields['validated'] = ''
                    fields['rejected'] = False
                    fields['notes'] = ''
                    fields['not_decoy'] = True
                    if 'decoy' in row['Protein #1'] or 'decoy' in row['Protein #2']:
                        fields['not_decoy'] = False

                    # Create the CLPeptide object
                    clpep = CLPeptide(**fields)

                    # Match protein sequences
                    clpep.fs_prot1_id = sM.sequencePk(clpep.display_protein1)
                    clpep.fs_prot2_id = sM.sequencePk(clpep.display_protein2)

                    # Find peptide position for linear peptide
                    if fields['peptide_position1'] == -1 and clpep.fs_prot1_id is not None:

                        key = f'{fields["peptide_wo_mod1"]}-{clpep.fs_prot1_id.pk}'

                        if key in pep_pos_dict:
                            clpep.peptide_position1 = pep_pos_dict[key]

                        else:
                            clpep.peptide_position1 = clpep.fs_prot1_id.sequence.find(fields['peptide_wo_mod1']) + 1
                            pep_pos_dict[key] = clpep.peptide_position1

                        # Check DSS dead end - this could be improved
                        pepseq = re.sub('(K\[.+?\])', 'k', row['Peptide #1'])
                        pepseq = re.sub('(\[.+?\])', '', pepseq)

                        pos = pepseq.find('k')

                        if pos > -1:
                            clpep.pep1_link_pos = pos + 1

                    if fields['peptide_position2'] == -1 and clpep.fs_prot2_id is not None:

                        key = f'{fields["peptide_wo_mod2"]}-{clpep.fs_prot2_id.pk}'

                        if key in pep_pos_dict:
                            clpep.peptide_position2 = pep_pos_dict[key]

                        else:
                            clpep.peptide_position2 = clpep.fs_prot2_id.sequence.find(fields['peptide_wo_mod2']) + 1
                            pep_pos_dict[key] = clpep.peptide_position2

                    if clpep.peptide_position2 == -1 and fields['pep2_link_pos'] != -1:
                        clpep.peptide_position2 = clpep.peptide_position1

                    clpep.guessLinkType()
                    # clpep.id = next_id

                    # Save object to db
                    clpep.save()
                    clpep.dataset.add(dataset.pk)

                    # clpep_list.append(clpep)
                    # through_list.append(ThroughModel(clpeptide_id= next_id, dataset_id=dataset.pk))
                    # next_id += 1

                # Save object to db
                # CLPeptide.objects.bulk_create(clpep_list, batch_size=999)
                # clpep.save()

                # Link object to dataset
                # ThroughModel.objects.bulk_create(through_list, batch_size=999)

                # Append filter string to dataset description
                RawDataset.objects.filter(pk=dataset.id).update(
                    parsing_log='Ok', parsing_status=True)

        except (InvalidFileFormatException, ValueError) as e:
            RawDataset.objects.filter(pk=dataset.id).update(
                parsing_status=False, parsing_log='Error: ' + e.value)

            msg = 'Error while parsing Kojak results.'

            logger = logging.getLogger('django')
            logger.error(msg, exc_info=True)
