"""
Copyright 2013-2018 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses
Mike Tyers's lab
Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries

# Import Django related libraries

# Import project libraries
from ..models import FastaDb_Sequence


class sequencesMatcher:
    """
    This class match Fasta protein sequences to identifier
    in cross-linked peptides.
    """

    def __init__(self, fastadb_pk):

        self.fastadb_pk = fastadb_pk
        self.key_fs = dict()

    def sequencePk(self, key):
        """
        Returns primary key of a protein sequence
        :param key: Protein identifier string
        :return: Integer
        """

        if key == '-':
            return None

        key = key.rstrip()

        # Look in cache dict for key first
        cache_key = key + '-' + str(self.fastadb_pk)

        if cache_key in self.key_fs:

            return self.key_fs[cache_key]

        # Look in database if not found
        else:

            fs = None
            fs_manager = FastaDb_Sequence.objects.filter(fastadb__pk=self.fastadb_pk,
                                                         raw_description__contains=key)

            if fs_manager.count():
                fs = fs_manager[0]

            # Cache result
            self.key_fs[cache_key] = fs

            return fs
