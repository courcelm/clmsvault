"""
Copyright 2013-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses
Mike Tyers's lab
Pierre Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries

# Import Django related libraries


# Import project libraries
from ..models import FastaDB
from ..models import FastaDb_Sequence

# Import third-party libraries
from Bio import SeqIO



class FastaParser:
    """
    Parse FASTA file and stores entries in database.
    """


    @staticmethod
    def parseFASTA(fastadb_instance):
        """
        Parse FASTA file and stores entries in database.
        """

        parsing_log = ''

        handle = open(fastadb_instance.file.path, 'rU')

        sequence_count = 0

        # Iterate through sequences
        for record in SeqIO.parse(handle, 'fasta'):
            fs_instance = FastaDb_Sequence(fastadb=fastadb_instance,
                                           raw_description=record.description,
                                           sequence=record.seq)

            parsing_log = fs_instance.extractFeatures(fastadb_instance)

            if parsing_log != '':
                break

            fs_instance.save()

            sequence_count += 1

        handle.close()

        if parsing_log == '' and sequence_count != 0:
            FastaDB.objects.filter(pk=fastadb_instance.id).update(
                parsing_log='Ok',
                parsing_status=True,
                sequence_count=sequence_count
            )
        else:

            if sequence_count == 0:
                parsing_log += ' No protein sequence found!'

            FastaDB.objects.filter(pk=fastadb_instance.id).update(
                parsing_log='Error: ' + parsing_log,
                parsing_status=False,
                sequence_count=sequence_count
            )
