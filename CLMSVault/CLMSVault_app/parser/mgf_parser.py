"""
Copyright 2015-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""


# Import standard libraries
import re

# Import project libraries

# Create your views here.


class MGFParser(object):
    def __init__(self, file_name):

        self.file_name = file_name

    @staticmethod
    def scan_total_ion_current(peak_list):
        """
        Sums the intensity of all peaks
        :param peak_list: List of [mz,intensity]
        :return: Float
        """

        tic = 0

        for mz, intensity in peak_list:
            tic += intensity

        return tic

    def parse_next_scan(self):
        """
        Extract values from one scan.
        :return: Dictionary
        """

        scan = dict()
        scan['peak_list'] = list()

        # Search beginning of the scan
        scan['scan_file_index'] = self.fh.tell()
        line = self.fh.readline()
        #print(scan['scan_file_index'])
        #print(line)

        while not line.startswith('BEGIN IONS'):
            #print('hi')
            scan['scan_file_index'] = self.fh.tell()
            line = self.fh.readline()

            if line == '':
                return None

        # Read fields
        while line:
            line = self.fh.readline().strip()

            if '=' in line:
                part1, part2 = (line.split('=', 1))
                scan[part1] = part2

            else:

                if line.startswith('END'):
                    line = False

                else:

                    fields = (line.split(' ', 1))
                    fields = [float(x) for x in fields]
                    scan['peak_list'].append(fields)

        scan['peak_count'] = len(scan['peak_list'])
        scan['total_ion_current'] = self.scan_total_ion_current(
            scan['peak_list'])

        return scan

    def read_scans(self):
        """
        Reads and parses all scan in the MGF file.
        :return: Generator that yield a scan dictionary
        """

        with open(self.file_name, 'r') as self.fh:

            scan = ''

            while scan is not None:

                scan = self.parse_next_scan()

                if scan is not None:
                    yield scan

    def get_scan_at_index(self, index):
        """
        Extracts a single scan from the MGF by its file index.
        :param index: Integer that point to a single scan in the MGF file.
        :return: Scan dictionary
        """

        with open(self.file_name, 'r') as self.fh:
            self.fh.seek(index)

            return self.parse_next_scan()

    def get_scans_dict(self, remove_fields=[], key='TITLE'):
        """
        Returns dictionary with all scans.
        :param remove_fields: String of the field to remove.
        :return: Dictionary
        """

        mgf_scans = dict()

        #count = 0

        for scan in self.read_scans():

            for field in remove_fields:
                scan.pop(field, None)

            if key == 'TITLE':
                mgf_scans[scan['TITLE']] = scan

            elif key == 'scan_number':

                m = re.search(r'scan=(?P<scan_number>\d+)', scan['TITLE'])

                if m is not None:

                    values = m.groupdict()
                    mgf_scans[int(values['scan_number'])] = scan
            #         print('1', scan)
            #         fh = self.fh
            #         scan2 = self.get_scan_at_index(scan['scan_file_index'])
            #         print('2', scan2)
            #
            #         self.fh = fh
            #
            #
            #
            # count += 1
            #
            # if count == 3:
            #     break

        return mgf_scans
