# # Copyright 2013-2017 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 

# Import standard libraries
import csv
import re

# Import Django related libraries

# Import project libraries
from ..models import CLPeptide
from ..models import RawDataset
from .exception import InvalidFileFormatException
from .sequences import sequencesMatcher


class XiParser:
    """
    Parser for Xi results.
    """

    @staticmethod
    def parseResults(dataset):
        """
        This method parses Xi results file provided by the dataset.
        Success or failure of this procedure is noted in the dataset object.
        """

        # Prepare sequenceMatcher
        sM = sequencesMatcher(dataset.fasta_db.pk)

        try:
            with open(dataset.file.path, 'r') as f:

                # Skip line to header
                filters_string = f.readline()
                f.readline()

                # Create CSV reader
                reader = csv.DictReader(f)

                # Check header to validate file format
                header_template = ('"run_name","scan_number","precursor_mz",'
                                   '"precursor_charge","precursor_intensity",'
                                   '"rank","match_score","spectrum_intensity_coverage",'
                                   '"total_fragment_matches","delta","error",'
                                   '"peptide1","display_protein1","peptide_position1",'
                                   '"pep1_link_pos","peptide2","display_protein2",'
                                   '"peptide_position2","pep2_link_pos",'
                                   '"autovalidated","validated","rejected",'
                                   '"notes",')

                header_file = '"' + '","'.join(name for name in reader.fieldnames) + '",'

                if header_template != header_file:
                    raise InvalidFileFormatException(
                        'Uploaded extra file is not recognized as a '
                        'Xi results file (bad file header)')

                # Iterate through lines and create CLPeptide    
                for row in reader:

                    # Create the CLPeptide object
                    row['total_fragment_matches'] = int(float(row['total_fragment_matches']))
                    clpep = CLPeptide(**row)

                    # Add manual fields
                    clpep.peptide_wo_mod1 = re.sub("[a-z0-9_]", "", clpep.peptide1)
                    clpep.peptide_wo_mod2 = re.sub("[a-z0-9_]", "", clpep.peptide2)

                    if clpep.pep1_link_pos != '':
                        clpep.pep1_link_pos = int(clpep.pep1_link_pos) + 1
                    else:
                        clpep.pep1_link_pos = -1

                    if clpep.pep2_link_pos != '':
                        clpep.pep2_link_pos = int(clpep.pep2_link_pos) + 1
                    else:
                        clpep.pep2_link_pos = -1

                    if clpep.peptide_position1 != '':
                        clpep.peptide_position1 = int(clpep.peptide_position1) + 1
                    else:
                        clpep.peptide_position1 = -1

                    if clpep.peptide_position2 != '':
                        clpep.peptide_position2 = int(clpep.peptide_position2) + 1
                    else:
                        clpep.peptide_position2 = -1
                        clpep.peptide2 = '-'
                        clpep.peptide_wo_mod2 = '-'

                    clpep.guessLinkType()
                    clpep.fixAutovalidated()
                    clpep.isDecoy()

                    # Fix very large delta
                    if float(clpep.delta) > 100000:
                        clpep.delta = 100000

                    # Match protein sequences
                    clpep.fs_prot1_id = sM.sequencePk(clpep.display_protein1)
                    clpep.fs_prot2_id = sM.sequencePk(clpep.display_protein2)

                    # Save object to db
                    clpep.save()

                    # Link object to dataset
                    clpep.dataset.add(dataset)

                # Append filter string to dataset description
                RawDataset.objects.filter(pk=dataset.id).update(
                    description=dataset.description + filters_string,
                    parsing_log='Ok',
                    parsing_status=True
                )



        except InvalidFileFormatException as e:
            RawDataset.objects.filter(pk=dataset.id).update(
                parsing_status=False, parsing_log='Error: ' + e.value)
