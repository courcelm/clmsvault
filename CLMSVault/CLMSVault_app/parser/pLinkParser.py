# # Copyright 2013-2017 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 

# Import standard libraries
import csv
import re

# Import Django related libraries

# Import project libraries
from ..models import CLPeptide
from ..models import RawDataset
from .exception import InvalidFileFormatException
from .sequences import sequencesMatcher


class pLinkParser:
    """
    Parser for pLink results.
    """

    @staticmethod
    def formatPeptideSequence(peptide):
        """
        This method transform peptide modification from pLink format to Xi 
        format. 
        """

        # Convert the fixed modification of cysteine to Xi format
        peptide = re.sub('C', 'Cm', peptide)

        return peptide

    @staticmethod
    def parseResults(dataset):
        """
        This method parses pLink results file provided by the dataset.
        Sucess or failure of this procedure is noted in the dataset object.
        """

        # Prepare sequenceMatcher
        sM = sequencesMatcher(dataset.fasta_db.pk)

        try:
            keepPSM = dict()

            if dataset.extra_file:

                with open(dataset.extra_file.path, 'r') as f:

                    # Create CSV reader
                    reader = csv.DictReader(f, delimiter='\t')

                    # Check header to validate file format
                    header_template = ('#\tSpectrum\tPeptideNum\tUniquePepNum\t'
                                       'Samples\tScore\tCondition')
                    header_file = '\t'.join(name for name in reader.fieldnames)

                    if header_template != header_file:
                        raise InvalidFileFormatException(
                            'Uploaded extra file is not recognized as a '
                            'pLink results file (bad file header)')

                    # Iterate through lines and create clPeptide
                    reader.next()
                    for row in reader:
                        keepPSM[row['Spectrum']] = 1
                        # print row
                        reader.next()

            with open(dataset.file.path, 'rb') as f:

                # Check header to validate file format
                header_template = '[Meta]'
                header_file = f.readline().rstrip()

                if header_template != header_file:
                    raise InvalidFileFormatException(
                        'Uploaded extra file is not recognized as a '
                        'pLink results file (bad file header)')

                # Skip to the spectrum section
                for row in f:
                    if row.startswith('[Spectrum'):
                        break

                # Iterate through lines and create CLPeptide    
                tmp_fields = dict()

                for row in f:

                    # Data has been gathered prepare object
                    if row.startswith('[Spectrum'):

                        if 'NO1_Beta_SQ' in tmp_fields and (
                            (tmp_fields['Input'] in keepPSM) == bool(dataset.extra_file)):

                            # Format each field to the right format for the data model
                            fields = dict()
                            (fields['run_name'], fields['scan_number'], dummy) = tmp_fields['Input'].split('.', 2)
                            fields['run_name'] = re.sub('RT\d+:\d+_', '', fields['run_name'])
                            fields['precursor_mz'] = tmp_fields['MZ']
                            fields['precursor_charge'] = tmp_fields['Charge']
                            fields['precursor_intensity'] = -1
                            fields['rank'] = 1
                            fields['match_score'] = tmp_fields['NO1_Score']
                            fields['spectrum_intensity_coverage'] = float(tmp_fields['NO1_MatchedIntensity']) / (
                            float(tmp_fields['NO1_MatchedIntensity']) + float(tmp_fields['NO1_UnMatchedIntensity']))
                            fields['total_fragment_matches'] = -1
                            fields['delta'] = 0

                            if 'NO2_Score' in tmp_fields:
                                fields['delta'] = float(tmp_fields['NO1_Score']) - float(tmp_fields['NO2_Score'])

                            fields['error'] = (float(tmp_fields['MH']) - float(tmp_fields['NO1_Mass'])) / float(
                                tmp_fields['MH']) * 10 ** 6
                            fields['peptide1'] = pLinkParser.formatPeptideSequence(tmp_fields['NO1_Alpha_SQ'])
                            fields['peptide_wo_mod1'] = tmp_fields['NO1_Alpha_SQ']
                            fields['display_protein1'] = re.sub('^\d+,', '', tmp_fields['NO1_Alpha_Proteins'])
                            fields['peptide_position1'] = int(
                                re.sub('^\d+,', '', tmp_fields['NO1_Alpha_ProteinSites'])) + 1
                            fields['pep1_link_pos'] = int(tmp_fields['NO1_XLink_Pos1']) + 1

                            fields['peptide2'] = pLinkParser.formatPeptideSequence(tmp_fields['NO1_Beta_SQ'])
                            fields['peptide_wo_mod2'] = tmp_fields['NO1_Beta_SQ']
                            fields['display_protein2'] = re.sub('^\d+,', '', tmp_fields['NO1_Beta_Proteins'])
                            fields['peptide_position2'] = int(
                                re.sub('^\d+,', '', tmp_fields['NO1_Beta_ProteinSites'])) + 1
                            fields['pep2_link_pos'] = int(tmp_fields['NO1_XLink_Pos2']) + 1

                            fields['autovalidated'] = False
                            fields['validated'] = ''
                            fields['rejected'] = False
                            fields['notes'] = ''
                            fields['not_decoy'] = True

                            if fields['display_protein1'].startswith('REVERSE') or fields[
                                'display_protein2'].startswith('REVERSE'):
                                fields['not_decoy'] = False

                            # print fields

                            # Create the CLPeptide object
                            clpep = CLPeptide(**fields)
                            clpep.guessLinkType()

                            # Match protein sequences
                            clpep.fs_prot1_id = sM.sequencePk(clpep.display_protein1)
                            clpep.fs_prot2_id = sM.sequencePk(clpep.display_protein2)

                            # Save object to db
                            clpep.save()

                            # Link object to dataset
                            clpep.dataset.add(dataset)

                        # Flush tmp fields
                        tmp_fields = dict()


                    # Collect field values
                    else:
                        row = row.rstrip()

                        if not row.startswith('['):
                            (field, value) = row.split('=')
                            tmp_fields[field] = value

                # Append filter string to dataset description
                RawDataset.objects.filter(pk=dataset.id).update(
                    parsing_log='Ok', parsing_status=True)


        except InvalidFileFormatException as e:
            RawDataset.objects.filter(pk=dataset.id).update(
                parsing_status=False, parsing_log='Error: ' + e.value)
