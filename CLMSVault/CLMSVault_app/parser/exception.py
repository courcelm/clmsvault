# Copyright 2013-2017 Mathieu Courcelles
# Mike Tyers's lab / IRIC / Universite de Montreal


# Import standard libraries

# Import Django related libraries

# Import project libraries


class InvalidFileFormatException(Exception):
    """
    This class reports exception during parsing of results file.
    Currently reports problem with header to check if it is really a results file. 
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

