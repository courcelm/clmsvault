# # Copyright 2013-2017 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries
import re

# Import Django related libraries

# Import project libraries
from ..models import CLPeptide
from ..models import RawDataset
from .exception import InvalidFileFormatException
from .sequences import sequencesMatcher


class DXmsms14N15NParser:
    """
    Parser for DXmsms14N15N results.
    """

    @staticmethod
    def parseResults(dataset):
        """
        This method parses DXmsms14N15N results file provided by the dataset.
        Sucess or failure of this procedure is noted in the dataset object.
        """

        # Prepare sequenceMatcher
        sM = sequencesMatcher(dataset.fasta_db.pk)

        # Id to db id

        try:
            with open(dataset.file.path, 'rb') as f:

                # Check header to validate file format
                line = f.readline().rstrip('\n\r')

                # Check header to validate file format
                header_template = 'Program Version:\t14N15N DX MSMS Match ESI Sorting Mass List and MGF 20141007KM'

                if header_template != line:
                    raise InvalidFileFormatException(
                        'Uploaded extra file is not recognized as a '
                        '14N15N DX MSMS Match results file (bad file header)')

                # Get run name from MGF file
                line = f.readline()
                line = f.readline()
                line = f.readline().rstrip('\n\r')

                run_name = ''
                run_name = line.split('\\')[-1]
                run_name = run_name[0:-4]

                # Search for protein sequences until id stats
                protein_counter = 0
                protein_dict = dict()

                while not line.startswith('BEGIN'):

                    line = f.readline().rstrip('\n\r')

                    # Read protein entries
                    if line.startswith('>'):
                        protein_counter += 1
                        protein_dict[protein_counter] = line[1:]

                # Iterate through lines and create clPeptide
                scan_num_dict = dict()
                line = f.readline().rstrip('\n\r')

                while line:

                    values = line.split('\t')
                    if len(values) != 49:
                        raise InvalidFileFormatException(
                            'Uploaded extra file is not recognized as a '
                            '14N15N DX MSMS Match results file (bad number of fields)')

                    # Prepare next line
                    line = f.readline().rstrip('\n\r')

                    # Format each field to the right format for the data model
                    fields = dict()

                    fields['run_name'] = run_name
                    fields['scan_number'] = values[7]

                    # Skip redundant MS/MS entries
                    if scan_num_dict.get(values[7]):
                        continue
                    scan_num_dict[values[7]] = True

                    fields['precursor_mz'] = values[3]
                    fields['precursor_charge'] = values[5]
                    fields['precursor_intensity'] = values[1]
                    fields['rank'] = values[13]

                    if values[26] == '':
                        values[26] = 0

                    if values[31] == '':
                        values[31] = 0

                    fields['spectrum_intensity_coverage'] = float(values[31]) / 100.0
                    fields['total_fragment_matches'] = values[30]

                    fields['match_score'] = float(values[30]) * float(values[31])

                    fields['delta'] = 0
                    fields['error'] = values[15]

                    fields['peptide1'] = values[21]
                    fields['peptide_wo_mod1'] = values[21]
                    fields['pep1_link_pos'] = int(values[19]) - int(values[17]) + 1
                    fields['peptide_position1'] = int(values[17])

                    fields['peptide2'] = values[28]
                    fields['peptide_wo_mod2'] = values[28]
                    fields['pep2_link_pos'] = int(values[26]) - int(values[24]) + 1
                    fields['peptide_position2'] = int(values[24])

                    fields['display_protein1'] = protein_dict[int(values[16])]
                    fields['display_protein2'] = protein_dict[int(values[23])]

                    fields['autovalidated'] = False
                    fields['validated'] = ''
                    fields['rejected'] = False
                    fields['notes'] = ''
                    fields['not_decoy'] = True

                    if (re.match('^R\d+$', fields['display_protein1']) or
                            re.match('^R\d+$', fields['display_protein2'])):
                        fields['not_decoy'] = False

                    # Create the CLPeptide object
                    clpep = CLPeptide(**fields)
                    clpep.guessLinkType()

                    # Match protein sequences
                    clpep.fs_prot1_id = sM.sequencePk(clpep.display_protein1)
                    clpep.fs_prot2_id = sM.sequencePk(clpep.display_protein2)

                    # Save object to db
                    clpep.save()

                    # Link object to dataset
                    clpep.dataset.add(dataset)

                # Append filter string to dataset description
                RawDataset.objects.filter(pk=dataset.id).update(
                    parsing_log='Ok', parsing_status=True)



        except InvalidFileFormatException as e:
            RawDataset.objects.filter(pk=dataset.id).update(
                parsing_status=False, parsing_log='Error: ' + e.value)
