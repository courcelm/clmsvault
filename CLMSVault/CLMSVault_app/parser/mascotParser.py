"""
Copyright 2013-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses
Mike Tyers's lab
Pierre Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries
import csv
import re

# Import Django related libraries

# Import project libraries
from ..models import CLPeptide
from ..models import RawDataset
from .exception import InvalidFileFormatException
from .sequences import sequencesMatcher


class MascotParser:
    """
    Parser for Mascot results.
    """

    @staticmethod
    def formatPeptideSequence(pep_seq, mods, pep_var_mod_pos, cross_linker):
        """
        This method transforms peptide modification fron Mascot format to Xi
        format.        
        """

        peptide = list()
        linker_pos = 0
        pos = 0

        for aa, mod in zip(list(pep_seq), list(pep_var_mod_pos)[2:-2]):

            pos += 1

            if mod == '0':
                peptide.append(aa)

            else:
                mod_name = mods[mod]

                if cross_linker in mod_name:
                    linker_pos = pos

                peptide.append(aa + mod_name.lower())

        # Check N-term
        if pep_var_mod_pos[0] != '0':

            mod_name = mods[pep_var_mod_pos[0]]

            if cross_linker in mod_name:
                linker_pos = 1
                peptide[0] = peptide[0] + mod_name.lower()

        return ''.join(peptide), linker_pos

    @staticmethod
    def parseResults(dataset):
        """
        This method parses Mascot results file.
        Success or failure of this procedure is noted in the data set object.
        """

        # Prepare sequenceMatcher
        sM = sequencesMatcher(dataset.fasta_db.pk)

        try:
            with open(dataset.file.path, 'r') as f:

                # Skip line to variable modification
                line = f.readline()

                # Search for column header and rewind by one line
                while line != '':
                    if line.startswith('"Variable modifications'):
                        break
                    line = f.readline()

                _ = f.readline()
                _ = f.readline()

                mods = dict()

                while True:

                    line = f.readline()

                    fields = line.split(',')

                    if len(fields) >= 2:

                        fields[1] = re.sub('"', '', fields[1])

                        if fields[1] == 'Carbamidomethyl (C)':
                            fields[1] = 'm'
                        elif fields[1] == 'Oxidation (M)':
                            fields[1] = 'ox'
                        elif fields[1] == 'Deamidated (NQ)':
                            fields[1] = 'd'  # Not Xi official short name
                        elif fields[1].startswith('BS3'):
                            fields[1] = 'BS3de'  # Not Xi official short name

                        mods[fields[0]] = fields[1]
                    else:
                        break

                # Skip line to header
                last_pos = f.tell()
                line = f.readline()

                # Search for column header and rewind by one line
                while line != '':
                    if line.startswith('prot_hit_num'):
                        f.seek(last_pos)
                        break
                    last_pos = f.tell()
                    line = f.readline()

                # Create CSV reader
                reader = csv.DictReader(f, delimiter=',')

                # Check header to validate file format
                header_template = ('prot_hit_num,prot_acc,prot_desc,prot_score,'
                                   'prot_mass,prot_matches,prot_matches_sig,'
                                   'prot_sequences,prot_sequences_sig,pep_query,'
                                   'pep_rank,pep_isbold,pep_isunique,pep_exp_mz,'
                                   'pep_exp_mr,pep_exp_z,pep_calc_mr,pep_delta,'
                                   'pep_start,pep_end,'
                                   'pep_miss,pep_score,pep_expect,pep_res_before,'
                                   'pep_seq,pep_res_after,pep_var_mod,pep_var_mod_pos,'
                                   'pep_num_match,pep_scan_title')

                try:
                    header_file = ','.join(name for name in reader.fieldnames)

                except TypeError:
                    raise InvalidFileFormatException(
                        'Uploaded file is not recognized as a '
                        'Mascot results file (bad file header)')

                if header_template != header_file:
                    raise InvalidFileFormatException(
                        'Uploaded file is not recognized as a '
                        'Mascot results file (bad file header)')

                # Iterate through lines and create clPeptide    
                for row in reader:

                    # Keep row with dead-end only
                    if dataset.cross_linker.name not in row['pep_var_mod']:
                        continue

                    # Format each field to the right format for the data model
                    fields = dict()

                    (run_name, scan_number, dump) = row['pep_scan_title'].split('.', 2)
                    fields['run_name'] = re.sub('^.+?_', '', run_name)
                    fields['scan_number'] = scan_number
                    fields['precursor_mz'] = row['pep_exp_mz']
                    fields['precursor_charge'] = row['pep_exp_z']
                    fields['precursor_intensity'] = -1
                    fields['rank'] = row['pep_rank']
                    fields['match_score'] = row['pep_score']
                    fields['spectrum_intensity_coverage'] = -1
                    fields['total_fragment_matches'] = row['pep_num_match']
                    fields['delta'] = -1
                    fields['error'] = float(row['pep_delta']) / float(row['pep_calc_mr']) * 10 ** 6

                    (fields['peptide1'], fields['pep1_link_pos']) = \
                        MascotParser.formatPeptideSequence(row['pep_seq'],
                                                           mods,
                                                           row['pep_var_mod_pos'],
                                                           dataset.cross_linker.name)
                    fields['peptide_wo_mod1'] = row['pep_seq']
                    fields['peptide_position1'] = row['pep_start']

                    fields['peptide2'] = '-'
                    fields['peptide_wo_mod2'] = '-'
                    fields['peptide_position2'] = -1
                    fields['pep2_link_pos'] = -1

                    fields['display_protein1'] = row['prot_acc']
                    fields['display_protein2'] = '-'

                    fields['autovalidated'] = False
                    fields['validated'] = ''
                    fields['rejected'] = False
                    fields['notes'] = ''
                    fields['not_decoy'] = not row['prot_acc'].startswith('r-')

                    # Create the CLPeptide object
                    clpep = CLPeptide(**fields)
                    clpep.guessLinkType()

                    # Match protein sequences
                    clpep.fs_prot1_id = sM.sequencePk(clpep.display_protein1)
                    clpep.fs_prot2_id = sM.sequencePk(clpep.display_protein2)

                    # Save object to db
                    clpep.save()

                    # Link object to data set
                    clpep.dataset.add(dataset)

                # Append filter string to data set description
                RawDataset.objects.filter(pk=dataset.id).update(
                    parsing_log='Ok', parsing_status=True)

        except InvalidFileFormatException as e:
            RawDataset.objects.filter(pk=dataset.id).update(
                parsing_status=False, parsing_log='Error: ' + e.value)
