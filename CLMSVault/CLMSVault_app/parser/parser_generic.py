"""
Copyright 2013-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses
Mike Tyers's lab
Pierre Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries

# Import Django related libraries
from django.db import transaction

# Import project libraries
from .DXmsms14N15N import DXmsms14N15NParser
from .XiParser import XiParser
from .xQuestParser import xQuestParser
from .pLinkParser import pLinkParser
from .FastaParser import FastaParser
from .mascotParser import MascotParser
from .QuantFC_Parser import QuantFC_Parser
from .KojakParser import KojakParser
from .XlinkXParser import XlinkXParser


class parser_generic:
    """
    Parser generic module.
    """

    @staticmethod
    @transaction.atomic
    def parseResults(instance):
        """
        Instantiates the right parser for the supplied dataset.
        """

        dispatch = dict()
        dispatch['14N15N DX MSMS Match'] = DXmsms14N15NParser
        dispatch['Kojak'] = KojakParser
        dispatch['Mascot'] = MascotParser
        dispatch['pLink'] = pLinkParser
        dispatch['Xi'] = XiParser
        dispatch['xQuest'] = xQuestParser
        dispatch['XlinkX'] = XlinkXParser

        dispatch[instance.search_algorithm.name].parseResults(instance)

    @staticmethod
    def parseFASTA(instance):
        FastaParser.parseFASTA(instance)

    @staticmethod
    def parseQuantFC(instance):
        QuantFC_Parser.parseResults(instance)
