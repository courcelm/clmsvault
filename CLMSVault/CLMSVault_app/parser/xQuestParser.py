# # Copyright 2013-2017 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries
import csv
import re

# Import Django related libraries

# Import project libraries
from ..models import CLPeptide
from ..models import RawDataset
from .exception import InvalidFileFormatException
from .sequences import sequencesMatcher


class xQuestParser:
    """
    Parser for xQuest results.
    """

    @staticmethod
    def formatPeptideSequence(peptide):
        """
        This method transforms peptide modification fron xQuest format to Xi
        format.        
        """

        # Remove Z amino acid
        peptide = re.sub('^Z', '', peptide)

        # Convert the fixed modification of cysteine to Xi format
        peptide = re.sub('C', 'Cm', peptide)

        # Convert variable modification of methionine to Xi format
        peptide = re.sub('X', 'Mox', peptide)

        return peptide

    @staticmethod
    def parseResults(dataset):
        """
        This method parses xQuest results file provided by the dataset.
        Sucess or failure of this procedure is noted in the dataset object.
        """

        # Prepare sequenceMatcher
        sM = sequencesMatcher(dataset.fasta_db.pk)

        try:
            with open(dataset.file.path, 'r') as f:

                # Create CSV reader
                reader = csv.DictReader(f, delimiter='\t')

                # Check header to validate file format
                header_template = ('Rank\tId\tProtein1\tProtein2\tType\tXLType'
                                   '\tSpectrum\tAbsPos1\tAbsPos2\tdeltaAA\tAnnotation\tMr\tMz\tz'
                                   '\tError_rel[ppm]\tnseen\tMatchOdds\tXcorrx\tXcorrb\tTIC\tTicA'
                                   '\tTicB\tWTIC\tintsum\tdeltaS\tld-Score\tFDR\tcomment')

                header_file = '\t'.join(name for name in reader.fieldnames)

                if header_template != header_file:
                    raise InvalidFileFormatException(
                        'Uploaded extra file is not recognized as a '
                        'xQuest results file (bad file header)')

                # Iterate through lines and create clPeptide    
                for row in reader:

                    # Format each field to the right format for the data model
                    fields = dict()

                    (run_name, scan_number, dump) = row['Spectrum'].split('.', 2)
                    fields['run_name'] = run_name
                    fields['scan_number'] = re.sub('/^0+/', '', scan_number)
                    fields['precursor_mz'] = row['Mz']
                    fields['precursor_charge'] = re.sub('\.\d+$', '', row['z'])
                    fields['precursor_intensity'] = -1
                    fields['rank'] = row['Rank']
                    fields['match_score'] = row['ld-Score']
                    fields['spectrum_intensity_coverage'] = row['TIC']
                    fields['total_fragment_matches'] = row['nseen']
                    fields['delta'] = row['deltaS']
                    fields['error'] = row['Error_rel[ppm]']

                    if row['Type'] == 'xlink':
                        (peptide1, peptide2, pep1_link_pos, pep2_link_pos) = row['Id'].split('-')
                        fields['peptide1'] = xQuestParser.formatPeptideSequence(peptide1)
                        fields['peptide_wo_mod1'] = re.sub('[a-z]', '', fields['peptide1'])
                        fields['pep1_link_pos'] = int(re.sub('^a', '', pep1_link_pos))
                        fields['peptide_position1'] = int(row['AbsPos1']) - int(fields['pep1_link_pos'])

                        if peptide1.startswith('Z'):
                            fields['pep1_link_pos'] -= 1

                        fields['peptide2'] = xQuestParser.formatPeptideSequence(peptide2)
                        fields['peptide_wo_mod2'] = re.sub('[a-z]', '', fields['peptide2'])
                        fields['pep2_link_pos'] = int(re.sub('^b', '', pep2_link_pos))
                        fields['peptide_position2'] = int(row['AbsPos2']) - int(fields['pep2_link_pos'])

                        if peptide2.startswith('Z'):
                            fields['pep2_link_pos'] -= 1

                    elif row['Type'] == 'monolink':
                        (peptide1, pep1_link_pos, mod_mass) = row['Id'].split('-')
                        fields['peptide1'] = xQuestParser.formatPeptideSequence(peptide1)
                        fields['peptide_wo_mod1'] = re.sub('[a-z]', '', fields['peptide1'])
                        fields['pep1_link_pos'] = int(re.sub('^\w', '', pep1_link_pos))
                        fields['peptide_position1'] = int(row['AbsPos1']) - int(fields['pep1_link_pos'])

                        if peptide1.startswith('Z'):
                            fields['pep1_link_pos'] -= 1

                        fields['peptide2'] = '-'
                        fields['peptide_wo_mod2'] = '-'
                        fields['peptide_position2'] = -1
                        fields['pep2_link_pos'] = -1

                    elif row['Type'] == 'intralink':
                        (peptide1, pep1_link_pos, pep2_link_pos) = row['Id'].split('-')
                        fields['peptide1'] = xQuestParser.formatPeptideSequence(peptide1)
                        fields['peptide_wo_mod1'] = re.sub('[a-z]', '', fields['peptide1'])
                        fields['pep1_link_pos'] = int(re.sub('^\w', '', pep1_link_pos))
                        fields['peptide_position1'] = int(row['AbsPos1']) - int(fields['pep1_link_pos'])

                        if peptide1.startswith('Z'):
                            fields['pep1_link_pos'] -= 1

                        fields['peptide2'] = '-'
                        fields['peptide_wo_mod2'] = '-'
                        fields['peptide_position2'] = -1
                        fields['pep2_link_pos'] = int(re.sub('^\w', '', pep2_link_pos))

                    fields['display_protein1'] = row['Protein1']
                    fields['display_protein2'] = row['Protein2']

                    fields['autovalidated'] = False
                    fields['validated'] = ''
                    fields['rejected'] = False
                    fields['notes'] = row['comment']
                    fields['not_decoy'] = not row['XLType'].startswith('decoy')

                    # Create the CLPeptide object
                    clpep = CLPeptide(**fields)
                    clpep.guessLinkType()

                    # Match protein sequences
                    clpep.fs_prot1_id = sM.sequencePk(clpep.display_protein1)
                    clpep.fs_prot2_id = sM.sequencePk(clpep.display_protein2)

                    # Save object to db
                    clpep.save()

                    # Link object to dataset
                    clpep.dataset.add(dataset)

                # Append filter string to dataset description
                RawDataset.objects.filter(pk=dataset.id).update(
                    parsing_log='Ok', parsing_status=True)



        except InvalidFileFormatException as e:
            RawDataset.objects.filter(pk=dataset.id).update(
                parsing_status=False, parsing_log='Error: ' + e.value)
