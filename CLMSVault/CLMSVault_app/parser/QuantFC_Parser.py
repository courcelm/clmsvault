# # Copyright 2013-2017 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries
import csv

# Import Django related libraries

# Import project libraries
from ..models import (CLPeptide,
                      Quantification,
                      QuantificationFC
                      )
from .exception import InvalidFileFormatException


class QuantFC_Parser:
    """
    Parser for Quantification fold change results.
    """

    @staticmethod
    def parseResults(quantification):
        """
        This method parses quantification file provided by the quantification object.
        Success or failure of this procedure is noted in the quantification object.
        """

        try:
            with open(quantification.file.path, 'r') as f:

                # Create CSV reader
                reader = csv.DictReader(f)

                # Check header to validate file format
                header_template = ''

                if quantification.file_header == 'CF':
                    header_template = ('"CLPeptideId","FoldChange"')
                else:
                    header_template = ('"File","ScanNumber","FoldChange"')

                header_file = '"' + '","'.join(name for name in reader.fieldnames) + '"'

                if not header_file.startswith(header_template):
                    raise InvalidFileFormatException(header_file +
                                                     'Uploaded extra file is not recognized as a '
                                                     'Quantification fold change results file (bad file header)')

                id_no_match = ''
                # Iterate through lines and create QuantificationFC
                if quantification.file_header == 'CF':
                    for row in reader:

                        # Split CLPeptideId
                        clpeptides_id = row['CLPeptideId'].split('-')

                        for clpep_id in clpeptides_id:

                            # Match clpep_id to database
                            queryset = CLPeptide.objects.filter(pk=clpep_id)

                            if len(queryset):

                                # Create the QuantificationFC object
                                quantificationFC = QuantificationFC()
                                quantificationFC.quantification = quantification
                                quantificationFC.clpeptide = queryset[0]
                                quantificationFC.fold_change = row['FoldChange']

                                # Save object to db
                                quantificationFC.save()
                            else:
                                id_no_match += clpep_id + '-'


                elif quantification.file_header == 'FSF':
                    for row in reader:

                        # Match clpep_id to database
                        queryset = CLPeptide.objects.filter(run_name=row['File'],
                                                            scan_number=row['ScanNumber'])

                        if len(queryset):

                            # Create the QuantificationFC object
                            quantificationFC = QuantificationFC()
                            quantificationFC.quantification = quantification
                            quantificationFC.clpeptide = queryset[0]
                            quantificationFC.fold_change = row['FoldChange']

                            # Save object to db
                            quantificationFC.save()
                        else:
                            id_no_match += row['File'] + ':' + row['ScanNumber'] + '-'

                if id_no_match == '':
                    id_no_match = 'Ok'

                # Append filter string to dataset description
                Quantification.objects.filter(pk=quantification.id).update(
                    parsing_log=id_no_match,
                    parsing_status=True
                )



        except InvalidFileFormatException as e:
            Quantification.objects.filter(pk=quantification.id).update(
                parsing_status=False, parsing_log='Error: ' + e.value)
