# Copyright 2013-2017 Mathieu Courcelles
# Mike Tyers's lab / IRIC / Universite de Montreal


# Import standard libraries
import logging
import os
from .parser.exception import InvalidFileFormatException
import pickle
import random
from unipath import Path
import urllib.request

# Import Django related libraries
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

# Import BioPython libraries
from Bio import pairwise2
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.Polypeptide import three_to_one

# Import project libraries
from .models import PDB
from .queryset_operation import clpeptide_set_2_protein_sequences


# Code from http://stackoverflow.com/questions/4529815/how-to-save-an-object-in-python
def save_object(obj, filename):
    with open(filename, 'wb') as output:
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)


def findPeptidePositionsInAligment(pep_seq, fasta_seq_aligned,
                                   pdb_seq_aligned, link_index, peptide_identity):
    """
    Searches for a peptide sequence in fasta protein sequence
    and returns un-gapped sequence in the aligned pdb sequence.
    """

    # Input validation
    if (len(pep_seq) == 0 or
                len(fasta_seq_aligned) == 0 or
                len(pep_seq) > len(fasta_seq_aligned) or
                link_index > len(pep_seq) or link_index < 1):
        return []

    # Initialisation for search
    hit_positions = []

    prot_start_cursor = 0
    prot_cursor = 0;
    pep_cursor = 0;

    link_index -= 1
    prot_link_index = 0

    # Screen the sequence for the peptide sequence
    while prot_cursor < len(fasta_seq_aligned):

        if pep_seq[pep_cursor] == fasta_seq_aligned[prot_cursor]:

            # Keep start position of the peptide
            if pep_cursor == 0:
                prot_start_cursor = prot_cursor

                # Keep position of the linked position
            if pep_cursor == link_index:
                prot_link_index = prot_cursor

            pep_cursor += 1

            # Check if we reached the end of the peptide
            if pep_cursor == len(pep_seq):

                pdb_pep_seq = list(pdb_seq_aligned[prot_start_cursor:prot_cursor + 1])
                index = prot_link_index - prot_start_cursor

                # Check sequence identity with PDB and discard it if too low
                count = 0.0
                ident_count = 0.0

                for (a, b) in zip(fasta_seq_aligned[prot_start_cursor:prot_cursor + 1],
                                  pdb_seq_aligned[prot_start_cursor:prot_cursor + 1]):
                    count += 1

                    if a == b:
                        ident_count += 1

                # Discard if sequence identity is too low
                identity = ident_count / count

                if identity >= peptide_identity:

                    # Assign cross-linked position by lower case
                    pdb_pep_seq[index] = pdb_pep_seq[index].lower()

                    #                     while (len(pdb_pep_seq) != 0 and
                    #                            pdb_pep_seq[index] == '-'):
                    #                         del(pdb_pep_seq[index])
                    #
                    #                         if index == len(pdb_pep_seq):
                    #                             index -= 1
                    #                     else:
                    #                         if len(pdb_pep_seq) != 0:
                    #                             pdb_pep_seq[index] = pdb_pep_seq[index].lower()




                    # Remove gaps in the aligned PDB peptide sequence
                    pdb_pep_seq = [x for x in pdb_pep_seq if x != '-']

                    # Keep hit if remaining peptide length above this threshold
                    if len(pdb_pep_seq) > 3:
                        hit = dict()
                        hit['start'] = prot_start_cursor
                        hit['stop'] = prot_cursor
                        hit['fasta_pep_seq'] = fasta_seq_aligned[prot_start_cursor:prot_cursor + 1]
                        hit['pdb_pep_seq'] = pdb_seq_aligned[prot_start_cursor:prot_cursor + 1]
                        hit['final_pep_seq'] = pdb_pep_seq
                        hit['identity'] = identity

                        hit_positions.append(hit)

                # Reset position for next search
                pep_cursor = 0
                prot_cursor = prot_start_cursor


        # Aligment gap to skip
        elif fasta_seq_aligned[prot_cursor] == '-':
            pass

        # Mismatch before the end of the peptide
        else:

            if pep_cursor != 0:
                prot_cursor = prot_start_cursor

            pep_cursor = 0

        prot_cursor += 1

    return hit_positions


def crosslinkResidues(pep_seq, cl_position, alignments, pdb, peptide_identity):
    """
    Searches for a peptide sequence in alignment and then
    return cross-linked position in PDB.
    """

    linked_residues = []

    # Locate peptide in sequence alignments
    for alignment in alignments:

        (pdb_seq, chain) = pdb.pdb_sequences[alignment['chain']]
        fasta_seq_aligned = alignment['fasta_seq_aligned']
        pdb_seq_aligned = alignment['pdb_seq_aligned']

        positions = findPeptidePositionsInAligment(pep_seq,
                                                   fasta_seq_aligned,
                                                   pdb_seq_aligned,
                                                   cl_position, peptide_identity)

        # Locate cross-linked residue in PDB chain
        for position in positions:

            pdb_pep_seq_start = 0
            pdb_pep_seq_linked = -1
            pdb_seq_index = 0
            pep_index = 0
            chain_list = list(chain)

            # Iterate each chain residue
            while pdb_seq_index < len(chain_list):
                residue = chain_list[pdb_seq_index]

                residue_name = residue.get_resname()

                try:
                    pdb_aa = three_to_one(residue_name)

                    # Check for linked residue
                    pep_aa = position['final_pep_seq'][pep_index].upper();

                    if pep_aa != position['final_pep_seq'][pep_index]:
                        pdb_pep_seq_linked = pdb_seq_index

                    if pdb_aa == pep_aa:

                        if pep_index == 0:
                            pdb_pep_seq_start = pdb_seq_index

                        pep_index += 1

                        # Complete peptide sequence found
                        if pep_index == len(position['final_pep_seq']):

                            if pdb_pep_seq_linked != -1:
                                linked_residue = chain_list[pdb_pep_seq_linked]
                                linked_residues.append((linked_residue, position['identity']))
                                pdb_pep_seq_linked = -1

                            # Reset to see if peptide is present latter in sequence
                            pep_index = 0
                            pdb_seq_index = pdb_pep_seq_start

                    else:
                        if pep_index != 0:
                            pdb_seq_index = pdb_pep_seq_start
                        pep_index = 0

                except KeyError:
                    pass

                pdb_seq_index += 1

    return linked_residues


def retrievePDBfile(pdb_identifier):
    """
    Returns PDB object from RCSB PDB server or local drive.
    """

    # Check if PDB is available locally
    pdb_queryset = PDB.objects.filter(identifier=pdb_identifier)

    # PDB is not available
    if pdb_queryset.count() == 0:

        # Create a new entry in database
        pdb = PDB()
        pdb.identifier = pdb_identifier
        pdb.save()

        # Download PDB file and store it to local drive for future access.
        # From http://chris.improbable.org/2010/01/26/django-reliably-saving-urls-file-field/

        url = ('http://www.rcsb.org/pdb/download/downloadFile.do?fileFormat=pdb&compression=NO&structureId='
               + pdb_identifier)
        file_temp = NamedTemporaryFile()
        file_temp.write(urllib.request.urlopen(url).read())
        file_temp.flush()

        pdb.file.save(pdb_identifier + '.pdb', File(file_temp))

        # Get PDB title
        parser = PDBParser()
        structure = parser.get_structure('tmp', pdb.file.file.name)
        pdb.title = structure.header['name']
        pdb.save()

        return pdb

    # PDB is available locally
    else:
        return pdb_queryset[0]


def read_PDB_sequences(pdb):
    """
    Opens specified PDB file and returns a dictionary of with key=chain
    and value=protein sequence string. 
    """

    parser = PDBParser()

    try:
        structure = parser.get_structure('tmp', pdb.file.file.name)

    except AssertionError:

        msg = 'Error while parsing PDB file.'

        logger = logging.getLogger('django')
        logger.error(msg, exc_info=True)

        raise InvalidFileFormatException(msg)

    pdb_sequences = dict()

    residues_list = []
    residues_DE_list = []
    residues_K_list = []
    residues_KSTY_list = []

    # Iterate protein chains and residues to construct the sequence
    for chain in structure.get_chains():

        sequence = ''

        for residue in chain:

            # Prepare residue lists
            residues_list.append(residue)

            if (residue.get_resname() == 'ASP' or
                        residue.get_resname() == 'GLU'):
                residues_DE_list.append(residue)

            elif residue.get_resname() == 'LYS':
                residues_K_list.append(residue)
                residues_KSTY_list.append(residue)

            elif (residue.get_resname() == 'SER' or
                          residue.get_resname() == 'THR' or
                          residue.get_resname() == 'TYR'):
                residues_KSTY_list.append(residue)

            # Prepare protein sequence
            if residue.get_resname() == 'SEP':
                sequence += 'S'
            else:
                try:
                    sequence += three_to_one(residue.get_resname())
                except KeyError:
                    pass

        pdb_sequences[str(chain.get_id())] = (sequence, chain)

    pdb.pdb_sequences = pdb_sequences
    pdb.residues_list = residues_list
    pdb.residues_K_list = residues_K_list
    pdb.residues_KSTY_list = residues_KSTY_list


def sequences_alignment(fasta_sequences, pdb, protein_identity):
    """
    Aligns protein sequences from FASTA against the PDB sequences.
    Returns a dictionary:
    Key: fasta_id
    Value: alignment
    """

    alignments_dict = dict()

    for (fasta_id, fasta_seq) in fasta_sequences.items():

        alignments_list = list()

        # Check cache for alignments
        project_dir = Path(__file__).ancestor(4)
        directory = os.path.join(project_dir, 'media_root', 'pdb_alignments')

        if not os.path.exists(directory):
            os.makedirs(directory)

        pkl_file = os.path.join(directory, '%s-%s.pkl' % (fasta_id, pdb.pk))

        if os.path.isfile(pkl_file):
            with open(pkl_file, 'rb') as pickle_fh:
                alignments_list = pickle.load(pickle_fh)

        else:
            # Do the alignments since it is not in the cache

            for (chain_id, (pdb_seq, chain)) in pdb.pdb_sequences.items():

                # Skip empty pdb_sequence
                if pdb_seq == '':
                    continue

                # Do protein sequence alignment with FASTA sequence
                alignment = pairwise2.align.globalms(fasta_seq, pdb_seq, 2,
                                                     -1, -1, -0.5,
                                                     one_alignment_only=1)
                fasta_seq_aligned = str(alignment[0][0])
                pdb_seq_aligned = str(alignment[0][1])

                # Check sequence identity
                min_length = min(len(fasta_seq_aligned.replace('-', '')),
                                 len(pdb_seq_aligned.replace('-', '')))

                match_count = 0.0

                for res1, res2 in zip(alignment[0][0], alignment[0][1]):
                    if res1 == res2 and res1 != '-' and res2 != '-':
                        match_count += 1

                identity = match_count / min_length

                # Store alignment
                alignment_dict = dict()
                alignment_dict['chain'] = chain_id
                alignment_dict['fasta_seq_aligned'] = fasta_seq_aligned
                alignment_dict['pdb_seq_aligned'] = pdb_seq_aligned
                alignment_dict['identity'] = identity
                alignments_list.append(alignment_dict)

            save_object(alignments_list, pkl_file)

        # Keep alignment based on user protein identity threshold
        alignments_dict[fasta_id] = [x for x in alignments_list
                                     if x['identity'] >= protein_identity]

    return alignments_dict


class CLDistance(object):
    """
    Object that will keep the cros-link distance and the two residues.
    """

    def __init__(self, distance, residue_1, residue_2):

        if distance != '':
            self.distance = '%.2f' % distance
        else:
            self.distance = distance

        self.residue_1 = residue_1
        self.residue_1_str = CLDistance.residueString(self.residue_1)
        self.residue_2 = residue_2
        self.residue_2_str = CLDistance.residueString(self.residue_2)

    def cldistance_key(self):
        """
        Returns a string that represent a unique key.
        """
        return '-'.join(sorted([self.residue_1_str, self.residue_2_str]))

    @staticmethod
    def residueString(residue):
        """
        Returns a string representation of the residue.
        """

        if residue == None:
            return ''
        else:
            return '[%s]%s:%s.CA' % (residue.get_resname(),
                                     residue.get_id()[1],
                                     residue.get_parent().get_id(),
                                     )

    def __repr__(self):
        """
        Returns a string representation of the cross-link distance.
        """
        return '%s - %s - %s' % (self.residue_1_str,
                                 self.distance,
                                 self.residue_2_str,
                                 )


def compute_cl_distance(clpeptide_set, pdb, protein_identity, peptide_identity, unique_key=None):
    """
    Measure distance of cross-linked residues and add it to clpep object.
    """

    # Remove decoy peptides
    clpeptide_set = clpeptide_set.filter(not_decoy=True)

    # Get original FASTA sequence for cross-links detected
    fasta_sequences = clpeptide_set_2_protein_sequences(clpeptide_set)

    # Read PDB file to get chain and protein sequences
    read_PDB_sequences(pdb)

    # Do sequence alignments
    alignments = sequences_alignment(fasta_sequences, pdb, protein_identity)

    # Iterate cross-linked peptide to get position in PDB
    clpeptide_set_return = []

    used_key = {}

    for clpep in clpeptide_set:

        linked_residues_1 = []

        if clpep.fs_prot1_id is not None:
            linked_residues_1 = crosslinkResidues(clpep.peptide_wo_mod1,
                                                  clpep.pep1_link_pos,
                                                  alignments[clpep.fs_prot1_id.id], pdb, peptide_identity)

        linked_residues_2 = []

        if clpep.fs_prot2_id is not None:
            linked_residues_2 = crosslinkResidues(clpep.peptide_wo_mod2,
                                                  clpep.pep2_link_pos,
                                                  alignments[clpep.fs_prot2_id.id], pdb, peptide_identity)

        # Generates pair of residues and calculate distance
        distance_set = set()
        clpep.distances = []

        # Cross-link mapped with both residues
        for residue_1, identity_1 in linked_residues_1:
            for residue_2, identity_2 in linked_residues_2:

                # Get CA atoms for both residues
                ca1 = residue_1['CA']
                ca2 = residue_2['CA']

                # Simply subtract the atoms to get their distance
                distance = ca1 - ca2

                cldistance = CLDistance(distance, residue_1, residue_2)
                cldistance.identity_1 = '%.2f' % identity_1
                cldistance.identity_2 = '%.2f' % identity_2

                key = cldistance.cldistance_key()

                if key not in distance_set:
                    clpep.distances.append(cldistance)
                    distance_set.add(key)

        # Cross-link mapped with residue 1 only
        if len(linked_residues_2) == 0:

            for residue_1, identity_1 in linked_residues_1:

                cldistance = CLDistance('', residue_1, None)
                cldistance.identity_1 = '%.2f' % identity_1
                cldistance.identity_2 = ''

                key = cldistance.cldistance_key()

                if key not in distance_set:
                    clpep.distances.append(cldistance)
                    distance_set.add(key)

        # Cross-link mapped with residue 2 only           
        if len(linked_residues_1) == 0:

            for residue_2, identity_2 in linked_residues_2:

                cldistance = CLDistance('', None, residue_2)
                cldistance.identity_1 = ''
                cldistance.identity_2 = '%.2f' % identity_2

                key = cldistance.cldistance_key()

                if key not in distance_set:
                    clpep.distances.append(cldistance)
                    distance_set.add(key)

        # Cross-link mapped with no residue                    
        if len(linked_residues_1) == 0 and len(linked_residues_2) == 0:
            cldistance = CLDistance('', None, None)
            cldistance.identity_1 = ''
            cldistance.identity_2 = ''

            key = cldistance.cldistance_key()

            if key not in distance_set:
                clpep.distances.append(cldistance)
                distance_set.add(key)

        # Sort distance
        clpep.distances = sorted(clpep.distances, key=lambda d: float(d.distance) if d.distance != '' else d.distance)
        clpep.min_distance = -1

        if clpep.distances:
            clpep.min_distance = clpep.distances[0].distance

        # Apply uniqueness if needed
        if unique_key is not None:

            key = clpep.uniqueKey(unique_key)

            if key not in used_key:
                clpeptide_set_return.append(clpep)
                used_key[key] = True

        else:
            clpeptide_set_return.append(clpep)

    return (clpeptide_set_return, alignments)


def randomDistance(pdb, random_dist, size):
    """
    Create list of distances with random residues
    """

    random_distance_list = []

    residues_1_list = []
    residues_2_list = []

    if random_dist == 'cresidues':
        residues_1_list = pdb.clink_residues_list
        residues_2_list = pdb.clink_residues_list

    elif random_dist == 'K':
        # Note:
        # Some time K residues are replaced by A and will not be considered
        residues_1_list = pdb.residues_K_list
        residues_2_list = pdb.residues_K_list

    elif random_dist == 'KvsDE':
        residues_1_list = pdb.residues_K_list
        residues_2_list = pdb.residues_DE_list

    elif random_dist == 'KSTY':
        residues_1_list = pdb.residues_KSTY_list
        residues_2_list = pdb.residues_KSTY_list

    elif random_dist == 'KvsALL':
        residues_1_list = pdb.residues_K_list
        residues_2_list = pdb.residues_list

    elif random_dist == 'ALL':
        residues_1_list = pdb.residues_list
        residues_2_list = pdb.residues_list

    residues_1_list = list(residues_1_list)
    residues_2_list = list(residues_2_list)

    # Avoid infinite loop
    if len(residues_1_list) <= 1 or len(residues_2_list) <= 1:
        return random_distance_list

    # Fixed number
    count = 0
    while count != size:

        distance = 0

        residue_1 = random.choice(residues_1_list)
        residue_2 = random.choice(residues_2_list)

        try:
            # Get CA atoms for both residues
            ca1 = residue_1['CA']
            ca2 = residue_2['CA']

            # Simply subtract the atoms to get their distance
            distance = ca1 - ca2

        except KeyError:
            pass

        if distance != 0:
            random_distance_list.append(distance)
            count += 1


            # All residue pair combination
            #     for idx, residue_1 in enumerate(residues_1_list):
            #
            #         for idx2, residue_2 in enumerate(residues_2_list):
            #
            #             if idx2 > idx:
            #
            #                 distance = 0
            #
            #                 try:
            #                     # Get CA atoms for both residues
            #                     ca1 = residue_1['CA']
            #                     ca2 = residue_2['CA']
            #
            #                     # Simply subtract the atoms to get their distance
            #                     distance = ca1 - ca2
            #                 except KeyError:
            #                     pass
            #
            #                 if distance != 0:
            #                     random_distance_list.append(distance)

    return random_distance_list
