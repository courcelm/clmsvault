"""
Copyright 2013-2018 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses
Mike Tyers's lab
Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries
from random import randint
from textwrap import wrap
import urllib.request

# Import Django related libraries
from django import forms
from django.contrib import admin
from django.core.cache import caches
from django.core.paginator import Paginator
from django.db import connection
from django.db.models.signals import post_save
from django.db.utils import OperationalError
from django.dispatch import receiver
from django.forms.utils import ErrorList
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.functional import cached_property

# Import project libraries
from .DatasetProcessing import DatasetProcessing
import CLMSVault.CLMSVault_app.export as export
from .models import (CLPeptide,
                     CLPeptideFilter,
                     CLPeptideFilterParam,
                     CrossLinker,
                     Dataset,
                     FastaDB,
                     FastaDb_Sequence,
                     Instrument,
                     PDB,
                     PeakList,
                     ProcessedDataset,
                     Project,
                     Quantification,
                     RawDataset,
                     SearchAlgorithm,
                     )
from .parser.exception import InvalidFileFormatException

from .parser.mgf_parser import MGFParser

from .parser.parser_generic import parser_generic
from .pdb_structure import (compute_cl_distance,
                            retrievePDBfile
                            )
from .queryset_operation import (dataset_set_2_clpeptide_set,
                                 clpeptide_set_2_protein_set,
                                 clpeptide_set_2_quantificationPkString
                                 )
from .views import (jsmol_view,
                    xiNET_view
                    )


def wrapString(text, length):
    return '<br />'.join(wrap(text, length))


class FasterAdminPaginator(Paginator):
    @cached_property
    def count(self):

        try:
            if not self.object_list.query.where:
                # estimates COUNT: https://djangosnippets.org/snippets/2593/
                cursor = connection.cursor()
                cursor.execute("SELECT TABLE_ROWS FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = %s",
                               [self.object_list.query.model._meta.db_table])
                ret = int(cursor.fetchone()[0])
                return ret

        except OperationalError:
            pass

        try:
            return self.object_list.count()
        except (AttributeError, TypeError):
            # AttributeError if object_list has no count() method.
            # TypeError if object_list.count() requires arguments
            # (i.e. is of type list).
            return len(self.object_list)


class PercolatorForm(forms.Form):
    """
    Generates an intermediate form for Percolator.
    """

    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    q_value = forms.FloatField(label='q-value', initial='0.01',
                               help_text='Cross-linked peptides with q-value lower or equal than the specified value will be retained.')

    CL_CHOICES = (
        ('IntraInter', 'Intra and inter protein cross-links'),
        ('Intra', 'Intra protein cross-links'),
        ('Inter', 'Inter protein cross-links'),
    )

    cl_type = forms.ChoiceField(label='Cross-link type', choices=CL_CHOICES)

    nv = forms.BooleanField(label='Normalize feature vector',
                            required=False)


class quantificationForm(forms.Form):
    def __init__(self, *args, **kwargs):

        super(quantificationForm, self).__init__(*args, **kwargs)

        quant_pk = ''

        if 'initial' in kwargs:
            quant_pk = kwargs['initial']['quant_pk']
            if quant_pk == '':
                quant_pk = [-1]
            else:
                quant_pk = quant_pk.split(',')

            self.fields['quantification'] = forms.ModelChoiceField(
                queryset=Quantification.objects.filter(pk__in=quant_pk),
                required=False)

    COLOR_CHOICES = (
        ('default', 'default'),
        ('score', 'Peptide score'),
        ('quant', 'Peptide fold change'),
        ('validation', 'Validation')
    )

    color_scheme = forms.ChoiceField(choices=COLOR_CHOICES)

    quantification = forms.ModelChoiceField(queryset=Quantification.objects.all(),
                                            required=False)

    fold_change_limit = forms.FloatField(required=False,
                                         help_text='Optional absolute value. Absolute maximum value used otherwise.')

    hide_not_quantified = forms.BooleanField(required=False)

    min_score = forms.FloatField(initial='0',
                                 help_text='Minimum score used for quantification color gradient.')

    max_score = forms.FloatField(initial='15',
                                 help_text='Maximum score used for quantification color gradient.')


class xiNETForm(quantificationForm):
    """
    Generates an intermediate form for xiNET selection.
    """

    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)


class PDBSelectionForm(forms.Form):
    """
    Generates an intermediate form for PDB selection.
    """

    required_css_class = 'required'

    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    pdb_identifier = forms.CharField(required=False,
                                     help_text='Enter PDB identifier or use BLAST feature below to get a suggestion.')

    pdb_select = forms.ModelChoiceField(queryset=PDB.objects.all(),
                                        required=False)

    protein_identity = forms.FloatField(initial='0.7')

    peptide_identity = forms.FloatField(initial='0.7')

    VIEWER_CHOICES = (
        ('HTML5', 'JSMol'),
        ('SIGNED', 'JMol'),
    )

    viewer = forms.ChoiceField(choices=VIEWER_CHOICES, required=False)

    RANDOM_DIST_CHOICES = (
        ('cresidues', 'Cross-linked residues'),
        ('K', 'K - (DSS/BS3)'),
        ('KvsDE', 'K vs DE - (DMTMM)'),
        ('KSTY', 'KSTY - (DSS/BS3)'),
        ('KvsALL', 'K vs ALL - (SDA/LC-SDA)'),
        ('ALL', 'All'),
    )

    cross_linked_residues = forms.ChoiceField(choices=RANDOM_DIST_CHOICES, required=False)

    def clean(self):
        """
        Form cleaning that check if a PDB identifier has been provided or 
        selected and check if the identifier is valid.
        """

        cleaned_data = super(forms.Form, self).clean()
        pdb_identifier = cleaned_data.get('pdb_identifier')
        pdb_select = cleaned_data.get('pdb_select', None)

        # Check that one field has been provided
        if pdb_identifier == '' and pdb_select == None:
            self._errors['pdb_identifier'] = ErrorList(['Provide either a PDB identifier or '
                                                        'select one from PDB select.'])
            self._errors['pdb_select'] = ErrorList(['Provide either a PDB identifier or '
                                                    'select one from PDB select.'])

        # Check for valid PDB identifier
        if pdb_identifier:
            if len(pdb_identifier) != 4:
                self._errors['pdb_identifier'] = ErrorList(['Invalid PDB identifier.'])
            else:

                # Validate URL
                url = ('http://www.rcsb.org/pdb/download/downloadFile.do?fileFormat=pdb&compression=NO&structureId='
                       + pdb_identifier)

                try:
                    connection = urllib.request.urlopen(url)
                    connection.close()

                except urllib.request.HTTPError as e:
                    self._errors['pdb_identifier'] = ErrorList(["PDB identifier doesn't exists."])

        return self.cleaned_data

    def get_PDB(self):
        """
        Returns a PDB object using the PDB identifier.
        """

        pdb_identifier = self.cleaned_data['pdb_identifier']

        # Check if PDB exist
        pdb = None

        if pdb_identifier != '':
            pdb = retrievePDBfile(pdb_identifier)
        else:
            pdb = self.cleaned_data['pdb_select']

        return pdb


class PDBQuantForm(quantificationForm, PDBSelectionForm):
    """
    Mixed PDBselection form and quantification form.
    """

    pass


class CLPeptideAdmin(admin.ModelAdmin):
    """
    Admin panel that displays identified cross-linked peptides and allows
    export to Xi result CSV file format.
    """

    actions = ['make_ProteoProfile_csv',
               'make_Xi_csv',
               'make_Distance_csv',
               'jsmol_view',
               'XiNET_view']

    list_display = ('pk', 'msms', 'run_name_wrapped', 'scan_number',
                    'precursor_mz_2d', 'precursor_charge',
                    'match_score_2d', 'error_2d',
                    'display_protein1', 'peptide1_wrapped',
                    'peptide_position1', 'pep1_link_pos',
                    'display_protein2', 'peptide2_wrapped',
                    'peptide_position2', 'pep2_link_pos',
                    'link_type', 'autovalidated_f', 'not_decoy_f')

    list_filter = ('dataset__project', 'dataset',
                   'link_type', 'cross_link', 'autovalidated',
                   'not_decoy', 'rejected',)

    paginator = FasterAdminPaginator

    raw_id_fields = ('dataset', 'fs_prot1_id', 'fs_prot2_id')

    search_fields = ('run_name', 'scan_number', 'display_protein1',
                     'peptide1', 'peptide_wo_mod1',
                     'display_protein2', 'peptide2', 'peptide_wo_mod2')

    def run_name_wrapped(self, obj):
        return wrapString(obj.run_name, 25)

    run_name_wrapped.allow_tags = True
    run_name_wrapped.short_description = 'run name'
    run_name_wrapped.admin_order_field = 'run_name'

    def peptide1_wrapped(self, obj):
        return wrapString(obj.peptide1, 25)

    peptide1_wrapped.allow_tags = True
    peptide1_wrapped.short_description = 'peptide1'
    peptide1_wrapped.admin_order_field = 'peptide1'

    def peptide2_wrapped(self, obj):
        return wrapString(obj.peptide2, 25)

    peptide2_wrapped.allow_tags = True
    peptide2_wrapped.short_description = 'peptide2'
    peptide2_wrapped.admin_order_field = 'peptide2'

    def autovalidated_f(self, obj):
        """
        Format to a short column name
        """
        return obj.autovalidated

    autovalidated_f.short_description = 'AV'
    autovalidated_f.admin_order_field = 'autovalidated'
    autovalidated_f.boolean = True

    def not_decoy_f(self, obj):
        """
        Format to a short column name
        """
        return obj.not_decoy

    not_decoy_f.short_description = 'ND'
    not_decoy_f.admin_order_field = 'not_decoy'
    not_decoy_f.boolean = True

    def error_2d(self, obj):
        """
        Format the 'error' field with 2 digits.
        """
        return '%.2f' % obj.error

    error_2d.short_description = 'error (ppm)'
    error_2d.admin_order_field = 'error'

    def getDataset(self, obj):
        """
        Return HTML link for all datasets that the peptides belongs.
        """
        return ''.join([dataset.formated_url_short() for dataset in obj.dataset.all()])

    getDataset.allow_tags = True
    getDataset.short_description = 'Datasets'

    def make_Distance_csv(self, request, queryset):
        """
        Action method to export distance of cross-linked peptides to Xi CSV 
        file format.
        """

        form = None
        protein_set = None
        random = None

        clpeptide_set = queryset
        protein_set = clpeptide_set_2_protein_set(clpeptide_set)

        # Read intermediate page
        if 'view' in request.POST:
            form = PDBSelectionForm(request.POST)

            if form.is_valid():
                pdb = form.get_PDB()

                # Restore queryset
                queryset = CLPeptide.objects.all()
                random = request.POST.get('random')
                cache = caches['default']
                queryset.query = cache.get('query_%s' % random)

                clpeptide_set, alignments = compute_cl_distance(queryset,
                                                                pdb,
                                                                form.cleaned_data['protein_identity'],
                                                                form.cleaned_data['peptide_identity'], )

                return export.make_Xi_csv(self, request, clpeptide_set,
                                          'Dataset_CLPeptide',
                                          clpep_distances=True)

        # Send an intermediate page
        if not form:
            # Store queryset query
            random = randint(1, 1000000)
            cache = caches['default']
            cache.set(f'query_{random}', queryset.query, 300)

            selected_action = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)[0]
            form = PDBSelectionForm(initial={'_selected_action': selected_action})

        if random is None:
            random = request.POST.get('random')

        return render(request, 'admin/pdb_selection_form_csv.html',
                      {'queryset': queryset,
                       'form': form,
                       'protein_set': protein_set,
                       'action': 'make_Distance_csv',
                       'random': random})

    make_Distance_csv.short_description = 'Export as Xi CSV with distance'

    def make_Xi_csv(self, request, queryset):
        """
        Action method to export cross-linked peptides to Xi CSV file format.
        """
        return export.make_Xi_csv(self, request, queryset, 'CLPeptide')

    make_Xi_csv.short_description = 'Export as Xi CSV'

    def jsmol_view(self, request, queryset):
        """
        Action method to export cross-linked peptides to JSMol.
        """

        form = None

        clpeptide_set = queryset
        protein_set = clpeptide_set_2_protein_set(clpeptide_set)
        quant_pk = clpeptide_set_2_quantificationPkString(clpeptide_set)

        # Read intermediate page 
        if 'view' in request.POST:
            form = PDBQuantForm(request.POST)

            # Restore queryset
            random = request.POST.get('random')
            queryset = CLPeptide.objects.all()
            cache = caches['default']
            queryset.query = cache.get('query_%s' % random)

            clpeptide_set = queryset
            protein_set = clpeptide_set_2_protein_set(clpeptide_set)
            quant_pk = clpeptide_set_2_quantificationPkString(clpeptide_set)

            if form.is_valid():
                return jsmol_view(request, queryset, form)

        # Send an intermediate page
        if not form:
            # Store queryset query
            random = randint(1, 1000000)
            cache = caches['default']
            cache.set(f'query_{random}', queryset.query, 300)

            selected_action = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)[0]
            form = PDBQuantForm(initial={'quant_pk': quant_pk,
                                         '_selected_action': selected_action})

        return render(request, 'admin/pdb_selection_form.html',
                      {'queryset': queryset,
                       'form': form,
                       'protein_set': protein_set,
                       'action': 'jsmol_view',
                       'random': random})

    jsmol_view.short_description = 'View in JSMol'

    def XiNET_view(self, request, queryset):
        """
        Action method to export cross-linked peptides to XiNET.
        """

        form = None

        # Read intermediate page
        if 'view' in request.POST:
            form = xiNETForm(request.POST)

            if form.is_valid():
                # Restore queryset
                queryset = CLPeptide.objects.all()
                random = request.POST.get('random')
                cache = caches['default']
                queryset.query = cache.get('query_%s' % random)

                return xiNET_view(request, queryset, form)

        # Send an intermediate page
        if not form:
            # Store queryset query
            random = randint(1, 1000000)
            cache = caches['default']
            cache.set(f'query_{random}', queryset.query, 300)

            selected_action = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)[0]

            quant_pk = clpeptide_set_2_quantificationPkString(queryset)

            form = xiNETForm(initial={'quant_pk': quant_pk,
                                      '_selected_action': selected_action,
                                      })

        return render(request, 'admin/xiNET_form.html',
                      {'queryset': queryset,
                       'form': form,
                       'action': 'XiNET_view',
                       'random': random,
                       })

    XiNET_view.short_description = 'View in XiNET'

    def make_ProteoProfile_csv(self, request, queryset):
        """
        Action method to export cross-linked peptides to ProteoProfile CSV file
        format.
        """

        return export.make_ProteoProfile_csv(self, request,
                                             queryset.select_related('fs_prot1_id',
                                                                     'fs_prot2_id'),
                                             'Dataset_CLPeptide')

    make_ProteoProfile_csv.short_description = 'Export as ProteoProfile CSV'

    def match_score_2d(self, obj):
        """
        Format the 'match_score' field with 2 digits.
        """
        return '%.2f' % obj.match_score

    match_score_2d.short_description = 'score'
    match_score_2d.admin_order_field = 'match_score'

    def precursor_mz_2d(self, obj):
        """
        Format the 'precursor_mz' field with 2 digits.
        """
        return '%.2f' % obj.precursor_mz

    precursor_mz_2d.short_description = 'm/z'
    precursor_mz_2d.admin_order_field = 'precursor_mz'

    def spectrum_intensity_coverage_2d(self, obj):
        """
        Format the 'spectrum_intensity_coverage' field with 2 digits.
        """
        return '%.2f' % obj.spectrum_intensity_coverage

    spectrum_intensity_coverage_2d.short_description = 'Int. cov.'
    spectrum_intensity_coverage_2d.admin_order_field = 'spectrum_intensity_coverage'


class CLPeptideFilterAdminInline(admin.TabularInline):
    """
    Inline that allows to select method, field, lookup and value to define
    a filter.
    """

    extra = 2

    model = CLPeptideFilterParam


class CLPeptideFilterAdmin(admin.ModelAdmin):
    """
    Admin panel that allows creation of filters for cross-linked peptides.
    """

    date_hierarchy = 'creation_date'

    inlines = [CLPeptideFilterAdminInline]

    list_display = ('pk', 'name', 'description', 'creation_date')

    save_as = True

    search_fields = ('name', 'description')


class DatasetAdminMixin(object):
    """
    Methods shared by Raw and Processed dataset admin classes.
    """

    actions = ['compareRunIds_csv', 'dataset_stats_csv',
               'make_interaction_matrix',
               'make_Percolator_tsv',
               'make_ProteoProfile_csv',
               'make_xTract_csv',
               'make_xlink_analyzer_csv',
               'make_psi_mi_tab25',
               'make_psi_mi_xml25',
               'make_Xi_csv',
               'make_Distance_csv',
               'run_Percolator',
               'jsmol_view',
               'XiNET_view',
               ]

    date_hierarchy = 'creation_date'

    list_per_page = 25

    # Test code for intermediate page of compareRunIds_csv
    # Modified from http://www.jpichon.net/blog/2010/08/django-admin-actions-and-intermediate-pages/
    class CompareForm(forms.Form):
        """
        Generate an intermediate form for comparison.
        """

        _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

        unique_key = forms.ChoiceField(choices=CLPeptideFilter.UKEY_CHOICES)

        peptide_count = forms.BooleanField(label='Display peptide count',
                                           initial=True,
                                           required=False)

    def compareRunIds_csv(self, request, queryset):
        """
        Compares cross-linked peptides across MS runs and export it to CSV 
        file format.
        """

        form = None

        # Read intermediate page
        if 'compare' in request.POST:
            form = self.CompareForm(request.POST)

            if form.is_valid():
                return export.compareRunIds_csv(self, request, queryset, form,
                                                'CLPeptide_comparison ')

        # Send an intermediate page
        if not form:
            selected_action = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)

            form = self.CompareForm(initial={'_selected_action': selected_action})

        return render(request, 'admin/compare_form.html',
                      {'datasets': queryset,
                       'compare_form': form, })

    compareRunIds_csv.short_description = 'Export cross-linked peptides comparison to CSV'

    def dataset_stats_csv(self, request, queryset):
        """
        Write dataset statistics about cross-linked peptides to CSV file.
        """
        return export.dataset_stats_csv(self, request, queryset,
                                        'Dataset_Statistics')

    dataset_stats_csv.short_description = 'Export dataset statistics to CSV'

    def get_CLPeptides_URL(self, obj):
        """
        Returns HTML link to see cross-linked peptides in the dataset.
        """
        return ('<a href="/admin/CLMSVault_app/clpeptide/?dataset__id__exact=%s">See</a>'
                % (obj.pk))

    get_CLPeptides_URL.allow_tags = True
    get_CLPeptides_URL.short_description = 'CLPeptides'

    def getProject(self, obj):
        """
        Returns HTML link to the associated project.
        """
        return obj.project.formated_url()

    getProject.allow_tags = True
    getProject.short_description = 'Project'
    getProject.admin_order_field = 'project'

    def make_Distance_csv(self, request, queryset):
        """
        Action method to export distance of cross-linked peptides to Xi CSV 
        file format.
        """

        form = None
        protein_set = None

        clpeptide_set = dataset_set_2_clpeptide_set(queryset)
        protein_set = clpeptide_set_2_protein_set(clpeptide_set)

        # Read intermediate page
        if 'view' in request.POST:
            form = PDBSelectionForm(request.POST)

            if form.is_valid():

                pdb = form.get_PDB()

                try:
                    clpeptide_set, alignments = compute_cl_distance(clpeptide_set,
                                                                    pdb,
                                                                    form.cleaned_data['protein_identity'],
                                                                    form.cleaned_data['peptide_identity']
                                                                    )
                except InvalidFileFormatException as e:
                    return HttpResponse('<h1>%s</h1>' % e)

                return export.make_Xi_csv(self, request, clpeptide_set,
                                          'Dataset_CLPeptide',
                                          clpep_distances=True)

        # Send an intermediate page
        if not form:
            selected_action = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
            form = PDBSelectionForm(initial={'_selected_action': selected_action})

        return render(request, 'admin/pdb_selection_form_csv.html',
                      {'queryset': queryset,
                       'form': form,
                       'protein_set': protein_set,
                       'action': 'make_Distance_csv'})

    make_Distance_csv.short_description = 'Export as Xi CSV with distance'

    def make_interaction_matrix(self, request, queryset):
        """
        Action method to export protein interactions matrix 
        CSV file format.
        """
        return export.interaction_matrix(self, request,
                                         dataset_set_2_clpeptide_set(queryset),
                                         'Dataset_CLPeptide')

    make_interaction_matrix.short_description = 'Export as Interaction matrix'

    def make_ProteoProfile_csv(self, request, queryset):
        """
        Action method to export cross-linked peptides to ProteoProfile CSV file format.
        """

        clpeptide_set = dataset_set_2_clpeptide_set(queryset)

        return export.make_ProteoProfile_csv(self, request,
                                             clpeptide_set.select_related('fs_prot1_id',
                                                                          'fs_prot2_id'),
                                             'Dataset_CLPeptide')

    make_ProteoProfile_csv.short_description = 'Export as ProteoProfile CSV'

    def make_xTract_csv(self, request, queryset):
        """
        Action method to export cross-linked peptides to xTract CSV file format.
        """

        clpeptide_set = dataset_set_2_clpeptide_set(queryset)

        return export.make_xTract_csv(self, request,
                                      clpeptide_set.select_related('fs_prot1_id',
                                                                   'fs_prot2_id'),
                                      'Dataset_CLPeptide')

    make_xTract_csv.short_description = 'Export as xTract CSV'

    def make_xlink_analyzer_csv(self, request, queryset):
        """
        Action method to export cross-linked peptides to XlinkAnalyzer CSV file format.
        """

        clpeptide_set = dataset_set_2_clpeptide_set(queryset)

        # Read intermediate page
        if 'view' in request.POST:
            form = PDBSelectionForm(request.POST)

            if form.is_valid():
                return export.make_xlink_analyzer_csv(self, request,
                                                      clpeptide_set.select_related('fs_prot1_id',
                                                                                   'fs_prot2_id'),
                                                      'Dataset_CLPeptide', form)

        # Send an intermediate page
        else:

            selected_action = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
            form = PDBSelectionForm(initial={
                '_selected_action': selected_action})

        return render(request, 'admin/pdb_selection_form_csv.html',
                      {'queryset': queryset,
                       'form': form,
                       'action': 'make_xlink_analyzer_csv'})

    make_xlink_analyzer_csv.short_description = 'Export as XlinkAnalyzer CSV'

    def make_Percolator_tsv(self, request, queryset):
        """
        Action method to export cross-linked peptides to Percolator TSV file format.
        """
        return export.make_Percolator_tsv(self, request,
                                          dataset_set_2_clpeptide_set(queryset),
                                          'Dataset_CLPeptide')

    make_Percolator_tsv.short_description = 'Export as Percolator TSV'

    def run_Percolator(self, request, queryset):
        """
        Action method to run Percolator on cross-linked peptides.
        """

        form = None

        # Read intermediate page
        if 'Process' in request.POST:
            form = PercolatorForm(request.POST)

            if form.is_valid():
                # Restore queryset
                random = request.POST.get('random')
                cache = caches['default']
                queryset = cache.get(f'queryset_{random}')

                return DatasetProcessing.run_Percolator(self, request, queryset, form)

        # Send an intermediate page
        if not form:
            # Store queryset query
            random = randint(1, 1000000)
            cache = caches['default']
            cache.set(f'queryset_{random}', queryset, 300)

            selected_action = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)[0]

            form = PercolatorForm(initial={
                '_selected_action': selected_action,
            })

        return render(request, 'admin/percolator_form.html',
                      {'queryset': queryset,
                       'form': form,
                       'action': 'run_Percolator',
                       'random': random,
                       })

    run_Percolator.short_description = 'Run Percolator'

    def make_Xi_csv(self, request, queryset):
        """
        Action method to export cross-linked peptides to Xi CSV file format.
        """
        return export.make_Xi_csv(self, request,
                                  dataset_set_2_clpeptide_set(queryset),
                                  'Dataset_CLPeptide')

    make_Xi_csv.short_description = 'Export as Xi CSV'

    def make_psi_mi_tab25(self, request, queryset):
        """
        Action method to export cross-linked peptides to PSI-MI TAB 2.5 
        tabular file format.
        """

        return export.psi_mi_tab25(self, request,
                                   dataset_set_2_clpeptide_set(queryset),
                                   'Dataset_CLPeptide')

    make_psi_mi_tab25.short_description = 'Export as PSI-MI TAB 2.5'

    def make_psi_mi_xml25(self, request, queryset):
        """
        Action method to export cross-linked peptides to PSI-MI XML 2.5 
        tabular file format.
        """
        return export.psi_mi_xml25(self, request,
                                   queryset,
                                   'Dataset_CLPeptide')

    make_psi_mi_xml25.short_description = 'Export as PSI-MI XML 2.5'

    def jsmol_view(self, request, queryset):
        """
        Action method to export cross-linked peptides to JSMol.
        """

        form = None
        protein_set = None

        clpeptide_set = dataset_set_2_clpeptide_set(queryset)
        protein_set = clpeptide_set_2_protein_set(clpeptide_set)
        quant_pk = clpeptide_set_2_quantificationPkString(clpeptide_set)

        # Read intermediate page
        if 'view' in request.POST:
            form = PDBQuantForm(request.POST)

            if form.is_valid():
                return jsmol_view(request, clpeptide_set, form)

        # Send an intermediate page
        if not form:
            selected_action = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
            form = PDBQuantForm(initial={'quant_pk': quant_pk,
                                         '_selected_action': selected_action})

        return render(request, 'admin/pdb_selection_form.html',
                      {'queryset': queryset,
                       'form': form,
                       'protein_set': protein_set,
                       'action': 'jsmol_view'})

    jsmol_view.short_description = 'View in JSMol'

    def XiNET_view(self, request, queryset):
        """
        Action method to export cross-linked peptides to XiNET.
        """

        form = None

        # Read intermediate page
        if 'view' in request.POST:
            form = xiNETForm(request.POST)

            if form.is_valid():
                # Restore queryset
                random = request.POST.get('random')
                cache = caches['default']
                queryset = cache.get(f'queryset_{random}')

                return xiNET_view(request, dataset_set_2_clpeptide_set(queryset), form)

        # Send an intermediate page
        if not form:
            # Store queryset query
            random = randint(1, 1000000)
            cache = caches['default']
            cache.set(f'queryset_{random}', queryset, 300)

            selected_action = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)[0]
            quant_pk = clpeptide_set_2_quantificationPkString(dataset_set_2_clpeptide_set(queryset))
            form = xiNETForm(initial={'quant_pk': quant_pk,
                                      '_selected_action': selected_action,
                                      })

        return render(request, 'admin/xiNET_form.html',
                      {'queryset': queryset,
                       'form': form,
                       'action': 'XiNET_view',
                       'random': random,
                       })

    XiNET_view.short_description = 'View in XiNET'


class RawDatasetAdmin(DatasetAdminMixin, admin.ModelAdmin):
    """
    Admin panel to upload cross-links dataset in the pipeline.
    """

    list_display = ('pk', 'name', 'getProject', 'file', 'cross_linker',
                    'fasta_db',
                    'search_algorithm', 'parsing_status',
                    'creation_date', 'get_CLPeptides_URL')

    list_filter = ('project', 'cross_linker', 'instrument_name',
                   'fasta_db', 'search_algorithm')

    readonly_fields = ('parsing_log', 'parsing_status')

    search_fields = ('name', 'file', 'description')

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.prefetch_related('cross_linker', 'fasta_db',
                                             'search_algorithm')
        return queryset

    def get_readonly_fields(self, request, obj=None):
        """
        This method sets some fields read only after dataset creation and
        processing. 
        """

        readonly_fields = list(self.readonly_fields)

        if obj is not None and obj.parsing_status:
            readonly_fields.extend(['file'])
            readonly_fields.extend(['extra_file'])
            readonly_fields.extend(['search_algorithm'])
            readonly_fields.extend(['cross_linker'])

        return readonly_fields

    @staticmethod
    @receiver(post_save, sender=RawDataset)
    def process_file(sender, instance, created, **kwargs):
        """
        This method initiates the parsing of uploaded search results.
        """

        if instance.parsing_status == False and created == False:
            parser_generic.parseResults(instance)


class ProcessedDatasetAdmin(DatasetAdminMixin, admin.ModelAdmin):
    """
    Admin panel to group/filter dataset of cross-linked peptides.
    """

    filter_horizontal = ('datasets',)

    list_display = ('pk', 'name', 'getProject', 'getDataset', 'getFilter',
                    'cross_linker', 'fasta_db',
                    'search_algorithm',
                    'creation_date', 'get_CLPeptides_URL')

    list_filter = ('project', 'clpeptidefilter__name', 'cross_linker',
                   'instrument_name', 'fasta_db', 'search_algorithm')

    raw_id_fields = ('clpeptidefilter',)

    readonly_fields = ('cross_linker', 'instrument_name',
                       'fasta_db', 'search_algorithm')

    search_fields = ('name', 'datasets__name', 'description',
                     'clpeptidefilter__name')

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.prefetch_related('clpeptidefilter',
                                             'cross_linker', 'fasta_db',
                                             'search_algorithm')
        return queryset

    def getDataset(self, obj):
        """
        Returns HTML links to all dataset selected to create this processed 
        dataset.
        """
        return ''.join([dataset.formated_url() for dataset in obj.datasets.all()])

    getDataset.allow_tags = True
    getDataset.short_description = 'Datasets'
    getDataset.admin_order_field = 'datasets'

    def getFilter(self, obj):
        """
        Returns HTML link to the filter applied to create this dataset.
        """
        if obj.clpeptidefilter is None:
            return ''
        else:
            return obj.clpeptidefilter.formated_url()

    getFilter.allow_tags = True
    getFilter.short_description = 'Filter'
    getFilter.admin_order_field = 'clpeptidefilter'

    def get_readonly_fields(self, request, obj=None):
        """
        Sets some fields read only after dataset processing.
        """
        readonly_fields = list(self.readonly_fields)

        if obj is not None and obj.datasets.count() != 0:
            readonly_fields.extend(['datasets'])
            readonly_fields.extend(['clpeptidefilter'])

        return readonly_fields

    @staticmethod
    @receiver(post_save, sender=ProcessedDataset)
    def process_file(sender, instance, created, **kwargs):
        """
        Launches dataset processing.
        """

        if (instance.clpeptide_set.all()[:1].count() == 0 and
                    'Percolator' not in instance.description):
            DatasetProcessing.process(instance)

    def save_model(self, request, obj, form, change):
        # Need to save m2m before launching the processing
        # Advice from http://makkalot-opensource.blogspot.ca/2009/01/django-admin-manytomany-behaviour.html
        super(ProcessedDatasetAdmin, self).save_model(request, obj, form, change)
        form.save_m2m()
        obj.save()


class FastaDBAdmin(admin.ModelAdmin):
    """
    Admin panel to upload FASTA database.
    """

    date_hierarchy = 'creation_date'

    list_display = ('pk', 'name', 'file', 'sequence_count', 'parsing_status',
                    'get_FastaDb_Sequence_URL', 'creation_date')

    list_filter = ('parsing_status',)

    readonly_fields = ('parsing_log', 'parsing_status', 'sequence_count')

    search_fields = ('name', 'file')

    def get_FastaDb_Sequence_URL(self, obj):
        """
        Returns HTML link to see protein sequences in the database.
        """
        return ('<a href="/admin/CLMSVault_app/fastadb_sequence/?fastadb__id__exact=%s">See</a>'
                % (obj.pk))

    get_FastaDb_Sequence_URL.allow_tags = True
    get_FastaDb_Sequence_URL.short_description = 'Sequences'

    def get_readonly_fields(self, request, obj=None):
        """
        This method sets some fields read only after FASTA file upload.
        """

        readonly_fields = list(self.readonly_fields)

        if obj is not None:
            readonly_fields.extend(['file'])

        return readonly_fields

    @staticmethod
    @receiver(post_save, sender=FastaDB)
    def process_file(sender, instance, created, **kwargs):
        """
        This method initiates the parsing of uploaded FASTA file.
        """

        if instance.parsing_status == False and created == False:
            parser_generic.parseFASTA(instance)

    def save_model(self, request, obj, form, change):

        if obj.update == True:
            obj.refreshParsing()

        obj.update = False
        super(FastaDBAdmin, self).save_model(request, obj, form, change)


class FastaDb_SequenceAdmin(admin.ModelAdmin):
    """
    Admin panel to check uploaded FASTA sequence.
    """

    list_display = ('pk', 'getFastaDB', 'identifier', 'gene_name',
                    'description', 'species')

    list_filter = ('fastadb', 'species')

    search_fields = ('identifier', 'gene_name', 'description', 'species')

    def getFastaDB(self, obj):
        """
        Returns HTML link to the associated FastaDB.
        """
        return obj.fastadb.formated_url()

    getFastaDB.allow_tags = True
    getFastaDB.short_description = 'Fastadb'
    getFastaDB.admin_order_field = 'fastadb'


class PDBAdmin(admin.ModelAdmin):
    """
    Admin panel to check uploaded PDB.
    """

    list_display = ('pk', 'identifier', 'title', 'file',)

    search_fields = ('identifier', 'title', 'file')


class PeakListAdmin(admin.ModelAdmin):
    fields = ('file', 'replace_ms2_ref', 'parsing_log', 'parsing_status')

    list_display = ('pk', 'file', 'replace_ms2_ref', 'parsing_log',
                    'parsing_status')

    readonly_fields = ('parsing_log', 'parsing_status')

    search_fields = ('file',)

    @staticmethod
    @receiver(post_save, sender=PeakList)
    def process_file(sender, instance, created, **kwargs):
        """
        This method initiates the parsing of uploaded MGF file.
        """

        if instance.parsing_log is None and created is False:
            pass
        else:
            return None

        # Read MGF file
        # Extract title, scan number, retention time, peak list position
        mgf_file = instance.file.path

        try:
            mgf_parser = MGFParser(mgf_file)
            mgf_scans = mgf_parser.get_scans_dict(key='scan_number',
                                                  remove_fields=['peak_list'])

        except (AttributeError, KeyError, TypeError, ValueError) as e:

            error = 'Unexpected issue while parsing %s. Please check file format' \
                    ' or if file is corrupted.' % mgf_file

            instance.parsing_log = error
            instance.save()

            return None

        mgf_file = instance.filename.replace('.mgf', '')

        for clpeptide in CLPeptide.objects.filter(run_name=mgf_file):

            if clpeptide.scan_file_index == -1 or instance.replace_ms2_ref:
                index = mgf_scans[clpeptide.scan_number]['scan_file_index']

                clpeptide.scan_file_index = index
                clpeptide.retention_time = mgf_scans[clpeptide.scan_number]['RTINSECONDS']
                clpeptide.save()

        instance.parsing_status = True
        instance.parsing_log = 'OK'
        instance.save()


class QuantificationAdmin(admin.ModelAdmin):
    """
    Admin panel to upload quantification dataset.
    """

    list_display = ('pk', 'name', 'quantification_type', 'file',
                    'parsing_status', 'creation_date')

    list_filter = ('quantification_type', 'parsing_status')

    readonly_fields = ('parsing_log', 'parsing_status')

    search_fields = ('name', 'quantification_type', 'file')

    def get_readonly_fields(self, request, obj=None):
        """
        This method sets some fields read only after file upload.
        """

        readonly_fields = list(self.readonly_fields)

        if obj is not None:
            readonly_fields.extend(['file'])

        return readonly_fields

    @staticmethod
    @receiver(post_save, sender=Quantification)
    def process_file(sender, instance, created, **kwargs):
        """
        This method initiates the parsing of uploaded Quantification file.
        """

        if instance.parsing_status == False and created == False:
            parser_generic.parseQuantFC(instance)


# Register admin panels
admin.site.register(RawDataset, RawDatasetAdmin)
admin.site.register(ProcessedDataset, ProcessedDatasetAdmin)
admin.site.register(CrossLinker)
admin.site.register(FastaDB, FastaDBAdmin)
admin.site.register(FastaDb_Sequence, FastaDb_SequenceAdmin)
admin.site.register(Instrument)
admin.site.register(CLPeptide, CLPeptideAdmin)
admin.site.register(SearchAlgorithm)
admin.site.register(Project)
admin.site.register(CLPeptideFilter, CLPeptideFilterAdmin)
admin.site.register(PDB, PDBAdmin)
admin.site.register(PeakList, PeakListAdmin)
admin.site.register(Quantification, QuantificationAdmin)
# admin.site.register(QuantificationFC)
