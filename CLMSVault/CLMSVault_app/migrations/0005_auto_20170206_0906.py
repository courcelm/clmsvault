# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0004_auto_20150303_1510'),
    ]

    operations = [
        migrations.CreateModel(
            name='PeakList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file_name', models.CharField(max_length=500)),
                ('file', models.FileField(upload_to=b'')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='clpeptide',
            name='scan_file_index',
            field=models.BigIntegerField(default=-1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='clpeptide',
            name='link_type',
            field=models.CharField(max_length=50, choices=[(b'Inter-protein', b'Inter-protein'), (b'Intra-protein', b'Intra-protein'), (b'Intra-peptide', b'Intra-peptide'), (b'Dead-end', b'Dead-end'), (b'Linear peptide', b'Linear peptide')]),
        ),
        migrations.AlterField(
            model_name='clpeptidefilter',
            name='quantification_logic',
            field=models.CharField(blank=True, max_length=1, null=True, choices=[(b'|', b'OR'), (b'&', b'AND')]),
        ),
        migrations.AlterField(
            model_name='clpeptidefilterparam',
            name='field',
            field=models.CharField(max_length=100, choices=[('id', 'id'), (b'run_name', b'run_name'), (b'scan_number', b'scan_number'), (b'precursor_mz', b'precursor_mz'), (b'precursor_charge', b'precursor_charge'), (b'precursor_intensity', b'precursor_intensity'), (b'rank', b'rank'), (b'match_score', b'match_score'), (b'spectrum_intensity_coverage', b'spectrum_intensity_coverage'), (b'total_fragment_matches', b'total_fragment_matches'), (b'delta', b'delta'), (b'error', b'error'), (b'peptide1', b'peptide1'), (b'peptide_wo_mod1', b'peptide_wo_mod1'), (b'display_protein1', b'display_protein1'), (b'fs_prot1_id', b'fs_prot1_id'), (b'peptide_position1', b'peptide_position1'), (b'pep1_link_pos', b'pep1_link_pos'), (b'peptide2', b'peptide2'), (b'peptide_wo_mod2', b'peptide_wo_mod2'), (b'display_protein2', b'display_protein2'), (b'fs_prot2_id', b'fs_prot2_id'), (b'peptide_position2', b'peptide_position2'), (b'pep2_link_pos', b'pep2_link_pos'), (b'autovalidated', b'autovalidated'), (b'validated', b'validated'), (b'rejected', b'rejected'), (b'notes', b'notes'), (b'link_type', b'link_type'), (b'cross_link', b'cross_link'), (b'not_decoy', b'not_decoy'), (b'scan_file_index', b'scan_file_index')]),
        ),
        migrations.AlterField(
            model_name='quantification',
            name='file_header',
            field=models.CharField(max_length=20, null=True, choices=[(b'CF', b'"CLPeptideId","FoldChange"'), (b'FSF', b'"File","ScanNumber","FoldChange"')]),
        ),
    ]
