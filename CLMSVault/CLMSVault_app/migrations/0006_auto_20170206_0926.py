# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0005_auto_20170206_0906'),
    ]

    operations = [
        migrations.AlterField(
            model_name='peaklist',
            name='file_name',
            field=models.CharField(unique=True, max_length=500),
        ),
    ]
