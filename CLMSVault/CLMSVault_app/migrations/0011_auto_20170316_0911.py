# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0010_auto_20170316_0854'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clpeptide',
            name='retention_time',
            field=models.FloatField(default=0),
        ),
    ]
