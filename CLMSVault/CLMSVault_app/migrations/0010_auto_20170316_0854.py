# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import CLMSVault.CLMSVault_app.models


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0009_auto_20170208_1131'),
    ]

    operations = [
        migrations.AddField(
            model_name='clpeptide',
            name='retention_time',
            field=models.BigIntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='clpeptidefilterparam',
            name='field',
            field=models.CharField(max_length=100, choices=[('id', 'id'), (b'run_name', b'run_name'), (b'scan_number', b'scan_number'), (b'precursor_mz', b'precursor_mz'), (b'precursor_charge', b'precursor_charge'), (b'precursor_intensity', b'precursor_intensity'), (b'rank', b'rank'), (b'match_score', b'match_score'), (b'spectrum_intensity_coverage', b'spectrum_intensity_coverage'), (b'total_fragment_matches', b'total_fragment_matches'), (b'delta', b'delta'), (b'error', b'error'), (b'peptide1', b'peptide1'), (b'peptide_wo_mod1', b'peptide_wo_mod1'), (b'display_protein1', b'display_protein1'), (b'fs_prot1_id', b'fs_prot1_id'), (b'peptide_position1', b'peptide_position1'), (b'pep1_link_pos', b'pep1_link_pos'), (b'peptide2', b'peptide2'), (b'peptide_wo_mod2', b'peptide_wo_mod2'), (b'display_protein2', b'display_protein2'), (b'fs_prot2_id', b'fs_prot2_id'), (b'peptide_position2', b'peptide_position2'), (b'pep2_link_pos', b'pep2_link_pos'), (b'autovalidated', b'autovalidated'), (b'validated', b'validated'), (b'rejected', b'rejected'), (b'notes', b'notes'), (b'link_type', b'link_type'), (b'cross_link', b'cross_link'), (b'not_decoy', b'not_decoy'), (b'scan_file_index', b'scan_file_index'), (b'retention_time', b'retention_time')]),
        ),
        migrations.AlterField(
            model_name='peaklist',
            name='file',
            field=models.FileField(help_text=b'MGF file', upload_to=CLMSVault.CLMSVault_app.models.upload_path_handler_peak_list),
        ),
    ]
