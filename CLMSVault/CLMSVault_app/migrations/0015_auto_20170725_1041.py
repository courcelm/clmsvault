# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0014_auto_20170725_1016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='peaklist',
            name='replace_ms2_ref',
            field=models.BooleanField(default=False, help_text=b'Replace existing MS/MS references.'),
        ),
    ]
