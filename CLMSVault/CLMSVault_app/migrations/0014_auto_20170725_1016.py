# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0013_auto_20170626_1059'),
    ]

    operations = [
        migrations.AddField(
            model_name='peaklist',
            name='parsing_log',
            field=models.CharField(max_length=1000, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='peaklist',
            name='parsing_status',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='peaklist',
            name='replace_ms2_ref',
            field=models.BooleanField(default=True, help_text=b'Replace existing MS/MS references.'),
            preserve_default=True,
        ),
    ]
