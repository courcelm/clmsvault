# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import CLMSVault.CLMSVault_app.models


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0006_auto_20170206_0926'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='peaklist',
            name='file_name',
        ),
        migrations.AlterField(
            model_name='peaklist',
            name='file',
            field=models.FileField(upload_to=CLMSVault.CLMSVault_app.models.upload_path_handler_peak_list),
        ),
    ]
