# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='quantification',
            name='file_header',
            field=models.CharField(max_length=20, null=True, choices=[(b'CF', b'"CLPeptideId","FoldChange",'), (b'FSF', b'"File","ScanNumber","FoldChange",')]),
            preserve_default=True,
        ),
    ]
