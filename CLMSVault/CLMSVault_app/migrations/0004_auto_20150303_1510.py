# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0003_auto_20150303_1203'),
    ]

    operations = [
        migrations.RenameField(
            model_name='clpeptidefilter',
            old_name='quantification_min_operator',
            new_name='quantification_operator_1',
        ),
        migrations.RenameField(
            model_name='clpeptidefilter',
            old_name='quantification_max_operator',
            new_name='quantification_operator_2',
        ),
        migrations.RenameField(
            model_name='clpeptidefilter',
            old_name='quantification_min_value',
            new_name='quantification_value_1',
        ),
        migrations.RenameField(
            model_name='clpeptidefilter',
            old_name='quantification_max_value',
            new_name='quantification_value_2',
        ),
        migrations.AddField(
            model_name='clpeptidefilter',
            name='quantification_logic',
            field=models.CharField(blank=True, max_length=1, null=True, choices=[(b'&', b'AND'), (b'|', b'OR')]),
            preserve_default=True,
        ),
    ]
