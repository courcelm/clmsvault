# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import CLMSVault.CLMSVault_app.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CLPeptide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('run_name', models.CharField(max_length=1000)),
                ('scan_number', models.IntegerField(verbose_name=b'Scan #')),
                ('precursor_mz', models.FloatField()),
                ('precursor_charge', models.CharField(max_length=5, verbose_name=b'z')),
                ('precursor_intensity', models.FloatField()),
                ('rank', models.IntegerField()),
                ('match_score', models.FloatField()),
                ('spectrum_intensity_coverage', models.FloatField()),
                ('total_fragment_matches', models.IntegerField(verbose_name=b'# Fragments')),
                ('delta', models.FloatField()),
                ('error', models.FloatField()),
                ('peptide1', models.CharField(max_length=100)),
                ('peptide_wo_mod1', models.CharField(max_length=150)),
                ('display_protein1', models.CharField(max_length=250)),
                ('peptide_position1', models.IntegerField(verbose_name=b'Pep. Pos. 1')),
                ('pep1_link_pos', models.IntegerField()),
                ('peptide2', models.CharField(max_length=100)),
                ('peptide_wo_mod2', models.CharField(max_length=150)),
                ('display_protein2', models.CharField(max_length=250)),
                ('peptide_position2', models.IntegerField(verbose_name=b'Pep. Pos. 2')),
                ('pep2_link_pos', models.IntegerField()),
                ('autovalidated', models.BooleanField(default=False)),
                ('validated', models.CharField(max_length=50)),
                ('rejected', models.BooleanField(default=False)),
                ('notes', models.CharField(max_length=100)),
                ('link_type', models.CharField(max_length=50, choices=[(b'Inter-protein', b'Inter-protein'), (b'Intra-protein', b'Intra-protein'), (b'Intra-peptide', b'Intra-peptide'), (b'Dead-end', b'Dead-end')])),
                ('cross_link', models.BooleanField(default=False, verbose_name=b'Inter-peptide cross-link')),
                ('not_decoy', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['-match_score'],
            },
            bases=(CLMSVault.CLMSVault_app.models.AdminURLMixin, models.Model),
        ),
        migrations.CreateModel(
            name='CLPeptideFilter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=100, verbose_name=b'Filter name')),
                ('description', models.TextField(verbose_name=b'Detailed description', blank=True)),
                ('fp_cutoff', models.FloatField(help_text=b'Range from 0 to 1. Applied before unique filter.', null=True, verbose_name=b'False positive cutoff', blank=True)),
                ('remove_decoy', models.BooleanField(default=False, verbose_name=b'Remove decoy hits')),
                ('KK_link', models.BooleanField(default=False, verbose_name=b'Remove non K-K cross-links')),
                ('unique_in', models.CharField(blank=True, max_length=20, null=True, verbose_name=b'Unique/False positive peptide in', choices=[(b'dataset', b'Dataset'), (b'msrun', b'MS run')])),
                ('unique_key', models.CharField(blank=True, max_length=20, null=True, verbose_name=b'Unique peptide key', choices=[(b'sequence', b'Peptide sequences'), (b'sequence-charge', b'Peptide sequences & charge'), (b'sequenceMods', b'Peptide sequences & mods'), (b'sequenceCpos', b'Peptide sequences & cross-link positions'), (b'proteinPpos', b'Proteins & cross-link positions')])),
            ],
            options={
            },
            bases=(CLMSVault.CLMSVault_app.models.AdminURLMixin, models.Model),
        ),
        migrations.CreateModel(
            name='CLPeptideFilterParam',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('method', models.CharField(max_length=20, choices=[(b'exclude', b'Exclude'), (b'filter', b'Filter')])),
                ('field', models.CharField(max_length=100, choices=[('id', 'id'), (b'run_name', b'run_name'), (b'scan_number', b'scan_number'), (b'precursor_mz', b'precursor_mz'), (b'precursor_charge', b'precursor_charge'), (b'precursor_intensity', b'precursor_intensity'), (b'rank', b'rank'), (b'match_score', b'match_score'), (b'spectrum_intensity_coverage', b'spectrum_intensity_coverage'), (b'total_fragment_matches', b'total_fragment_matches'), (b'delta', b'delta'), (b'error', b'error'), (b'peptide1', b'peptide1'), (b'peptide_wo_mod1', b'peptide_wo_mod1'), (b'display_protein1', b'display_protein1'), (b'fs_prot1_id', b'fs_prot1_id'), (b'peptide_position1', b'peptide_position1'), (b'pep1_link_pos', b'pep1_link_pos'), (b'peptide2', b'peptide2'), (b'peptide_wo_mod2', b'peptide_wo_mod2'), (b'display_protein2', b'display_protein2'), (b'fs_prot2_id', b'fs_prot2_id'), (b'peptide_position2', b'peptide_position2'), (b'pep2_link_pos', b'pep2_link_pos'), (b'autovalidated', b'autovalidated'), (b'validated', b'validated'), (b'rejected', b'rejected'), (b'notes', b'notes'), (b'link_type', b'link_type'), (b'cross_link', b'cross_link'), (b'not_decoy', b'not_decoy')])),
                ('field_lookup', models.CharField(max_length=50, choices=[(b'exact', b'Exact match'), (b'iexact', b'Exact match case insentive'), (b'contains', b'Contains'), (b'icontains', b'Contains case insentive'), (b'gt', b'Greater than'), (b'gte', b'Greater than or equal to'), (b'lt', b'Less than'), (b'lte', b'Less than or equal to'), (b'startswith', b'Starts-with'), (b'istartswith', b'Starts-with case insensitive'), (b'endswith', b'Ends-with'), (b'iendswith', b'Ends-with case insensitive'), (b'isnull', b'Is null'), (b'regex', b'Regular expression match'), (b'iregex', b'Regular expression match case insensitive')])),
                ('value', models.CharField(max_length=50)),
                ('clpeptidefilter', models.ForeignKey(to='CLMSVault_app.CLPeptideFilter')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CrossLinker',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Dataset',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=100)),
                ('description', models.TextField(verbose_name=b'Detailed description', blank=True)),
            ],
            options={
                'ordering': ['-creation_date'],
                'abstract': False,
            },
            bases=(CLMSVault.CLMSVault_app.models.AdminURLMixin, models.Model),
        ),
        migrations.CreateModel(
            name='FastaDB',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(unique=True, max_length=200)),
                ('file', models.FileField(help_text=b'Select FASTA file.', max_length=255, upload_to=CLMSVault.CLMSVault_app.models.upload_path_handler)),
                ('identifier_regexp', models.CharField(max_length=50)),
                ('gene_name_regexp', models.CharField(max_length=50, blank=True)),
                ('description_regexp', models.CharField(max_length=50, blank=True)),
                ('species_regexp', models.CharField(max_length=50, blank=True)),
                ('parsing_status', models.BooleanField(default=False)),
                ('parsing_log', models.CharField(max_length=1000, null=True, blank=True)),
                ('sequence_count', models.IntegerField(default=0)),
                ('update', models.BooleanField(default=True, help_text=b'Trigger update for fields regexp.Will not stay checked after save.')),
            ],
            options={
                'ordering': ['-creation_date'],
            },
            bases=(CLMSVault.CLMSVault_app.models.AdminURLMixin, models.Model),
        ),
        migrations.CreateModel(
            name='FastaDb_Sequence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(max_length=50, blank=True)),
                ('gene_name', models.CharField(max_length=50, blank=True)),
                ('raw_description', models.CharField(max_length=250)),
                ('description', models.CharField(max_length=250, blank=True)),
                ('species', models.CharField(max_length=100, blank=True)),
                ('sequence', models.TextField()),
                ('fastadb', models.ForeignKey(to='CLMSVault_app.FastaDB')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Instrument',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PDB',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(unique=True, max_length=50)),
                ('title', models.CharField(max_length=1000)),
                ('file', models.FileField(help_text=b'Select PDB file.', max_length=255, upload_to=CLMSVault.CLMSVault_app.models.upload_path_handler_PDB)),
            ],
            options={
                'ordering': ['identifier'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProcessedDataset',
            fields=[
                ('dataset_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='CLMSVault_app.Dataset')),
                ('clpeptidefilter', models.ForeignKey(blank=True, to='CLMSVault_app.CLPeptideFilter', null=True)),
            ],
            options={
            },
            bases=('CLMSVault_app.dataset',),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(unique=True, max_length=250)),
            ],
            options={
                'ordering': ['-creation_date'],
            },
            bases=(CLMSVault.CLMSVault_app.models.AdminURLMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Quantification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=100)),
                ('quantification_type', models.CharField(max_length=20, choices=[(b'FC', b'Fold change'), (b'FClog2', b'Fold change log2'), (b'FClog10', b'Fold change log10')])),
                ('parsing_status', models.BooleanField(default=False)),
                ('parsing_log', models.CharField(max_length=1000, null=True, blank=True)),
                ('file', models.FileField(help_text=b'Select quantification file.', max_length=255, upload_to=CLMSVault.CLMSVault_app.models.upload_path_handler)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='QuantificationFC',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fold_change', models.FloatField()),
                ('clpeptide', models.ForeignKey(to='CLMSVault_app.CLPeptide')),
                ('quantification', models.ForeignKey(to='CLMSVault_app.Quantification')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RawDataset',
            fields=[
                ('dataset_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='CLMSVault_app.Dataset')),
                ('file', models.FileField(help_text=b'pLink note: Merge the files ending with xlink_qry.proteins.txt from 1.sample\\search folder.', max_length=255, upload_to=CLMSVault.CLMSVault_app.models.upload_path_handler)),
                ('extra_file', models.FileField(help_text=b'pLink only: select pLink_combine.spectra.xls from 2.report\\sample1 folder. This file is used to filter FDR filtered hits.', max_length=255, upload_to=CLMSVault.CLMSVault_app.models.upload_path_handler, blank=True)),
                ('parsing_status', models.BooleanField(default=False)),
                ('parsing_log', models.CharField(max_length=1000, null=True, blank=True)),
            ],
            options={
            },
            bases=('CLMSVault_app.dataset',),
        ),
        migrations.CreateModel(
            name='SearchAlgorithm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='processeddataset',
            name='datasets',
            field=models.ManyToManyField(related_name=b'processed_datasets', to='CLMSVault_app.Dataset'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataset',
            name='cross_linker',
            field=models.ForeignKey(to='CLMSVault_app.CrossLinker', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataset',
            name='fasta_db',
            field=models.ForeignKey(to='CLMSVault_app.FastaDB', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataset',
            name='instrument_name',
            field=models.ForeignKey(to='CLMSVault_app.Instrument', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataset',
            name='project',
            field=models.ForeignKey(to='CLMSVault_app.Project'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataset',
            name='search_algorithm',
            field=models.ForeignKey(to='CLMSVault_app.SearchAlgorithm', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='clpeptide',
            name='dataset',
            field=models.ManyToManyField(to='CLMSVault_app.Dataset'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='clpeptide',
            name='fs_prot1_id',
            field=models.ForeignKey(related_name=b'clpep1', to='CLMSVault_app.FastaDb_Sequence', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='clpeptide',
            name='fs_prot2_id',
            field=models.ForeignKey(related_name=b'clpep2', to='CLMSVault_app.FastaDb_Sequence', null=True),
            preserve_default=True,
        ),
    ]
