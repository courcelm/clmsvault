# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0002_quantification_file_header'),
    ]

    operations = [
        migrations.AddField(
            model_name='clpeptidefilter',
            name='quantification_experiment',
            field=models.ForeignKey(blank=True, to='CLMSVault_app.Quantification', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='clpeptidefilter',
            name='quantification_max_operator',
            field=models.CharField(blank=True, max_length=3, null=True, choices=[(b'gt', b'Greater than'), (b'gte', b'Greater than or equal to'), (b'lt', b'Less than'), (b'lte', b'Less than or equal to')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='clpeptidefilter',
            name='quantification_max_value',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='clpeptidefilter',
            name='quantification_min_operator',
            field=models.CharField(blank=True, max_length=3, null=True, choices=[(b'gt', b'Greater than'), (b'gte', b'Greater than or equal to'), (b'lt', b'Less than'), (b'lte', b'Less than or equal to')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='clpeptidefilter',
            name='quantification_min_value',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
