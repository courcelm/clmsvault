# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CLMSVault_app', '0008_crosslinker_mass'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='crosslinker',
            name='mass',
        ),
        migrations.AddField(
            model_name='crosslinker',
            name='linked_formula',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
    ]
