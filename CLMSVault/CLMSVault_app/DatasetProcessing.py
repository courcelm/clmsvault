## Copyright 2013-2018 Mathieu Courcelles
## Mike Tyers's lab / IRIC / Universite de Montreal 

# Import standard libraries
import subprocess
import os
import tempfile

# Import Django related libraries
from django.conf import settings
from django.contrib import messages
from django.db import transaction
from django.db.models import Q
from django.utils.safestring import mark_safe


# Import project libraries
from .models import (CLPeptide, 
                    ProcessedDataset,
                    QuantificationFC,
                    )

from .queryset_operation import (dataset_set_2_clpeptide_set,
                                )

import CLMSVault.CLMSVault_app.export as export



class DatasetProcessing:
    """
    This class process datasets of cross-linked peptides.
    It groups multiple datasets in a single one and filter
    cross-linked peptides.
    """
    
    @staticmethod
    @transaction.atomic
    def process(instance):
        """
        Groups multiple datasets in a single one and filter
        cross-linked peptides.
        """
        
        datasets = instance.datasets.all()

        # Transfer extra info
        fields = dict()
        fields['cross_linker'] = ''
        fields['instrument_name'] = ''
        fields['fasta_db'] = ''
        fields['search_algorithm'] = ''
        dataset_description = ''

        
        # Iterates selected datasets to get information
        for dataset in datasets:
            
            dataset_description += dataset.description
            
            # Check extra info homogenity
            for field in fields:
                if fields[field] == '':
                    fields[field] = dataset.__getattribute__(field)
                elif fields[field] != dataset.__getattribute__(field):
                    fields[field] = None
                    
        # Update extra info in database
        fields['description'] = instance.description + dataset_description
        ProcessedDataset.objects.filter(pk=instance.id).update(**fields)
            
            
        # Take care first of CLPeptides
        datasets_list = list(datasets.values_list('pk', flat=True))
        clpeptide_set = CLPeptide.objects.filter(dataset__in=datasets_list)
        
        # Filter for quantification
        if instance.clpeptidefilter is not None and instance.clpeptidefilter.quantification_experiment is not None:
            qfcs = QuantificationFC.objects.filter(quantification_id=instance.clpeptidefilter.quantification_experiment)
            fc = dict()
            
            if instance.clpeptidefilter.quantification_value_1 is not None:
                fc['__'.join(['fold_change', instance.clpeptidefilter.quantification_operator_1])] = instance.clpeptidefilter.quantification_value_1
                
            if instance.clpeptidefilter.quantification_value_2 is not None:
                fc['__'.join(['fold_change', instance.clpeptidefilter.quantification_operator_2])] = instance.clpeptidefilter.quantification_value_2
            
            if len(fc) == 1:
                qfcs = qfcs.filter(**fc)
            
            if len(fc) == 2:
                keys = fc.keys()
                
                if instance.clpeptidefilter.quantification_logic == '|':
                    qfcs = qfcs.filter(Q( **{keys[0]: fc[keys[0]]}) | Q(**{keys[1]: fc[keys[1]]})  )
                elif instance.clpeptidefilter.quantification_logic == '&':
                    qfcs = qfcs.filter(Q( **{keys[0]: fc[keys[0]]}) & Q(**{keys[1]: fc[keys[1]]})  )
                    
            
            quant_clpeptide_pk_list = list(qfcs.values_list('clpeptide', flat=True))
            clpeptide_set = clpeptide_set.filter(pk__in=quant_clpeptide_pk_list)

        
        
        # Apply/prepare filter/exclude to CLPeptides
        fp_msrun = dict()
        unique_msrun_pep = dict()
        unique_key = dict()
        
        if instance.clpeptidefilter:
            for fe in instance.clpeptidefilter.clpeptidefilterparam_set.all():

                d = dict()
                d['__'.join([fe.field, fe.field_lookup])] = fe.value

                if fe.method == 'filter':
                    clpeptide_set = clpeptide_set.filter(**d)
                else:
                    clpeptide_set = clpeptide_set.exclude(**d)
        
        
            # Determine false positive cutoff score
            if instance.clpeptidefilter.fp_cutoff is not None:
                
                fp_msrun['dataset'] = [0.0, 0.0, 100000]
                
                for clpep in clpeptide_set:
                    
                    clpep_count = 0.0;
                    clpep_decoy_count = 0.0;
                    min_score = 100000;
                
                    if clpep.run_name in fp_msrun:
                        clpep_count, clpep_decoy_count, min_score  = fp_msrun[clpep.run_name]
                    
                    clpep_count += 1
                    fp_msrun['dataset'][0] += 1
                    
                    if clpep.not_decoy == False:
                        clpep_decoy_count += 1
                        fp_msrun['dataset'][1] += 1
                    
                    if (clpep_decoy_count / clpep_count) <= instance.clpeptidefilter.fp_cutoff:
                        min_score = clpep.match_score
                        
                    if (fp_msrun['dataset'][1] / fp_msrun['dataset'][0]) <= instance.clpeptidefilter.fp_cutoff:
                        fp_msrun['dataset'][2] = clpep.match_score
                    
                    # Store value for next iteration
                    fp_msrun[clpep.run_name] = [clpep_count, clpep_decoy_count, min_score]

                
            # Unique peptide filter
            if instance.clpeptidefilter.unique_key is not None:
                
                for key in instance.clpeptidefilter.unique_key.split('-'):
                    unique_key[key] = True;        
        
        
        # Add peptides to the new dataset           
        for clpep in clpeptide_set:
            
            # Apply filter
            if instance.clpeptidefilter is not None:
            
                # K-K link filter
                #print instance.clpeptidefilter.KK_link
                
                if instance.clpeptidefilter.KK_link:

                    if not clpep.cross_link:
                        continue
                    
                    if clpep.pep1_link_pos == 1:
                        continue
                    
                    if clpep.pep2_link_pos == 1:
                        continue
                    
                    index1 = clpep.pep1_link_pos
                    p1 = clpep.peptide_wo_mod1
                    index2 = clpep.pep2_link_pos
                    p2 = clpep.peptide_wo_mod2
                    
                    aa1 = clpep.peptide_wo_mod1[clpep.pep1_link_pos - 1]
                    
                    if aa1 != 'K':
                        continue

                    if clpep.link_type == 'Intra-peptide':
                        aa2 = clpep.peptide_wo_mod1[clpep.pep2_link_pos - 1]

                    else:
                        aa2 = clpep.peptide_wo_mod2[clpep.pep2_link_pos - 1]
                    
                    #print ('%s-%s') % (aa1, aa2)
                    #print aa1 != aa2
                    
                    if aa2 != 'K':
                        continue
            
                # Skip decoy if requested
                if instance.clpeptidefilter.remove_decoy and clpep.not_decoy == False:
                    continue
            
                # Test peptide uniqueness
                msrun = clpep.run_name
                key = ''
                
                if instance.clpeptidefilter.unique_key is not None:
                    
                    if instance.clpeptidefilter.unique_in == 'dataset':
                        msrun = 'Dataset'
                    
                    # Skip peptide if already seen
                    key = clpep.uniqueKey(unique_key)
    
                    if not msrun in unique_msrun_pep:
                        unique_msrun_pep[msrun] = dict()
                
                    if key in unique_msrun_pep[msrun]:
                        continue
                    else:
                        unique_msrun_pep[msrun][key] = True
                
                    
                # Limit false positives
                if instance.clpeptidefilter.fp_cutoff is not None:
                    
                    if instance.clpeptidefilter.unique_in == 'msrun':
                        if clpep.match_score < fp_msrun[clpep.run_name][2]:
                            continue
                    else:
                        if clpep.match_score < fp_msrun['dataset'][2]:
                            break
                    
            # Add peptide to dataset
            clpep.dataset.add(instance)
            #clpep.save()
            
            

    @staticmethod
    @transaction.atomic
    def run_Percolator(self, request, queryset, form):
        """
        Run Percolator on a dataset and create a new Processed dataset
        """
        

        # Write cross-links to Percolator TSV format
        temp_fh = tempfile.NamedTemporaryFile(delete=False, mode='w')

        clpeptide_set = dataset_set_2_clpeptide_set(queryset)
        
        
        # Filter cross-link type
        if form.cleaned_data['cl_type'] == 'IntraInter':
            clpeptide_set = clpeptide_set.filter(cross_link=True).exclude(link_type='Intra-peptide')
        elif form.cleaned_data['cl_type'] == 'Intra':
            clpeptide_set = clpeptide_set.filter(link_type='Intra-protein')
        elif form.cleaned_data['cl_type'] == 'Inter':
            clpeptide_set = clpeptide_set.filter(link_type='Inter-protein')

        export.make_Percolator_tsv(self, request, clpeptide_set, 
                              'Dataset_CLPeptide', fh=temp_fh, nv=form.cleaned_data['nv'])
        temp_fh.close()
        
        
        
        # Get q-value
        q_value = form.cleaned_data['q_value']
 
        
        # Run Percolator
        out_file = temp_fh.name + '_perco'
        
        p = subprocess.Popen('"%s" -t %s -F %s -U "%s" > %s' % (settings.PERCOLATOR_BIN, 
                                                          q_value,
                                                          q_value,
                                                          temp_fh.name, 
                                                          out_file), 
                             shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        retval = p.wait()
    
        
        
        
        # Check if Percolator had a problem

        lines = [x.decode('utf8', 'ignore') for x in p.stdout.readlines()]
        percolator_out = '<br />'.join(lines)

        if retval != 0:
            messages.add_message(request, messages.ERROR, mark_safe(percolator_out))
        else:
            messages.add_message(request, messages.INFO, mark_safe(percolator_out))
            
            # Read CLpeptideId
            clpeptideid_dict  = dict()
            
            f = open(out_file, 'r')
            f.readline()  # Skip header
            
            
            for line in f:
                fields = line.split('\t')
                
                # Apply q-value cut-off
                if float(fields[2]) <= q_value:
                    clpeptideid_dict[int(fields[0])] = True
                
            f.close()

            # Create processed dataset
            datasets = queryset
            datasets_pk = []

            # Transfer extra info
            fields = dict()
            fields['cross_linker'] = ''
            fields['instrument_name'] = ''
            fields['fasta_db'] = ''
            fields['name'] = ''
            fields['description'] = ''
            fields['project_id'] = ''
            fields['search_algorithm'] = ''    
            
            # Iterates selected datasets to get information
            for dataset in datasets:
                
                datasets_pk.append(dataset.pk)
                
                # Check extra info homogenity
                for field in fields:
                    if fields[field] == '':
                        fields[field] = dataset.__getattribute__(field)

                        
            # Update extra info in database
            fields['name'] += ' (percolated: q-value <=%s, cl-type=%s, nv=%s)' % (q_value, 
                                                                                  form.cleaned_data['cl_type'],
                                                                                  form.cleaned_data['nv'])
            fields['description'] += percolator_out.replace('<br />', '\n')
            
            pdataset = ProcessedDataset(**fields)
            pdataset.save()
            
            # Add original dataset
            for pk in datasets_pk:
                pdataset.datasets.add(pk)
                
            # Add CLpeptide to dataset
            
            ThroughModel = CLPeptide.dataset.through
            through_list = []
            key_check = dict()
            
            for clpep in clpeptide_set:
                
                if clpep.pk in clpeptideid_dict:
                    
                    key = '%s-%s' % (clpep.pk, pdataset.pk)
                    
                    if key not in key_check:
                    
                        through_list.append(ThroughModel(clpeptide_id= clpep.pk, dataset_id=pdataset.pk))
                        key_check[key] = True

            # Link CLPetide object to dataset
            ThroughModel.objects.bulk_create(through_list)


            messages.add_message(request, messages.INFO, mark_safe('A new Processed dataset was created with Percolator results: ' + pdataset.formated_url()))
                    
            

        # Remove temporary file
        os.remove(temp_fh.name)

        if os.path.isfile(out_file):
            os.remove(out_file)