# # Copyright 2013-2017 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 


# Import standard libraries
from math import floor
import numpy as np
import os
from scipy.stats import mannwhitneyu

# Import Django related libraries
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.utils.safestring import mark_safe

# Import BioPython libraries
from Bio.PDB.PDBParser import PDBParser

# Import project libraries
from .parser.exception import InvalidFileFormatException
from .parser.mgf_parser import MGFParser
from .queryset_operation import clpeptide_set_2_protein_set
from .models import (CLPeptide,
                     FastaDb_Sequence,
                     PeakList,
                     QuantificationFC)
from .pdb_structure import (compute_cl_distance,
                            randomDistance,
                            )
from numpy.lib.function_base import average

# Dictionary of peptide modifications with associated symbol
modification_name_symbol = {
    # 'Carbamidomethyl': 'cm',
    # 'Cysteinyl': 'cy',
    # 'Deamidated': 'd',
    # 'Dimethyl': 'dme',
    # 'Methyl': 'me',
    # 'Oxidation': 'ox',
    # 'Phospho': 'ph',
    # 'TMT6plex': 'tmt'
    '[15.99]': 'ox',
    '[57.02]': 'cm',
    '[156.08]': 'dss',
}


def alive(request):
    """
    Returns HttpResponse with status 204
    :param request: HttpRequest
    :return: HttpResponse
    """

    return HttpResponse(status=204)


def format_peptide_seq_with_mod(peptide, _15n=False):
    """
    Formats peptide sequence string with modification for spectrum viewer.
    :param peptide: Peptide object
    :param _15n: Peptide is 15N labelled
    :return: Peptide sequence string with modification symbols.
    """

    for modification, symbol in modification_name_symbol.items():
        peptide = peptide.replace(modification, symbol)

    if _15n:

        tmp = list()

        for char in peptide:

            tmp += char

            if char == char.upper():

                if _15n == '15N_13C_':
                    tmp += '28'

                elif _15n == '15N_':
                    tmp += '15'

        peptide = ''.join(tmp)

    return peptide


@login_required
def clpeptide_msms(request, pk):
    """
    This view displays PSM MS/MS spectrum.
    :param request: HttpRequest object
    :param pk: Peptide primary key integer
    :return: HttpResponse
    """

    clpeptide = CLPeptide.objects.get(pk=pk)

    # Get MGF file
    try:
        run_name = clpeptide.run_name + '.mgf'

        peak_list_object = PeakList.objects.filter(file__endswith=run_name).last()

    except PeakList.DoesNotExist:
        raise Http404('No Peak list entry for %s.' % run_name)

    msms_file_name = os.path.join(settings.MEDIA_ROOT, str(peak_list_object.file))
    parser = MGFParser(msms_file_name)

    # Get peak list
    try:
        scan = parser.get_scan_at_index(clpeptide.scan_file_index)

    except IOError:
        raise Http404('Spectrum file not found (%s)' % msms_file_name)

    for i, value in enumerate(scan['peak_list']):
        scan['peak_list'][i] = '%s %s' % (value[0], value[1])

    peak_list = '\n'.join(scan['peak_list'])

    validation_disabled = ''

    if clpeptide.validated == '':
        clpeptide.validated = None

    pep1_15n = False
    pep2_15n = False

    if clpeptide.display_protein1.startswith('15N_13C_'):
        pep1_15n = '15N_13C_'
    elif clpeptide.display_protein1.startswith('15N_'):
        pep1_15n = '15N_'

    if clpeptide.display_protein2.startswith('15N_13C_'):
        pep2_15n = '15N_13C_'
    elif clpeptide.display_protein2.startswith('15N_'):
        pep2_15n = '15N_'

    context = {
        'myTolerance': 10,
        'myPrecursorZ': clpeptide.precursor_charge,
        'myPeptide': format_peptide_seq_with_mod(clpeptide.peptide1, _15n=pep1_15n),
        'myPeptide2': format_peptide_seq_with_mod(clpeptide.peptide2, _15n=pep2_15n),
        'myPeaklist': peak_list,
        'clpeptide': clpeptide,
        'validation_disabled': validation_disabled,
        'linked_formula': clpeptide.dataset.first().cross_linker.linked_formula
    }

    return render(request,
                  'spectrum_viewer.html',
                  context)


@login_required
def clpeptide_validation(request, pk, value):
    """
    This view updates validation status of a peptide spectrum match.
    :param request: HttpRequest object
    :param pk: Peptide primary key integer
    :param value: Validation character
    :return: HttpResponse object
    """

    clpeptide = CLPeptide.objects.get(pk=pk)

    json_data = '{"updated": 0}'

    if value in ['G', 'Y', 'R']:
        clpeptide.validated = value
        clpeptide.save()

        json_data = '{"updated": 1}'

    return HttpResponse(json_data, content_type='application/json')


@login_required
def xiNET_view(request, clpeptide_set, form):
    """
    This function generates a view to display cross-links with xiNET.
    """

    clpeptide_set = clpeptide_set.filter(not_decoy=True, cross_link=True)

    quant_set = QuantificationFC.objects.filter(quantification=form.cleaned_data['quantification'])

    # Prepare protein set
    protein_set = clpeptide_set_2_protein_set(clpeptide_set)

    # Find absolute max if none is specified
    limit = 0
    if form.cleaned_data['fold_change_limit'] is None:

        for quant in quant_set:

            absolute_value = abs(quant.fold_change)

            if absolute_value > limit:
                limit = absolute_value
    else:
        limit = form.cleaned_data['fold_change_limit']

    c = {'clpeptide_set': clpeptide_set,
         'color_scheme': form.cleaned_data['color_scheme'],
         'fold_change_limit': limit,
         'protein_set': protein_set,
         'quant_set': quant_set,
         'hide_not_quantified': form.cleaned_data['hide_not_quantified'],
         'min_score': form.cleaned_data['min_score'],
         'max_score': form.cleaned_data['max_score']}

    return render(request, 'xiNET.html', c)


@login_required
def jsmol_view(request, clpeptide_set, form=None):
    """
    This function generates a view to display cross-links with JSMol.
    """

    pdb = form.get_PDB()

    ukey = dict()
    ukey['proteinPpos'] = True

    try:
        clpeptide_set, alignments = compute_cl_distance(clpeptide_set,
                                                        pdb,
                                                        form.cleaned_data['protein_identity'],
                                                        form.cleaned_data['peptide_identity'],
                                                        unique_key=ukey)
    except InvalidFileFormatException as e:
        return HttpResponse('<h1>%s</h1>' % e)

    cross_links = []
    validation_list = list()

    cl_count = len(clpeptide_set)
    mapped_cl_count = 0

    distance_list = []
    min_distance_list = []

    # Quant
    quant_set = QuantificationFC.objects.filter(quantification=form.cleaned_data['quantification'])

    # Find absolute max if none is specified
    limit = 0
    quant_clpep_key = {}

    for quant in quant_set:

        absoluate_value = abs(quant.fold_change)

        if absoluate_value > limit:
            limit = absoluate_value

        quant_clpep_key[quant.clpeptide.pk] = True

    if form.cleaned_data['fold_change_limit'] is not None:
        limit = form.cleaned_data['fold_change_limit']

    clink_residues_dict = dict()

    for clpep in clpeptide_set:

        if form.cleaned_data['hide_not_quantified'] and clpep.pk not in quant_clpep_key:
            continue

        if clpep.fs_prot1_id is None:
            continue

        protein_1 = '%s [%s]' % (clpep.fs_prot1_id.identifier, clpep.peptide_position1 + clpep.pep1_link_pos - 1)

        protein_2 = ''
        if clpep.fs_prot2_id is not None:
            protein_2 = '%s [%s]' % (clpep.fs_prot2_id.identifier, clpep.peptide_position2 + clpep.pep2_link_pos - 1)

        j = False;

        min_distance = 1000000000000

        for cldistance in clpep.distances:

            if cldistance.distance != '':
                j = True
                distance_list.append(float(cldistance.distance))
                clink_residues_dict[cldistance.residue_1_str] = cldistance.residue_1
                clink_residues_dict[cldistance.residue_2_str] = cldistance.residue_2

                if min_distance > float(cldistance.distance):
                    min_distance = float(cldistance.distance)

            cross_links.append(
                '{pk: "%s", prot1: "%s", prot2: "%s", res1: "%s", res2: "%s", score: "%.2f", distance: "%s", ident1: "%s", ident2: "%s"}' % (
                    clpep.pk,
                    str(protein_1),
                    str(protein_2),
                    cldistance.residue_1_str,
                    cldistance.residue_2_str,
                    clpep.match_score,
                    cldistance.distance,
                    cldistance.identity_1,
                    cldistance.identity_2,
                ))

        validation_list.append(clpep)

        if min_distance != 1000000000000:
            min_distance_list.append(min_distance)

        if j:
            mapped_cl_count += 1

    pdb.clink_residues_list = clink_residues_dict.values()

    # Check distance distribution of cross-links to see if it is random
    p_values = []
    draw_count = 1000

    p_val_threshold = 0.05
    lower_count = 0

    s_mean = np.mean(min_distance_list)

    for x in range(0, draw_count):

        random_distance_list = randomDistance(pdb, form.cleaned_data['cross_linked_residues'],
                                              len(min_distance_list))

        if len(min_distance_list) > 0 and len(random_distance_list) > 0:

            try:
                p_values.append(mannwhitneyu(min_distance_list, random_distance_list)[1] * 2)
                r_mean = np.mean(random_distance_list)
                if p_values[-1] <= p_val_threshold:
                    lower_count += 1

            except ValueError:
                p_values.append(1)
        else:
            p_values.append(1)

    min_p_value = min(p_values)
    max_p_value = max(p_values)
    avg_p_value = average(p_values)

    p_value_str = 'p-value <= %.1e' % max_p_value
    ratio = lower_count / float(draw_count)

    if ratio < 1:
        p_value_str = 'p-value <= 0.05 for %d %% of the draws.' % (ratio * 100)

    # Get compound details
    parser = PDBParser()
    structure = parser.get_structure('tmp', pdb.file.file.name)
    pdb.compound = structure.header['compound']

    chain_list = list()
    for chain in structure.get_chains():
        chain_list.append(chain.id)

    molecules = list()

    for key in pdb.compound:
        molecules.append([key, pdb.compound[key].get('molecule', ''), pdb.compound[key].get('chain', '')])

    # Histogram
    all_bin_values, all_bin_labels = ([], [])
    min_bin_values, min_bin_labels = ([], [])
    rand_bin_values, rand_bin_labels = ([], [])

    floor_all_distance_list = [0, ]

    if len(distance_list) != 0:
        floor_all_distance_list = [floor(x) for x in distance_list]
        all_bin_values, all_bin_labels = np.histogram(floor_all_distance_list, bins=np.arange(0,
                                                                                              max(
                                                                                                  floor_all_distance_list) + 4,
                                                                                              2))
    if len(min_distance_list) != 0:
        floor_min_distance_list = [floor(x) for x in min_distance_list]
        min_bin_values, min_bin_labels = np.histogram(floor_min_distance_list, bins=np.arange(0,
                                                                                              max(
                                                                                                  floor_all_distance_list) + 4,
                                                                                              2))

    if len(random_distance_list) != 0:
        floor_rand_distance_list = [floor(x) for x in random_distance_list]
        rand_bin_values, rand_bin_labels = np.histogram(floor_rand_distance_list, bins=np.arange(0,
                                                                                                 max(
                                                                                                     floor_all_distance_list) + 4,
                                                                                                 2))

    # Replace fasta_id with identifier
    for key in list(alignments.keys()):
        seq = FastaDb_Sequence.objects.filter(pk=key)
        identifier = seq[0].identifier
        alignments[identifier] = alignments.pop(key)

    # Ready
    c = {'cross_links_array': mark_safe("[" + ','.join(cross_links) + "]"),
         'pdb': pdb,
         'cl_count': cl_count,
         'mapped_cl_count': mapped_cl_count,
         'viewer': form.cleaned_data['viewer'],
         'molecules': molecules,
         'chain_list': chain_list,
         'alignments': alignments,
         'all_bin_values': all_bin_values,
         'min_bin_values': min_bin_values,
         'rand_bin_values': rand_bin_values,
         'bin_labels': all_bin_labels,
         'color_scheme': form.cleaned_data['color_scheme'],
         'fold_change_limit': limit,
         'quant_set': quant_set,
         'min_val': 0,
         'draw_count': draw_count,
         'p_value': p_value_str,
         'min_score': form.cleaned_data['min_score'],
         'max_score': form.cleaned_data['max_score'],
         'validation_list': validation_list

         }

    return render(request, 'jsmol.html', c)
