"""
Copyright 2013-2018 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses
Mike Tyers's lab
Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries
import os
import re

# Import Django related libraries
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save, pre_save, pre_delete
from django.dispatch import receiver
from django.utils.safestring import mark_safe


# Import project libraries




# # File upload
def upload_path_handler(instance, filename):
    """
    Path handler for dataset file upload.
    """
    return "{classname}/{id}-{filename}".format(classname=instance.__class__.__name__,
                                                id=instance.pk, filename=filename)


class AdminURLMixin(object):
    """
    Generate admin url for objet in this model
    Code from: http://timmyomahony.com/blog/2012/12/12/reversing-admin-urls-and-creating-admin-links-you-models/
    """

    def formated_url(self):
        """
        Returns HTML link for the admin panel.
        """
        return '<a href="%s">%s</a><br />' % (self.get_admin_url(), self)

    def get_admin_url(self):
        content_type = ContentType \
            .objects \
            .get_for_model(self.__class__)
        return reverse("admin:%s_%s_change" % (
            content_type.app_label,
            content_type.model),
                       args=(self.id,))


#### Model classes #### #### #### #### #### #### #### #### #### #### #### ####

class CrossLinker(models.Model):
    """
    This class holds the name of all the cross-linkers.
    """

    name = models.CharField(max_length=100, unique=True)

    linked_formula = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class FastaDB(AdminURLMixin, models.Model):
    """
    This class holds the name of all the FASTA database
    and parsing configuration.
    """

    creation_date = models.DateTimeField(auto_now_add=True)

    name = models.CharField(max_length=200, unique=True)

    file = models.FileField(upload_to=upload_path_handler,
                            max_length=255,
                            help_text='Select FASTA file.')

    identifier_regexp = models.CharField(max_length=50)

    gene_name_regexp = models.CharField(max_length=50, blank=True)

    description_regexp = models.CharField(max_length=50, blank=True)

    species_regexp = models.CharField(max_length=50, blank=True)

    parsing_status = models.BooleanField(default=False)

    parsing_log = models.CharField(max_length=1000, blank=True, null=True)

    sequence_count = models.IntegerField(default=0)

    update = models.BooleanField(help_text=('Trigger update for fields regexp.'
                                            'Will not stay checked after save.'),
                                 default=True)

    def refreshParsing(self):
        """
        Update features with new regexp.
        """

        if self.pk:
            parsing_log = ''

            for fs in self.fastadb_sequence_set.all():
                parsing_log = fs.extractFeatures(self)
                fs.save()

            if parsing_log == '':
                self.parsing_log = 'Ok'
                self.parsing_status = True
            else:
                self.parsing_log = 'Error in: ' + parsing_log,
                self.parsing_status = False

            self.save()

    def __str__(self):
        return f'[{self.pk}] {self.name}'

    class Meta:
        ordering = ['-creation_date']


class FastaDb_Sequence(models.Model):
    """
    This class holds FASTA database protein sequences
    and extracted features.
    """

    fastadb = models.ForeignKey(FastaDB)

    identifier = models.CharField(max_length=50, blank=True)

    gene_name = models.CharField(max_length=50, blank=True)

    raw_description = models.CharField(max_length=250)

    description = models.CharField(max_length=250, blank=True)

    species = models.CharField(max_length=100, blank=True)

    sequence = models.TextField()

    def extractFeatures(self, fastadb_instance):
        """
        Applies regexp to extract features from description.
        """

        features = ['identifier',
                    'gene_name',
                    'description',
                    'species'
                    ]

        no_match = []

        for feature in features:

            regexp = fastadb_instance.__getattribute__(feature + '_regexp')

            if regexp != '':

                try:
                    matchObj = re.search(regexp,
                                         self.raw_description)
                except re.error:
                    no_match.append('Invalid regular expression for %s.'
                                    % feature)
                    break

                if matchObj:
                    try:
                        self.__setattr__(feature, matchObj.group(1))
                    except IndexError:
                        no_match.append(feature)
                else:
                    no_match.append(feature)

        return ','.join(no_match)

    def __str__(self):
        return self.identifier


class Instrument(models.Model):
    """
    This class holds the name of all the MS instruments.
    """

    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class SearchAlgorithm(models.Model):
    """
    This class holds the name of all the search algorithms.
    """

    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Project(AdminURLMixin, models.Model):
    """
    This class groups datasets.
    """

    creation_date = models.DateTimeField(auto_now_add=True)

    name = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return (f'[{self.pk}] {self.name}')

    class Meta:
        ordering = ['-creation_date']


class Dataset(AdminURLMixin, models.Model):
    """
    This class holds dataset information of cross-linked peptides.
    """

    creation_date = models.DateTimeField(auto_now_add=True)

    name = models.CharField(max_length=200)

    project = models.ForeignKey(Project)

    cross_linker = models.ForeignKey(CrossLinker, null=True)

    instrument_name = models.ForeignKey(Instrument, null=True)

    fasta_db = models.ForeignKey(FastaDB, null=True)

    search_algorithm = models.ForeignKey(SearchAlgorithm, null=True)

    description = models.TextField('Detailed description', blank=True)

    class Meta:
        abstract = False
        ordering = ['-creation_date']

    def formated_url(self):
        """
        Returns a formated HTML link for the CLPeptide admin panel. 
        """

        try:
            return '<a href="%s">%s</a><br />' % (self.rawdataset.get_admin_url(),
                                                  self)
        except Dataset.DoesNotExist:
            return '<a href="%s">%s</a><br />' % (self.processeddataset.get_admin_url(),
                                                  self)

    def formated_url_short(self):
        """
        Returns a formated HTML link for the CLPeptide admin panel. 
        """

        try:
            return '<a href="%s">[%s]</a><br />' % (self.rawdataset.get_admin_url(),
                                                    self.pk)
        except Dataset.DoesNotExist:
            return '<a href="%s">[%s]</a><br />' % (self.processeddataset.get_admin_url(),
                                                    self.pk)

    def __str__(self):
        return (f'[{self.pk}] {self.name}')


class RawDataset(Dataset):
    """
    This class holds raw dataset information of cross-linked peptides
    from search algorithm output.
    """

    file = models.FileField(upload_to=upload_path_handler,
                            max_length=255,
                            help_text=('pLink note: Merge the files ending '
                                       'with xlink_qry.proteins.txt from 1.sample\\search'
                                       ' folder.'))

    extra_file = models.FileField(upload_to=upload_path_handler, blank=True,
                                  max_length=255,
                                  help_text=('pLink only: select pLink_combine.spectra.xls '
                                             'from 2.report\\sample1 folder. This file is used '
                                             'to filter FDR filtered hits.'))

    parsing_status = models.BooleanField(default=False)

    parsing_log = models.CharField(max_length=1000, blank=True, null=True)


# Code for dataset file upload
# Code from http://stackoverflow.com/questions/9968532/django-admin-file-upload-with-current-model-id
_UNSAVED_FILEFIELD = 'unsaved_filefield'
_UNSAVED_FILEFIELD_EXTRA = 'unsaved_filefield_extra'


@receiver(pre_save)
def skip_saving_file(sender, instance, **kwargs):
    """
    Waits that object has been saved before saving files.
    """

    if sender in [RawDataset, FastaDB, PeakList, Quantification]:

        if not instance.pk and not hasattr(instance, _UNSAVED_FILEFIELD):
            setattr(instance, _UNSAVED_FILEFIELD, instance.file)
            instance.file = None

        if (not instance.pk and not hasattr(instance, _UNSAVED_FILEFIELD_EXTRA)
            and hasattr(instance, 'extra_file') and instance.extra_file):
            setattr(instance, _UNSAVED_FILEFIELD_EXTRA, instance.extra_file)
            instance.extra_file = None


@receiver(post_save)
def save_file(sender, instance, created, **kwargs):
    """
    Saves the files now that the dataset object has been saved.
    """

    if sender in [RawDataset, FastaDB, PeakList, Quantification]:

        if created and hasattr(instance, _UNSAVED_FILEFIELD):
            instance.file = getattr(instance, _UNSAVED_FILEFIELD)
            if created and hasattr(instance, _UNSAVED_FILEFIELD_EXTRA):
                instance.extra_file = getattr(instance, _UNSAVED_FILEFIELD_EXTRA)

            if instance.file != '':
                instance.save()


@receiver(pre_delete, sender=RawDataset)
def remove_CLPeptideRD(sender, instance, **kwargs):
    """
    Remove CLpeptides not linked to any dataset after deletion
    """

    for clpep in instance.clpeptide_set.all():
        if clpep.dataset.count() == 1:
            clpep.delete()


class CLPeptide(AdminURLMixin, models.Model):
    """
    This class holds details of cross-linked peptides.
    """

    dataset = models.ManyToManyField(Dataset)

    run_name = models.CharField(max_length=1000)

    scan_number = models.IntegerField('Scan #')

    precursor_mz = models.FloatField()

    precursor_charge = models.CharField('z', max_length=5)

    precursor_intensity = models.FloatField()

    rank = models.IntegerField()

    match_score = models.FloatField()

    spectrum_intensity_coverage = models.FloatField()

    total_fragment_matches = models.IntegerField('# Fragments')

    delta = models.FloatField()

    error = models.FloatField()

    peptide1 = models.CharField(max_length=100)

    peptide_wo_mod1 = models.CharField(max_length=150)

    display_protein1 = models.CharField(max_length=250)

    fs_prot1_id = models.ForeignKey(FastaDb_Sequence,
                                    related_name='clpep1', null=True)

    peptide_position1 = models.IntegerField('Pep. Pos. 1')

    pep1_link_pos = models.IntegerField()

    peptide2 = models.CharField(max_length=100)

    peptide_wo_mod2 = models.CharField(max_length=150)

    display_protein2 = models.CharField(max_length=250)

    fs_prot2_id = models.ForeignKey(FastaDb_Sequence,
                                    related_name='clpep2', null=True)

    peptide_position2 = models.IntegerField('Pep. Pos. 2')

    pep2_link_pos = models.IntegerField()

    autovalidated = models.BooleanField(default=False)

    validated = models.CharField(max_length=50)

    rejected = models.BooleanField(default=False)

    notes = models.CharField(blank=True, null=True,
                             max_length=100)

    LINK_TYPE_CHOICES = (
        ('Inter-protein', 'Inter-protein'),
        ('Intra-protein', 'Intra-protein'),
        ('Intra-peptide', 'Intra-peptide'),
        ('Dead-end', 'Dead-end'),
        ('Linear peptide', 'Linear peptide'),
    )

    link_type = models.CharField(max_length=50, choices=LINK_TYPE_CHOICES)

    cross_link = models.BooleanField('Cross-link', default=False)

    not_decoy = models.BooleanField(default=False)

    scan_file_index = models.BigIntegerField(default=-1)

    retention_time = models.FloatField(default=0)

    # Fields for XlinkX
    n_score_a = models.FloatField(blank=True, null=True)

    n_score_a_MS2_MS3 = models.FloatField(blank=True, null=True)

    n_score_b = models.FloatField(blank=True, null=True)

    n_score_b_MS2_MS3 = models.FloatField(blank=True, null=True)

    spectra_num = models.IntegerField(blank=True, null=True)

    class Meta:
        ordering = ['-match_score']

    def formated_url_jsmol(self):
        """
        Returns HTML link for the admin panel.
        """
        return '<a href="%s" style="text-decoration:underline;">%s</a><br />' % (self.get_admin_url(), self)

    def msms(self):

        if self.scan_file_index == -1:
            return ''

        colors = dict()
        colors['G'] = 'color: #00a65a !important;'
        colors['Y'] = 'color: #f39c12 !important;'
        colors['R'] = 'color: #dd4b39 !important;'
        colors[''] = ''
        colors[None] = ''

        url = reverse('view_msms', args=[self.pk])

        return mark_safe('<a href="%s" style="%s" target="_blank"><i class="fa fa-bar-chart "></i></a>' % (
            url, colors[self.validated]))

    def fixAutovalidated(self):
        """
        Convert the string value stored in the field to a boolean type.
        """

        if str(self.autovalidated).lower() == 'true':
            self.autovalidated = True
        else:
            self.autovalidated = False

    def guessLinkType(self):
        """
        This method guess the type of peptide cross-link since Xi is not
        providing this information.
        """

        self.link_type = 'Inter-protein'
        self.cross_link = True

        if self.peptide_wo_mod1 == self.peptide_wo_mod2:
            self.link_type = 'Inter-protein'
            self.cross_link = True

        elif self.display_protein1 == self.display_protein2:
            self.link_type = 'Intra-protein'
            self.cross_link = True

        else:

            if self.pep2_link_pos == -1:
                self.link_type = 'Dead-end'
                self.cross_link = False

                # Xi format for dead-end and linear peptides
                if self.pep1_link_pos == -1:

                    self.link_type = 'Linear peptide'

                    deadend_keywords = ['bs3', 'bs2g', 'adh_oh']

                    pepseq = self.peptide1
                    pepseq = re.sub('Cm', 'C', pepseq)
                    pepseq = re.sub('Mox', 'M', pepseq)
                    pepseq = re.sub('Sp', 'S', pepseq)
                    pepseq = re.sub('Tp', 'T', pepseq)
                    pepseq = re.sub('Yp', 'Y', pepseq)

                    for key in deadend_keywords:
                        pos = pepseq.find(key)

                        if pos != -1:
                            self.link_type = 'Dead-end'
                            self.pep1_link_pos = pos + 1
                            break

            elif self.pep2_link_pos > -1 and self.peptide_wo_mod2 == '-':
                self.link_type = 'Intra-peptide'
                self.cross_link = True

    def isDecoy(self):
        """
        Convert the string value stored in the field to a boolean type.
        """

        if (self.display_protein1 == 'DECOY' or
                    self.display_protein2 == 'DECOY'):

            self.not_decoy = False

        else:
            self.not_decoy = True

    def __str__(self):
        return str(self.pk)

    def uniqueKey(self, params):
        """
        Returns a unique key that describe the cross-linked peptide.
        """

        key = list()
        separator = '~~'

        if 'sequence' in params:
            seq_list = [self.peptide_wo_mod1, self.peptide_wo_mod2]
            seq_list.sort()
            key.extend(seq_list)

        if 'sequenceMods' in params:
            seq_list = [self.peptide1, self.peptide2]
            seq_list.sort()
            key.extend(seq_list)

        if 'sequenceCpos' in params:
            seq_list = [separator.join([self.peptide_wo_mod1,
                                        str(self.pep1_link_pos)]),
                        separator.join([self.peptide_wo_mod2,
                                        str(self.pep2_link_pos)])
                        ]
            seq_list.sort()
            key.extend(seq_list)

        if 'proteinPpos' in params:

            identifier1 = ''

            try:
                identifier1 = self.fs_prot1_id.identifier

            except:
                if self.peptide1:
                    identifier1 = 'DECOY'

            identifier2 = ''

            try:
                identifier2 = self.fs_prot2_id.identifier

            except:
                if self.peptide2:
                    identifier2 = 'DECOY'

            seq_list = [separator.join([identifier1,
                                        str(self.peptide_position1 +
                                            self.pep1_link_pos - 1)
                                        ]),
                        separator.join([identifier2,
                                        str(self.peptide_position2 +
                                            self.pep2_link_pos - 1)
                                        ])
                        ]
            seq_list.sort()
            key.extend(seq_list)

        if 'charge' in params:
            key.extend(self.precursor_charge)

        return separator.join(key)


class Quantification(models.Model):
    """
    This class Quantification experiment.
    """

    creation_date = models.DateTimeField(auto_now_add=True)

    name = models.CharField(max_length=100)

    TYPE_CHOICES = (
        ('FC', 'Fold change'),
        ('FClog2', 'Fold change log2'),
        ('FClog10', 'Fold change log10'),
    )

    quantification_type = models.CharField(max_length=20, choices=TYPE_CHOICES)

    parsing_status = models.BooleanField(default=False)

    parsing_log = models.CharField(max_length=1000, blank=True, null=True)

    HEADER_CHOICES = (
        ('CF', '"CLPeptideId","FoldChange"'),
        ('FSF', '"File","ScanNumber","FoldChange"')
    )

    file_header = models.CharField(max_length=20, choices=HEADER_CHOICES, null=True)

    file = models.FileField(upload_to=upload_path_handler,
                            max_length=255,
                            help_text='Select quantification file.')

    def __str__(self):
        return f'{self.pk} - {self.name}'


class QuantificationFC(models.Model):
    quantification = models.ForeignKey(Quantification)

    clpeptide = models.ForeignKey(CLPeptide)

    fold_change = models.FloatField()


class CLPeptideFilter(AdminURLMixin, models.Model):
    """
    This class holds pre-defined filters for cross-linked peptides.
    """

    creation_date = models.DateTimeField(auto_now_add=True)

    name = models.CharField('Filter name', max_length=100)

    description = models.TextField('Detailed description', blank=True)

    fp_cutoff = models.FloatField('False positive cutoff',
                                  help_text=('Range from 0 to 1. '
                                             'Applied before unique filter.'),
                                  blank=True, null=True)

    remove_decoy = models.BooleanField('Remove decoy hits', default=False)

    KK_link = models.BooleanField('Remove non K-K cross-links', default=False)

    UIN_CHOICES = (
        ('dataset', 'Dataset'),
        ('msrun', 'MS run'),
    )

    unique_in = models.CharField('Unique/False positive peptide in',
                                 max_length=20,
                                 choices=UIN_CHOICES,
                                 blank=True, null=True)

    UKEY_CHOICES = (
        ('sequence', 'Peptide sequences'),
        ('sequence-charge', 'Peptide sequences & charge'),
        ('sequenceMods', 'Peptide sequences & mods'),
        ('sequenceCpos', 'Peptide sequences & cross-link positions'),
        ('proteinPpos', 'Proteins & cross-link positions'),
    )

    unique_key = models.CharField('Unique peptide key', max_length=20,
                                  choices=UKEY_CHOICES,
                                  blank=True, null=True)

    OPERATOR_CHOICES = (
        ('gt', 'Greater than'),
        ('gte', 'Greater than or equal to'),
        ('lt', 'Less than'),
        ('lte', 'Less than or equal to'),
    )

    LOGIC_CHOICES = (
        ('|', 'OR'),
        ('&', 'AND'),

    )

    quantification_experiment = models.ForeignKey(Quantification, null=True,
                                                  blank=True)

    quantification_value_1 = models.FloatField(null=True, blank=True)

    quantification_operator_1 = models.CharField(max_length=3,
                                                 choices=OPERATOR_CHOICES,
                                                 null=True, blank=True)

    quantification_logic = models.CharField(max_length=1,
                                            choices=LOGIC_CHOICES,
                                            null=True, blank=True)

    quantification_value_2 = models.FloatField(null=True, blank=True)

    quantification_operator_2 = models.CharField(max_length=3,
                                                 choices=OPERATOR_CHOICES,
                                                 null=True, blank=True)

    def __str__(self):
        return f'[{self.pk}] {self.name}'


class CLPeptideFilterParam(models.Model):
    """
    Stores unit filter details (method, field, lookup and value).
    """

    clpeptidefilter = models.ForeignKey(CLPeptideFilter)

    METHOD_CHOICES = (
        ('exclude', 'Exclude'),
        ('filter', 'Filter'),
    )

    method = models.CharField(max_length=20, choices=METHOD_CHOICES)

    field = models.CharField(max_length=100, choices=[(field.name, field.name) \
                                                      for field in CLPeptide._meta.fields])

    LOOKUP_CHOICES = (
        ('exact', 'Exact match'),
        ('iexact', 'Exact match case insentive'),
        ('contains', 'Contains'),
        ('icontains', 'Contains case insentive'),
        ('gt', 'Greater than'),
        ('gte', 'Greater than or equal to'),
        ('lt', 'Less than'),
        ('lte', 'Less than or equal to'),
        ('startswith', 'Starts-with'),
        ('istartswith', 'Starts-with case insensitive'),
        ('endswith', 'Ends-with'),
        ('iendswith', 'Ends-with case insensitive'),
        ('isnull', 'Is null'),
        ('regex', 'Regular expression match'),
        ('iregex', 'Regular expression match case insensitive'),
    )

    field_lookup = models.CharField(max_length=50, choices=LOOKUP_CHOICES)

    value = models.CharField(max_length=50)


class ProcessedDataset(Dataset):
    """
    This class holds dataset information of cross-linked peptides
    after grouping and/or filtering.
    """

    datasets = models.ManyToManyField(Dataset,
                                      related_name='processed_datasets')

    clpeptidefilter = models.ForeignKey(CLPeptideFilter, blank=True,
                                        null=True)


@receiver(pre_delete, sender=ProcessedDataset)
def remove_CLPeptidePD(sender, instance, **kwargs):
    """
    Removes CLpeptides not linked to any dataset after deletion
    """

    for clpep in instance.clpeptide_set.all():
        if clpep.dataset.count() == 1:
            clpep.delete()


def upload_path_handler_PDB(instance, filename):
    """
    Path handler for dataset file upload.
    """
    return "{classname}/{filename}.pdb".format(classname=instance.__class__.__name__,
                                               filename=instance.identifier)


class PDB(models.Model):
    """
    This class holds PDB stored on the server.
    """

    identifier = models.CharField(max_length=50, unique=True, )

    title = models.CharField(max_length=1000)

    file = models.FileField(upload_to=upload_path_handler_PDB,
                            max_length=255,
                            help_text='Select PDB file.')

    def __str__(self):
        return f'{self.identifier} - {self.title}'

    class Meta:
        ordering = ['identifier']


def upload_path_handler_peak_list(instance, file_name):
    """
    Path handler for file upload.
    :param instance: Model instance
    :param file_name: String
    :return: String
    """

    file_path = os.path.join(type(instance).__name__, str(instance.pk), file_name)

    if os.path.exists(file_path):
        raise OSError('File exists at: %s' % file_path)

    return file_path


class PeakList(models.Model):
    """
    This class holds MGF PeakList stored on the server.
    """

    file = models.FileField(help_text='MGF file',
                            upload_to=upload_path_handler_peak_list)

    replace_ms2_ref = models.BooleanField(default=False,
                                          help_text='Replace existing MS/MS references.')

    parsing_status = models.BooleanField(default=False)

    parsing_log = models.CharField(max_length=1000, blank=True, null=True)

    @property
    def filename(self):
        return os.path.basename(self.file.name)
