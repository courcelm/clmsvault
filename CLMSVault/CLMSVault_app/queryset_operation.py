# # Copyright 2013-2014 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 

# Import standard libraries

# Import Django related libraries

# Import project libraries
from .models import (CLPeptide,
                     FastaDb_Sequence,
                     QuantificationFC
                     )


def clpeptide_set_2_protein_set(clpeptide_set):
    """
    Converts a clpeptide queryset to a protein (FastaDb_Sequence)queryset.
    """

    protein_pk_set = set()
    for x in clpeptide_set.order_by().values_list('fs_prot1_id', 'fs_prot2_id').distinct():
        protein_pk_set.add(x[0])
        protein_pk_set.add(x[1])

    protein_set = FastaDb_Sequence.objects.filter(pk__in=list(protein_pk_set))

    return protein_set


def clpeptide_set_2_protein_sequences(clpeptide_set):
    """
    Converts a clpeptide queryset to a protein sequences dictionary.
    Key: protein.id
    Value: protein.sequence
    """

    protein_sequences = dict()

    for protein in clpeptide_set_2_protein_set(clpeptide_set):
        protein_sequences[protein.id] = protein.sequence

    return protein_sequences


def clpeptide_set_2_quantificationPkString(clpeptide_set):
    """
    Converts a clpeptide queryset to quantification pk list.
    """

    s_clpep = set()
    s = set()

    for clpep in clpeptide_set:
        s_clpep.add(clpep.pk)

    for qFC in QuantificationFC.objects.all():

        if qFC.clpeptide.pk in s_clpep:
            s.add(str(qFC.quantification.pk))
            # s.add(str(qFC.clpeptide.pk))

    return ','.join(s)


def dataset_set_2_clpeptide_set(dataset_set):
    """
    Converts a dataset queryset to a clpeptide queryset.
    """

    datasets_list = list(dataset_set.values_list('pk', flat=True))
    clpeptide_set = CLPeptide.objects.filter(dataset__in=datasets_list)

    return clpeptide_set
