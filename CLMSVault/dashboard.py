# # Copyright 2013 Mathieu Courcelles
# # Mike Tyers's lab / IRIC / Universite de Montreal 

"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'MSQCdb.dashboard.CustomIndexDashboard'
"""


# Import standard librariesdjang

# Import Django related libraries
from django.utils.translation import ugettext_lazy as _

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


# Import project libraries



class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    
    template = 'grappelli/dashboard/dashboard_custom.html'
    
    
    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        
        # append an app list module for "Applications"
        self.children.append(modules.AppList(
            _('Quick links'),
            collapsible=True,
            column=1,
            #css_classes=('collapse closed',),
            css_classes=('grp-collapse grp-closed',),
            exclude=('django.contrib.*',),
        ))
        
        # append an app list module for "Administration"
        self.children.append(modules.ModelList(
            _('Administration'),
            column=1,
            #collapsible=False,
            css_classes=('grp-collapse grp-closed',),
            models=('django.contrib.*',),
        ))

        
        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=10,
            #collapsible=False,
            css_classes=('grp-collapse grp-closed',),
            column=2,
        ))

        
