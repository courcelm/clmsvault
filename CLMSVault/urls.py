## Copyright 2013-2017 Mathieu Courcelles
## Mike Tyers's lab / IRIC / Universite de Montreal 

# Import standard libraries
import os

# Import Django related libraries
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import RedirectView
from django.views.static import serve

# Import project libraries
from CLMSVault.CLMSVault_app.views import alive, clpeptide_msms, clpeptide_validation


admin.autodiscover()

urlpatterns = [

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^$', RedirectView.as_view(url='/admin/')),

    # JSMol fix
    # url(r'/j2s/(?P<path>.*)$', 'django.views.static.serve',
    #     {'document_root': os.path.dirname(settings.MEDIA_ROOT + '/js/jsmol/j2s/%(path)')}),
    url(r'idioma/(?P<path>.*)$', serve,
        {'document_root': os.path.dirname(settings.STATIC_ROOT + '/js/jsmol/idioma/%(path)')}),
    # url(r'/java/(?P<path>.*)$', 'django.views.static.serve',
    #     {'document_root': os.path.dirname(settings.MEDIA_ROOT + '/js/jsmol/java/%(path)')}),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^media/(?P<path>.*)$', serve,
        {'document_root': os.path.dirname(settings.MEDIA_ROOT + '/')}),

    url(r'view_msms/(?P<pk>\d+?)/$',
        clpeptide_msms,
        name='view_msms'),

    url(r'view_msms/(?P<pk>\d+?)/validation/(?P<value>\w?)/$',
        clpeptide_validation,
        name='msms_validation'),

    url(r'^alive/', alive),

]

# Admin customization
# Ref: http://stackoverflow.com/questions/4938491/django-admin-change-header-django-administration-text
admin.site.index_title = ''

if settings.DEBUG:

    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns